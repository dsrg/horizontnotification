#pragma once


#include "client.h"
#include <stdlib.h>
#include <time.h>




  void * initialize_data_logger(const char * filename, const char **  names, int size) {
    int n;
    DataLogger_h * hand = malloc(sizeof(DataLogger_h));
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    char buffer[50];
    sprintf(buffer, "%s/%d-%d-%d_%d%d%d.txt", filename, tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    hand->f = fopen(buffer, "w");
    for (n = 0 ; n<size; n++) {
      fprintf(hand->f, "%s\t", names[n]);
    }
    fprintf(hand->f, "\n");  
    return hand;
  }

  void log_data(void * obj, double * data, int size){
    DataLogger_h * hand = (DataLogger_h *)obj;
    if (hand != NULL) {
      int n;
      for (n  = 0; n < size; n++) {
        fprintf(hand->f, "%f\t", data[n]);
      }
      fprintf(hand->f, "\n");
    }
    
  }

  void deinitialize_data_logger(void * obj){
    DataLogger_h * hand = (DataLogger_h *)obj;
    if (hand != NULL) {
      fclose(hand->f);
    }
    
  }

  int bit_set(int * bits_address ,int * bits_stat, int size) {
    int n;
    int ret = 0;
    for (n = 0; n < size; n++) {
      bitWrite(ret, bits_address[n], bits_stat[n]);
    }
    return ret;
  }


  void bit_read(int val, int * bits_address, int* ret, int size) {
    int n;
    for (n = 0; n < size; n++) {
      ret[n] = bitRead(val, bits_address[n]);
    }
  }
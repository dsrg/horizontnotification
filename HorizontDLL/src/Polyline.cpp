
#include "Polyline.h"

using namespace Horizont_DLL;

    // Methods 
  Point Segment::point_on_segment_closest_to( Point p){
    Vector seg_vec = to_vector();
    Vector pos_vec = p - get_start();
    double seg_l = this->length();
    double proj = MAX(0, MIN(seg_vec * pos_vec / MAX(seg_l*seg_l, 1.0e-6), 1));
    if (seg_l == 0)
      return get_start();
    return get_start() + Point(seg_vec) * proj;
  }


  Point Segment::point_on_vector_closest_to(Point p) {
    Vector vh(end - start);
    Vector vh_n = vh.normalize();
    Vector pos_vec = p - start;
    double proj = vh_n*pos_vec;
    return start + Point(vh_n) * proj;
  }


  Point Segment::point_at_distance_along_vector(double d, bool forward) {
      if (forward)
          return start + Point(to_vector().normalize())*d;
        return start - Point(to_vector().normalize())*d;
  }


  Polyline::Polyline(double* x, double *y, double *z, int size) {
    distance.push_back(0);
    this->size = size;
    for (int i = 0; i < size; i++) {
      points.push_back(Point(x[i], y[i], z[i]));
      if (i > 0) {
          double dist =  distance.at(i-1) + Vector((points.at(i) - points.at(i - 1))).length();
          distance.push_back(dist);
      }
    }
    last_point =0;
    last_lookup_point = Point(0, 0, 0);
    last_index = -1;
    Point last_lookup_result_point = Point(0, 0, 0);

  }
  


  Point Polyline::closest_point_on_polyline(Point p, double * dist_from_start_out, double max_dist, bool search_all, bool interpolate) {
    //printf("Getting location for %f, %f, %f\n", p.get_x(), p.get_y(), p.get_z());
    if (last_lookup_point == p && last_index >=0) {
      *dist_from_start_out = last_point;
      return last_lookup_result_point;
    }
    search_all = search_all ||  last_index < 0; // look everywhere if last search failed/
    double poly_len = distance.at(size-1);
    double max_dist_calc =  search_all ?  distance.at(size-1) :  max_dist;
    int from = MIN(size - 2, index_at_distance (last_point - max_dist_calc));
    int to = MAX(2, index_at_distance(last_point + max_dist_calc));
    double min_dist = Vector(points.at(from) - p).length();
    int temp_i = from;
    double tmp_dist;
    Point temp_point;
    int i;
    for (i = from + 1; i <= to; i++)  {
      Segment s(points.at(i), points.at(i - 1));
      Point p_on_segment = s.point_on_segment_closest_to(p);
      tmp_dist = Vector(p_on_segment - p).length();
      if (tmp_dist <= min_dist) {
        min_dist = tmp_dist;
        temp_i = i - 1;
        temp_point = p_on_segment;
      }
    }
    double d_to_in =distance.at(temp_i);
    double d_from_ind =  Vector(points.at(temp_i) - temp_point).length();

    *dist_from_start_out = d_to_in + d_from_ind;

    // Interpolate if we are far from the end
    if (interpolate &&  temp_i >= size - 2) { // maybe -2
      Point p_on_vector = Segment(points.at(size - 2), points.at(size - 1)).point_on_vector_closest_to(p);
      tmp_dist = Vector(p_on_vector - p).length();
      if (tmp_dist + min_dist *0.001 < min_dist) {
        *dist_from_start_out = distance.at(size - 1);
        temp_point =  p_on_vector;
      }
    }

    if (interpolate &&  temp_i == 0) {
      Point p_on_vector = Segment(points.at(1), points.at(0)).point_on_vector_closest_to(p);
      tmp_dist = Vector(p_on_vector - p).length();
      if (tmp_dist + min_dist *0.001 < min_dist ) {
        *dist_from_start_out =  0;
        temp_point = p_on_vector;
      }
    }
    last_point = *dist_from_start_out;
    last_index = temp_i;
    last_lookup_result_point = temp_point;
    last_lookup_point = p;
    return temp_point;  
  }

  Point  Polyline::point_at_index(int index) {
    return points.at(MAX(0, MIN(index, size - 1)));
  }
  

  double Polyline::get_last_point() {
    return last_point;
  }


  int Polyline::get_last_index(){
    return last_index; 
  }


  int Polyline::index_at_distance(double value) {
    if (distance.at(size-1) < value) {
      return size - 1;
    }
    else if (size  == 1) {
      return 0;
    }
    else if (distance.at(0) > value) {
      return 0;
    }
    int l_ind  = 0;
    int h_ind  =size-1;
    int m_ind  = (int)floor((h_ind - l_ind)*1.0 / 2);
    while (h_ind - l_ind > 1) {
      if (distance.at(m_ind)  == value) {
        return m_ind;
      }
      else if (distance.at(m_ind) > value) {
        h_ind = m_ind;
      }
      else {
        l_ind = m_ind;
      }
      m_ind = l_ind + (int)floor((h_ind - l_ind)*1.0 / 2);
    }
    // check if the next distance is the same and if it is then find the last one identical
    while (size < m_ind - 1 && distance.at(m_ind) == distance.at(m_ind + 1)) {
      m_ind++;
    }
    return m_ind;
  };
  

  double Polyline::distance_at_index(int index) {
    return distance.at(MAX(0,MIN(index, size-1)));
  }

  Point Polyline::point_at_distance(double d) {
      int ind = index_at_distance(d);
      double d_at_ind = distance_at_index(ind);
      double dif = d-d_at_ind;
      Segment s = segment_at_index(ind);
      return s.point_at_distance_along_vector(dif);
  }

  Segment Polyline::segment_at_index(int index){
      if (index >= size-1)
          return Segment(points.at(size-1), points.at(size-1)+ 0.01); // just a tyny segment afterr the end
      else if (index <= 0)
          return Segment(points.at(0), points.at(1));
      else
          return Segment(points.at(index), points.at(index+1));
  }


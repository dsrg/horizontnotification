#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <conio.h>
#include <string>
#include "DataParser.h"

#include <iostream>
using namespace Horizont_DLL;

DataParser::DataParser(const char * filename, double Cd, double A, double q, double m, double g, double f, double Pmax, double Tmax)
{
  printf("=== HORIZONT_DLL Library === (START)\n");

    this->Cd = Cd;
    this->A = A;
    this->q = q;
    this->m = m;
    this->g = g;
    this->f = f;
    this->Pmax = Pmax;
    this->Tmax = Tmax;
    rows = this->get_rows(filename);
  printf("[HORIZONT_DLL] '%s' data parser has %d rows\n", filename, rows);
  if (rows > 0) {

    desired_speed_profile = (double*)malloc(sizeof(double) * rows);
      coasting_speed_profile = (double*)malloc(sizeof(double) * rows);
    x_coor = (double*)malloc(sizeof(double) * rows);
    y_coor = (double*)malloc(sizeof(double) * rows);
    z_coor = (double*)malloc(sizeof(double) * rows);
    distance = (double*)malloc(sizeof(double) * rows);
    max_speed = (double*)malloc(sizeof(double) * rows);

    double * curvature = (double*)malloc(sizeof(double) * rows);
    double * super_elevation = (double*)malloc(sizeof(double) * rows);

    double * comfort_curve_velocity = (double*)malloc(sizeof(double) * rows);
    double * min_velocity_profile = (double*)malloc(sizeof(double) * rows);
    double * critical_velocity = (double*)malloc(sizeof(double) * rows);
    double * coasting_velocity_profile = (double*)malloc(sizeof(double) * rows);
    double * elevation = (double*)malloc(sizeof(double) * rows);

    rows = read_from_file(rows, filename, x_coor, y_coor, z_coor, curvature, max_speed, super_elevation);
    if (rows < 0) {
      printf("[HORIZONT_DLL] data parser FAILED reading file %s\n", filename);
    }
    //
    distance[0] = 0;
    elevation[0] = 0;
    //
    for (int i = 1; i < rows; i++) {
      double dist_of_segment = sqrt(pow((x_coor[i] - x_coor[i - 1]), 2) + pow((y_coor[i] - y_coor[i - 1]), 2) + pow((z_coor[i] - z_coor[i - 1]), 2));
	  double dist_of_segment_project = sqrt(pow((x_coor[i] - x_coor[i - 1]), 2) + pow((y_coor[i] - y_coor[i - 1]), 2));

      distance[i] = distance[i - 1] + dist_of_segment;
      if (dist_of_segment_project < 0.000001) {
        elevation[i - 1] = elevation[i - 2];
      }
      else {
        double slope = (z_coor[i] - z_coor[i - 1]) / dist_of_segment_project;
        elevation[i - 1] = slope;
      }

      distance[i] = distance[i - 1] + dist_of_segment;
    }
    elevation[rows - 1] = elevation[rows - 2];
    //


    int reverse_i;

    for (int i = 0; i < rows; i++) {
      comfort_curve_velocity[i] = v_comfort_speed(curvature[i], super_elevation[i]);
      min_velocity_profile[i] = H_MIN(max_speed[i], comfort_curve_velocity[i]);
    }
    for (int i = 0; i < rows; i++) {
      if ((i == 0) || (i == (rows - 1)))
        critical_velocity[i] = min_velocity_profile[i];
      else
        critical_velocity[i] = critical_speed_spots(min_velocity_profile[i - 1], min_velocity_profile[i], min_velocity_profile[i + 1]);
    }
    for (int i = 0; i < rows; i++) {
      reverse_i = rows - (1 + i);
      if (i == 0)
        coasting_velocity_profile[reverse_i] = min_velocity_profile[reverse_i];
      else {
        if (Pmax <= 0.0 && Tmax <= 0.0){
          coasting_velocity_profile[reverse_i] = ice_coasting_speed(elevation[reverse_i], coasting_velocity_profile[reverse_i + 1], distance[reverse_i + 1], distance[reverse_i], critical_velocity[reverse_i]);
        }
        else {
          coasting_velocity_profile[reverse_i] = initial_coasting_speed(elevation[reverse_i], coasting_velocity_profile[reverse_i + 1], distance[reverse_i + 1], distance[reverse_i], critical_velocity[reverse_i]);
        }
      }
      desired_speed_profile[reverse_i] = H_MIN(coasting_velocity_profile[reverse_i], min_velocity_profile[reverse_i]);
      coasting_speed_profile[reverse_i] = coasting_velocity_profile[reverse_i];
    }
    poly = new Polyline(x_coor, y_coor, z_coor, rows);
    printf("[HORIZONT_DLL] data parser finished reading '%s' it has '%d'\n", filename, rows);
  }
}


DataParser::~DataParser()
{
    delete poly, distance, desired_speed_profile, x_coor, y_coor, z_coor, max_speed, coasting_speed_profile;
}




Polyline * DataParser::get_polyline() {
  return poly;
}


double DataParser::value_at_ditance(double dist, double * vec) {

  int middle = poly->index_at_distance(dist);
  return vec[middle] + ((vec[middle + 1] - vec[middle]) / (distance[middle + 1] - distance[middle]))*(dist - distance[middle]);
}

double DataParser::get_recomended_speed_at_distance(double current_distance) {
  return value_at_ditance(current_distance, desired_speed_profile);
}

double DataParser::get_coasting_speed_at_distance(double current_distance) {
    return value_at_ditance(current_distance, coasting_speed_profile);
}

int DataParser::read_from_file(int number_of_lines, const char * file, double * x, double * y, double *z, double * curve_out, double * speed_max, double * super_elevation){
    FILE *fp;

    errno_t  error = fopen_s(&fp, file, "r");

    if (error != 0) {
        fprintf(stderr, "Can't open output file %s!\n",
            file);
    printf("[HORIZONT_DLL] data parser FAILED reading file %s\n", file);
        return -1;
    }
    int ch = 0;
    char *line = (char *)malloc(MAX_LINE_SZ);
    double x_i, y_i, z_i, c_i, s_i, e_i;
    while (!feof(fp) && ch < number_of_lines) {
        fgets(line, MAX_LINE_SZ, fp);
        if ((sizeof(line) / sizeof(char) < 1) && (line[sizeof(line) / sizeof(char) - 1] != '\n'))
            continue;
        if (sscanf_s(line, "%lf %lf %lf %lf %lf %lf", &x_i, &y_i, &z_i, &c_i, &s_i, & e_i) != 6)
            continue;
        x[ch] = x_i;
        y[ch] = y_i;
        z[ch] = z_i;
        curve_out[ch] = c_i;
        speed_max[ch] = s_i;
        super_elevation[ch] = e_i;
        ch++;
    }
    fclose(fp);
    return ch;
}

int DataParser::get_rows(const char * filename) {

    FILE *fp;
    errno_t  error = fopen_s(&fp, filename, "r");

    if (error != 0) {
        fprintf(stderr, "Can't open output file %s!\n",	filename);
    printf("[HORIZONT_DLL] data parser FAILED reading file %s\n", filename);
        return -1;
    }
    int ch, number_of_lines = 0;
    do {
        ch = fgetc(fp);
        if (ch == '\n')
            number_of_lines++;
    } while (ch != EOF);
    if (ch != '\n' && number_of_lines != 0)
        number_of_lines++;
  fclose(fp);
    return number_of_lines;
}

double DataParser::v_comfort_speed(double radius, double superelevation) {

    double f, comfort_curve_velocity;

    double f_initial = 0.2;
    double difference_f = 1;

    while (difference_f > 0.001) {
        f = f_initial;
        comfort_curve_velocity = ((sqrt(radius*9.81*(f + superelevation) / (1 - f*superelevation))) * 3600) / 1000;
        f_initial = 0.2237 * pow(2.71828182846, ((-0.006) * comfort_curve_velocity));
        difference_f = fabs(f_initial - f);
    }
    return comfort_curve_velocity;
    //printf("Comfort curve velocity: %f\n", comfort_curve_velocity);
}

double DataParser::critical_speed_spots(double value1, double value2, double value3) {

    double result;

    if ((value1>value2) && (value3 >= value2)) {
        result = value2;
    }
    else {
        result = 1000;
    }
    return result;
}

double DataParser::ice_coasting_speed(double Slope, double V_coast_next, double Offset_next, double Offset_current, double Critical_Speed) {

  /* local variable declaration */
  double C; //local variable
  double K; //local variable
  //vehicle aerodynamic characteristics
  double Cd = 0.3;
  double A = 2.3;
  double q = 1.293;
  //vehicle aerodynamic characteristics
  double m = 1644;
  double g = 9.81;
  double f = 0.015;

  double K1; //local variable
  double K2; //local variable
  double V_coast_next_IC;
  double under_sqrt;

  double V_coasting;

  K = 0.5*q*Cd*A;
  C = m*g*f*cos(atan(Slope)) + m*g*sin(atan(Slope));




  if ((Critical_Speed>900) && (V_coast_next<10)) {
	  V_coasting = V_coast_next;
  }
  else {
	  if ((Critical_Speed - V_coast_next)>10 && (Critical_Speed<150)) {
		  V_coasting = Critical_Speed;
	  }
	  else {
		  if ((Critical_Speed < 1000) && (Critical_Speed<V_coast_next)) {
			  V_coasting = Critical_Speed;
		  }
		  else {

			  V_coast_next_IC = V_coast_next / 3.6;
			  K1 = C + K* V_coast_next_IC*V_coast_next_IC;
			  K2 = exp(2 * K*(Offset_next - Offset_current) / m);
			  under_sqrt = abs((K1*K2 - C) / K);
			  V_coasting = sqrt(under_sqrt);
			  V_coasting = V_coasting*3.6;
		  }
	  }
  }

  return V_coasting;
}

  

double DataParser::initial_coasting_speed(double Slope, double V_coast_next, double Offset_next, double Offset_current, double Critical_Speed) {

    double Cad;
    double Crb4;
    double Vcr;
    double RConst;
    double RSegment;
    double Accceleration;
    double V_coasting;
    double V_coast_next_E;
    Cad = 0.5*q*Cd*A;
    Crb4 = 9.578*0.10472*Pmax;
    Vcr = pow((Crb4 / (2 * Cad)), 0.33333333333);
    RConst = Cad*pow(Vcr, 2) + Crb4 / Vcr;
    RSegment = m*g*f*cos(atan(Slope)) + m*g*sin(atan(Slope));



	if ((Critical_Speed>200) && (V_coast_next<10)) {
		V_coasting = V_coast_next;
	}
	else {
		if ((Critical_Speed - V_coast_next<20) && (Critical_Speed<150)) {
			V_coasting = Critical_Speed;
		}
		else {
			if ((Critical_Speed < 1000) && (Critical_Speed<V_coast_next)) {
				V_coasting = Critical_Speed;
			}
			else {
				Accceleration = (RSegment + RConst) / m;
				V_coast_next_E = V_coast_next / 3.6;
				V_coasting = sqrt((V_coast_next_E * V_coast_next_E) + 2 * (Offset_next - Offset_current)*Accceleration);
				V_coasting = V_coasting * 3.6;
			}
		}
	}







    
    return V_coasting;
}

double DataParser::get_max_distance()
{
  return distance[rows - 1];
}

double DataParser::get_min_distance()
{
  return distance[0];
}

double DataParser::get_last_speed()
{
  return desired_speed_profile[rows - 1];
}

double DataParser::get_first_speed()
{
  return desired_speed_profile[0];
}


double DataParser::get_last_speed_coasting()
{
    return coasting_speed_profile[rows - 1];
}

double DataParser::get_first_speed_coasting()
{
    return coasting_speed_profile[0];
}

bool DataParser::get_x_vec(double * x_out, int length) {
  if (rows < 0)
    return 0;
  memcpy(x_out, x_coor, H_MIN(length, rows)*sizeof(double));
  return true;

}

bool DataParser::get_y_vec(double * y_out, int length){
  if (rows < 0)
    return 0;
  memcpy(y_out, y_coor, H_MIN(length, rows)*sizeof(double));
  return true;
}

bool DataParser::get_z_vec(double * z_out, int length){
  if (rows < 0)
    return 0;
  memcpy(z_out, z_coor, H_MIN(length, rows)*sizeof(double));
  return true;

}

bool DataParser::get_dist_vec(double * dist, int length){
  if (rows < 0)
    return 0;
  memcpy(dist, distance, H_MIN(length,rows)*sizeof(double));
  return true;

}


bool DataParser::get_rec_speed_vec(double * rec_s_out, int length){
  if (rows < 0)
    return 0;
  memcpy(rec_s_out, desired_speed_profile, H_MIN(length, rows)*sizeof(double));
  return true;

}


bool DataParser::get_coasting_speed_vec(double * rec_s_out, int length) {
    if (rows < 0)
        return 0;
    memcpy(rec_s_out, coasting_speed_profile, H_MIN(length, rows) * sizeof(double));
    return true;

}





bool DataParser::get_max_speed_vec(double * max_s_out, int length){
  if (rows < 0)
    return 0;
  memcpy(max_s_out, max_speed, H_MIN(length, rows)*sizeof(double));
  return true;

}


int DataParser::get_rows(){
  return rows;
}


bool   DataParser::get_pos_at_distance(double dist, double * x_out, double * y_out, double * z_out) {
  if (!poly)
    return false;
  Point p =poly->point_at_distance(dist);
  *x_out = p.get_x();
  *y_out = p.get_y();
  *z_out = p.get_z();
  return true;
}

bool   DataParser::get_index_and_ratio_at_distance(double dist, int * index_out, double * ratio_out) {
  *index_out = poly->index_at_distance(dist);
  *ratio_out = (dist - distance[*index_out]) / (distance[*index_out + 1] - distance[*index_out]);
  return *index_out >= 0;
}

bool   DataParser::get_all_at_distance(double distance, double * m_speed, double * r_speed, double * c_speed){
  int middle;
  double ratio;
  if (get_index_and_ratio_at_distance(distance, &middle, &ratio)) {
    *m_speed = max_speed[middle] + (max_speed[middle + 1] - max_speed[middle]) * ratio;
    *r_speed = desired_speed_profile[middle] + (desired_speed_profile[middle + 1] - desired_speed_profile[middle]) * ratio;
    *c_speed = coasting_speed_profile[middle] + (coasting_speed_profile[middle + 1] - coasting_speed_profile[middle]) * ratio;
    return true;
  }
  return false;
}


bool   DataParser::get_all_at_position(double x, double y, double z, bool sealc_all, double max_range, double * polyline_pos, double * m_speed, double * r_speed, double * c_speed){
  poly->closest_point_on_polyline(Point(x, y, z), polyline_pos, max_range, sealc_all, false);
  int ind = poly->get_last_index();
  double dist_at_ind = poly->distance_at_index(ind);
  double ratio = 0.0;
  if (poly->distance_at_index(ind + 1) - dist_at_ind > 0.0001)
    ratio = (*polyline_pos - dist_at_ind) / (poly->distance_at_index(ind + 1) - dist_at_ind);
  *m_speed = max_speed[ind] + (max_speed[ind + 1] - max_speed[ind]) * ratio;
  *r_speed = desired_speed_profile[ind] + (desired_speed_profile[ind + 1] - desired_speed_profile[ind]) * ratio;
  *c_speed = coasting_speed_profile[ind] + (coasting_speed_profile[ind + 1] - coasting_speed_profile[ind]) * ratio;
  return true;
}



double DataParser::get_distance_at_position(double x, double y, double z, bool sealc_all, double max_range ) {
  double dist;
  poly->closest_point_on_polyline(Point(x, y, z), &dist, max_range, sealc_all, false);
  return dist;
}

double DataParser::get_max_speed_at_position(double x, double y, double z) {
  double dist = get_distance_at_position(x, y, z, false);
  return value_at_ditance(dist, max_speed);

}
double DataParser::get_max_speed_at_distance(double dist) {
  return value_at_ditance(dist, max_speed);
}

double DataParser::get_recomended_speed_at_position(double x, double y, double z) {
  double dist = get_distance_at_position(x, y, z, false);
  return get_recomended_speed_at_distance(dist);
}

double DataParser::get_coasting_speed_at_position(double x, double y, double z) {
    double dist = get_distance_at_position(x, y, z, false);
    return get_coasting_speed_at_distance(dist);
}

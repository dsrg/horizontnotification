#include "DataLoger.h"
#include <stdio.h>


Connector::Connector(int port, const char *address, int bConnect, int sleep_ms){
  this->bConnect = bConnect == 1;
  this->sleep_ms = sleep_ms;
  if (this == NULL)
  {
    printf("Not enough memory!\n");
    connected = false;
    return;
  }

  if (WSAStartup(MAKEWORD(2, 2), &this->wsd) != 0)
  {
    printf("WSAStartup failed!\n");
    connected = false;
    return;
  }
  this->s = socket(AF_INET, SOCK_DGRAM, 0);
  if (this->s == INVALID_SOCKET)
  {
    printf("socket() failed; %d\n", WSAGetLastError());
    connected = false;
    return;

  }
  this->recipient.sin_family = AF_INET;
  this->recipient.sin_port = htons((short)port);
  if ((this->recipient.sin_addr.s_addr = inet_addr(address)) == INADDR_NONE)
  {
    struct hostent *host = NULL;

    host = gethostbyname(address);
    if (host)
      CopyMemory(&this->recipient.sin_addr, host->h_addr_list[0],  host->h_length);
    else
    {
      printf("gethostbyname() failed: %d\n", WSAGetLastError());
      WSACleanup();
      connected = false;
      return;
    }
  }
  if (bConnect)
  {
    if (connect(this->s, (SOCKADDR *)&this->recipient, sizeof(this->recipient)) == SOCKET_ERROR)
    {
      printf("connect() failed: %d\n", WSAGetLastError());
      WSACleanup();
      connected = false;
      return;
    }
  }
  this->datasize = sizeof(this->data);
  this->buffer = (char *)malloc(this->datasize);
  connected = true;
  last_time_update = 0;
}

Connector::~Connector(){
  if (connected) {
    closesocket(s);
    WSACleanup();
  }
}

void Connector::send_message_dash(int gear, double speed, double rmp, double temp, double soc, double  power, double fc, double fc_cumul, double distance, int engine_on, double time_in, int extra_control){
  if (time_in > last_time_update) {
    last_time_update = time_in;

    data.gear = gear;
    data.speed = speed;
    data.rmp = rmp;
    data.avaliable_power = temp;
    data.fuelLevel = soc;
    data.power_in_out = power;
    data.fuel_consumption = fc_cumul;
    data.distance = distance;
    data.engineOn = engine_on;
    data.extra_control = extra_control;
    if (connected) {
      memcpy(buffer, &data, datasize);
      if (bConnect){
        int ret = send(s, buffer, datasize, 0);
        if (ret == SOCKET_ERROR)
        {
          printf("send() failed: %d\n", WSAGetLastError());
        }
      }
      else  {
        int ret = sendto(s, buffer, datasize, 0, (SOCKADDR *)&recipient, sizeof(recipient));
        if (ret == SOCKET_ERROR)
        {
          printf("sendto() failed; %d\n", WSAGetLastError());
        }
      }
    }
  }
}

double Connector::send_msg_plot(double * x_data, double * y_data, size_t x_size, size_t y_size, double time){
  if (time > last_time_update) {
    last_time_update = time;
    char * buff = (char *)malloc((y_size + x_size)*sizeof(double));
    if (connected) {
      memcpy(buff, x_data, x_size*sizeof(double));
      memcpy(buff + x_size*sizeof(double), y_data, y_size*sizeof(double));
      if (bConnect){
        int ret = send(s, buff, (y_size + x_size)*sizeof(double), 0);
        if (ret == SOCKET_ERROR)
        {
          printf("send() failed: %d\n", WSAGetLastError());
        }

      }

      else
      {

        int ret = sendto(s, buff, (y_size + x_size)*sizeof(double), 0, (SOCKADDR *)&recipient, sizeof(recipient));
        if (ret == SOCKET_ERROR)
        {
          printf("sendto() failed; %d\n", WSAGetLastError());
        }

      }
      sleep_ms(sleep_ms);
    }
  }
  return time;
}
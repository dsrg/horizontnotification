#include "HorizontDLL.h"
#include "DataParser.h"
#include "Polyline.h"
#include "DataLoger.h"


using namespace Horizont_DLL;

void *  initialize_horizont(const char * filename, double Cd, double A, double q, double m, double g, double f, double Pmax, double Tmax) {
	DataParser * dp = new DataParser(filename,  Cd,  A,  q,  m,  g,  f,  Pmax,  Tmax);
  if (dp->get_rows()>0)
    return dp;
  printf("Can not load file for Horizont");
  exit(-1);
}
void   deinitialize_horizont(void* obj) {
	 if (DataParser * dp = (DataParser *)obj) {
		 delete dp;
	 }
 }
double get_recomended_speed_at_distance(void* obj, double curent_pos) {
	 if (DataParser * dp = (DataParser *)obj) {
		 if (curent_pos < dp->get_min_distance())
			 return dp->get_first_speed();
		 else if (dp->get_max_distance() < curent_pos)
			return dp->get_last_speed();
		 else
			 return dp->get_recomended_speed_at_distance(curent_pos);			
	 }
	 return -1.0;
}
double get_coasting_speed_at_distance(void* obj, double curent_pos) {
	if (DataParser * dp = (DataParser *)obj) {
		if (curent_pos < dp->get_min_distance())
			return dp->get_first_speed_coasting();
		else if (dp->get_max_distance() < curent_pos)
			return dp->get_last_speed_coasting();
		else
			return dp->get_coasting_speed_at_distance(curent_pos);
	}
	return -1.0;
}





 double get_max_distance_horizont(void* obj){
   if (DataParser * dp = (DataParser *)obj) {
     return dp->get_max_distance();
   }
   return -1;
 }

 void get_horizont_x_vec(void* obj, double * x_out, int length){
   if (DataParser * dp = (DataParser *)obj) {
     dp->get_x_vec(x_out, length);
   }
 }

 void get_horizont_y_vec(void* obj, double * y_out, int length){
   if (DataParser * dp = (DataParser *)obj) {
     dp->get_y_vec(y_out, length);
   }
 
 }

 void get_horizont_z_vec(void* obj, double * z_out, int length){
   if (DataParser * dp = (DataParser *)obj) {
     dp->get_z_vec(z_out, length);
   }
 }

 void get_horizont_dist_vec(void* obj, double * dist, int length){
   if (DataParser * dp = (DataParser *)obj) {
     dp->get_dist_vec(dist, length);
   }
 
 }

 int get_horizont_rows(void* obj) {
   if (DataParser * dp = (DataParser *)obj) {
     return dp->get_rows();
   }
   return -1;
 }

 int get_pos_at_polyline_location(void* obj, double dist, double * pos, int size) {
   double x, y, z;
   if (DataParser * dp = (DataParser *)obj) {
     dp->get_pos_at_distance(dist, &x, &y, &z);
     pos[0] = x;
     pos[1] = y;
     pos[2] = z;
     return 1;
   }
   return 0;
 
 }
 int   get_pos_at_distance(void* obj, double dist, double * x_out, double * y_out, double * z_out) {
   if (DataParser * dp = (DataParser *)obj) {
     dp->get_pos_at_distance(dist, x_out, y_out, z_out);
     return 1;
   }
   return 0;
 }

 double get_distance_at_position(void* obj, double x, double y, double z, int sealc_all, double max_range){
   if (DataParser * dp = (DataParser *)obj) {
     return dp->get_distance_at_position(x, y, z, sealc_all==1, max_range);
   }
   return -1;
 }

 double get_max_speed_at_position(void* obj, double x, double y, double z){
   if (DataParser * dp = (DataParser *)obj) {
     return dp->get_max_speed_at_position(x, y, z);
   }
   return -1;

 }
 double get_max_speed_at_distance(void* obj, double distance){
   if (DataParser * dp = (DataParser *)obj) {
     return dp->get_max_speed_at_distance(distance);
   }
   return -1;

 }
 double get_recomended_speed_at_position(void* obj, double x, double y, double z){
   if (DataParser * dp = (DataParser *)obj) {
     return dp->get_recomended_speed_at_position(x, y, z);
   }
   return -1;

 }

 double get_coasting_speed_at_position(void* obj, double x, double y, double z) {
	 if (DataParser * dp = (DataParser *)obj) {
		 return dp->get_coasting_speed_at_position(x, y, z);
	 }
	 return -1;

 }


 int get_all_at_position(void* obj, double x, double y, double z, int search_all, double max_range, double * pos_poly_out, double * max_speed_out, double * recomended_speed_out, double * coasting_speed_out) {
   if (DataParser * dp = (DataParser *)obj) {
     return dp->get_all_at_position(x, y, z, search_all, max_range, pos_poly_out, max_speed_out, recomended_speed_out, coasting_speed_out);
   }
   return -1;

 }


 int get_all_at_distance(void* obj, double  pos_poly, double * max_speed_out, double * recomended_speed_out, double * coasting_speed_out){
   if (DataParser * dp = (DataParser *)obj) {
     return dp->get_all_at_distance(pos_poly, max_speed_out, recomended_speed_out, coasting_speed_out);
   }
   return -1; 
 }



 void * init_com(int port, const char *address, int port_in, int sleep_ms) {

   printf("Creating new connection for port %d\n", port);
   Connector * conn = new Connector(port, address, 0, sleep_ms);
   return conn;
 }
 
 void send_msg(void* handl, int gear, double speed, double rmp, double temp, double soc, double  power, double fc, double fc_cumul, double distance, int engine_on, double time_in, int extra_control){
   if (Connector * cn = (Connector *)handl)
     cn->send_message_dash(gear, speed, rmp, temp, soc, power, fc, fc_cumul, distance, engine_on, time_in, extra_control);
   else 
     printf("Dashboard message -  wrong pointer\n");   
 }
 
 double send_msg_plot(void* handl, double * x_data, double * y_data, size_t x_size, size_t y_size, double time){
   if (Connector * cn = (Connector *)handl) {
     return cn->send_msg_plot(x_data,  y_data, x_size, y_size, time);
   }
   return time;
 }
 
 void close_com(void * handl){
   if (Connector * cn = (Connector *)handl) {
       printf("Closing connection for port %d\n");
       delete cn;
   }
 }
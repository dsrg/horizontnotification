#include <stdio.h>
#include <tchar.h>
#include "interfaces.h"



#pragma comment(lib, "wsock32.lib")


DWORD WINAPI udp_receive_thread( LPVOID owner )
{
	// Spoustim metodu execute (smycka pro prijem zprav) ve vlastnim vlakne
	udp_interface *parent = (udp_interface *)owner;
	return parent->execute();
}


udp_interface::udp_interface( int local_port, int remote_port,const char *remote_addr, int bufsize, int params)
{
  printf("Starting ar %i %i\n", local_port, remote_port);
	nParams = params;
	// Inicializace lokalnich promennych
	flocal_port = local_port;
	fremote_port = remote_port;
	hreadthread = NULL;
	sock = INVALID_SOCKET;
	terminate = false;

	// Vzdalena adresa a port
  remoteaddr.sin_family = AF_INET;
  remoteaddr.sin_port = htons(fremote_port);
  if (nParams)
    remoteaddr.sin_addr.s_addr = inet_addr(remote_addr); // Pro pripad brodcast adresy zadat: INADDR_BROADCAST;
  else
    remoteaddr.sin_addr.s_addr = INADDR_BROADCAST;
  // Alokace bufferu pro prijem zprav
  rcvbufsize = bufsize;
  rcvbuf = new char[rcvbufsize];
  
}

udp_interface::~udp_interface()
{
	close();
	// Uvolnuji buffer pro prijem zprav
	delete[] rcvbuf;
}

bool udp_interface::open()
{
	// Cteci vlakno jiz bezi => komunikace je jiz otevrena
	if (hreadthread != NULL) return false;

	// Otevirem udp komunikaci

	// Inicializace socketu (s pozadovanou nejvyssi verzi)
	WORD wVersionRequested;
	WSADATA wsaData;
	wVersionRequested = MAKEWORD( 2, 2 );
	if ( WSAStartup( wVersionRequested, &wsaData ) != 0 ) {
    	return false;
	}

	// Vytvarim socket
	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock == INVALID_SOCKET) {
		WSACleanup();
		return false;
	}

	// Nastavuji parametry socketu
	if (!nParams)
	{
		char sock_param = 1;
		setsockopt( sock, SOL_SOCKET, SO_BROADCAST, &sock_param, sizeof(char) );
		setsockopt( sock, SOL_SOCKET, SO_REUSEADDR, &sock_param, sizeof(char) );  // Otevrena adresa muze byt pouzita i jinymi procesy  // Broadcast povolen
	}

	if (nParams == 1)
	{
		char sock_param = 1;
		setsockopt( sock, SOL_SOCKET, SO_REUSEADDR, &sock_param, sizeof(char) );  // Otevrena adresa muze byt pouzita i jinymi procesy  // Broadcast povolen

	}
	

	// Lokalni adresa
	struct sockaddr_in localaddr;           // Struktura pro ulozeni adresy
	localaddr.sin_family = AF_INET;         // Komunikacni protokol
	localaddr.sin_port = htons( flocal_port );    // Komunikacni port
	localaddr.sin_addr.s_addr = INADDR_ANY; // Adresa (jakakoliv lokalni)

	// Oteviram socket
	if ( bind(sock, (struct sockaddr *) &localaddr, sizeof(localaddr)) != 0 ) {
		WSACleanup();
		return false;
	}

	// Spoustim cteci vlakno
  if (false) {
    terminate = false;
    hreadthread = CreateThread(NULL, 0, udp_receive_thread, this, 0, NULL);
    if (hreadthread == NULL) {
      WSACleanup();
      return false;
    }
  }
	return true;
}

bool udp_interface::close()
{
	// Komunikace je jiz uzavrena
	if (hreadthread == NULL) return false;

	terminate = true;
	// Ukoncuji komunikaci
	shutdown( sock, 2 );
	// Zaviram socket
	closesocket( sock );

	// Cekam na dobeh cteciho vlakna (max 1000 ms)
  if (fremote_port) {
    WaitForSingleObject(hreadthread, 1000);
    CloseHandle(hreadthread);
    hreadthread = NULL;
  }

	// Ukonceni socket komunikace
	WSACleanup();

	return true;
}

void udp_interface::set(int local_port, int remote_port, const char *remote_addr, int bufsize)
{
// Inicializace lokalnich promennych
	flocal_port = local_port;
	fremote_port = remote_port;

	// Vzdalena adresa a port
	remoteaddr.sin_family = AF_INET;
	remoteaddr.sin_port = htons(fremote_port);
	remoteaddr.sin_addr.s_addr = inet_addr( remote_addr ); /*Pro pripad brodcast adresy zadat: INADDR_BROADCAST;*/

	// Alokace bufferu pro prijem zprav
	rcvbufsize = bufsize;
	delete[] rcvbuf;
	rcvbuf = new char[rcvbufsize];
}

DWORD udp_interface::execute()
{
	// Hlavni smycka pro prijem zprav, bezi v samostatnem vlaknu
	int count;
	int sockerr;
	
	while ( ! terminate )
	{
		// Cekam na zpravu
		count = recv( sock, rcvbuf, rcvbufsize, 0 );

		if (count == SOCKET_ERROR)
		{
			// Cteni se nezdarilo
			sockerr = WSAGetLastError();
			// Uzivatel neukoncil komunikaci => nastala tedy nejaka chyba, informuji uzivatele
			//if ( ! terminate ) on_socket_error( sockerr );
		}
		else {
			// Data prijata, volam obsluznou funkci
			on_receive( rcvbuf, count );
		}
	}

	return 0;
}

bool udp_interface::send( char *msg, int msglen )
{
	// Posle zpravu msg o delce msglen na adresu remoteaddr
	int result = sendto( sock, (char *)msg, msglen, 0, (struct sockaddr *)&remoteaddr, sizeof(remoteaddr) );
	if ( result == SOCKET_ERROR) 
	{
		on_socket_error( WSAGetLastError() );
		return false;
	}
	return true;
}

ConnectionInterface::ConnectionInterface(int portIn, int portOut, const char *remote_addr) : udp_interface(portIn, portOut, remote_addr, sizeof(dataDashBoard), 2) {
  data = dataDashBoard();
  data_size = sizeof(dataDashBoard);
  buffer = (char *)malloc(data_size);
  used_amount = 0;
  last_updata = -1;
  open();
}


void ConnectionInterface::used_increase(int val){
  used_amount += val;
}

bool ConnectionInterface::still_in_use() {
  return used_amount > 0;
}

void ConnectionInterface::on_receive(char *msg, int msglen)
{


}


bool ConnectionInterface::send_message_dash(int gear, double speed, double rmp, double temp, double soc, double  power, double fc, double fc_cumul, double distance, int engine_on, double time_in, int extra_control){
  if (time_in > last_updata) {
    data.gear = gear;
    data.speed = speed;
    data.rmp = rmp;
    data.avaliable_power = temp;
    data.fuelLevel = soc;
    data.power_in_out = power;
    data.fuel_consumption = fc_cumul;
    data.distance = distance;
    data.engineOn = engine_on;
    data.extra_control = extra_control;
    last_updata = time_in;
    memcpy(buffer, &data, data_size);
    return send(buffer, data_size);
  } 
  return false;
}


double ConnectionInterface::send_msg_plot(double * x_data, double * y_data, size_t x_size, size_t y_size, double time){
  char * buff = (char *)malloc((y_size + x_size)*sizeof(double));
  memcpy(buff, x_data, x_size*sizeof(double));
  memcpy(buff + x_size*sizeof(double), y_data, y_size*sizeof(double));
  send( buff, (y_size + x_size)*sizeof(double));
  return time;
}

int ConnectionInterface::getPort() {
  return fremote_port;
}
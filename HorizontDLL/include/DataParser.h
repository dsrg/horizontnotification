#pragma once
#include "Polyline.h"

#define PI 3.14159265
#define MAX_LINE_SZ 1024
#define H_MIN(X,Y)(((X)<(Y))?(X):(Y))

namespace Horizont_DLL {

  class DataParser
  {
  public:

    DataParser(const char * filename, double Cd, double A, double q, double m, double g, double f, double Pmax, double Tmax);
    ~DataParser();

    bool   get_pos_at_distance(double dist, double * x_out, double * y_out, double * z_out);
    double get_distance_at_position(double x, double y, double z, bool sealc_all = false, double max_range = 100);
   
    bool   get_index_and_ratio_at_distance(double distance, int * index_out, double * ratio_out);

    bool   get_all_at_distance(double distance, double * max_speed, double * recomended_speed, double * coasting_speed);
    bool   get_all_at_position(double x, double y, double z, bool sealc_all, double max_range, double * polyline_pos, double * max_speed, double * recomended_speed, double * coasting_speed);


    double get_max_speed_at_position(double x, double y, double z);
    double get_max_speed_at_distance(double distance);

    double get_recomended_speed_at_position(double x, double y, double z);
    double get_recomended_speed_at_distance(double distance);


	  double get_coasting_speed_at_position(double x, double y, double z);
	  double get_coasting_speed_at_distance(double distance);


    double get_max_distance(); // last value in distance array distance[rows];
    double get_last_speed();
    double get_first_speed();
	double get_last_speed_coasting();
	double get_first_speed_coasting();
    double get_min_distance();


    bool get_x_vec(double * x_out, int length);
    bool get_y_vec(double * y_out, int length);
    bool get_z_vec(double * z_out, int length);
    bool get_dist_vec(double * dist, int length);
    bool get_rec_speed_vec(double * rec_s_out, int length);
	bool get_coasting_speed_vec(double * rec_s_out, int length);
	bool get_max_speed_vec(double * max_s_out, int length);


    int get_rows();
    Polyline * get_polyline();
  private:
    Polyline * poly;
    double * distance;
    double * desired_speed_profile;
	double * coasting_speed_profile;
    double *x_coor, *y_coor, *z_coor, * max_speed;
    double Cd;
    double A;
    double q;
    double m;
    double g;
    double f;
    double Pmax;
    double Tmax;

    int rows;
    double value_at_ditance(double dist, double * vec);

    double v_comfort_speed(double radius, double superelevation);
    double critical_speed_spots(double value1, double value2, double value3);
    double initial_coasting_speed(double Slope, double V_coast_next, double Offset_next, double Offset_current, double Critical_Speed);
    double ice_coasting_speed(double Slope, double V_coast_next, double Offset_next, double Offset_current, double Critical_Speed);
    int read_from_file(int number_of_lines, const char * file, double * x, double * y, double *z, double * cureve_out, double * speed_max, double * super_elevation);
    int get_rows(const char * filename);
    




  };
}

#pragma once
#ifdef  __linux__ 
  #include <sys/socket.h>
  #include <netinet/in.h>
  #include <unistd.h>
  #define sleep_ms(x) usleep((x)*1000)
#else
  #include <winsock2.h>
  typedef int socklen_t;
  #include <Windows.h>
  #define sleep_ms(x) Sleep(x)
#endif


 typedef struct dataDashBoard {
    float speed;
    float rmp;
    float fuelLevel;
    float breakFluid;
    float avaliable_power;
    float oilLevel;
    float power_in_out;
    float fuel_consumption;
    float distance;
    int gear;
    char engineOn;
    char rightBlinkr;
    char leftBlinkr;
    char straterState;
    char ignitionState;
    char headLamp;
    char lowLamp;
    char handBreak;
    char horn;
    char fog_light;
    char park_light;
    int wipers;
    int clutch;
    int accelerator;
    int brake;
    unsigned long indicators;
    unsigned long extra_control;
  };


class Connector {
public:
  Connector(int port, const char *address, int bConnect, int sleep_ms);
  ~Connector();
  void send_message_dash(int gear, double speed, double rmp, double temp, double soc, double  power, double fc, double fc_cumul, double distance, int engine_on, double time_in, int extra_control);
  double send_msg_plot(double * x_data, double * y_data, size_t x_size, size_t y_size, double time);
protected:
  WSADATA        wsd;
  SOCKET         s;
  SOCKADDR_IN    recipient;
  BOOL bConnect;
  dataDashBoard data;
  char* buffer;
  int datasize;
  int sleep_ms;
  bool connected;
  double last_time_update;
};
#ifndef planetarium_interfacesH
#define planetarium_interfacesH


#include <Windows.h>


typedef struct dataDashBoard {
  float speed;
  float rmp;
  float fuelLevel;
  float breakFluid;
  float avaliable_power;
  float oilLevel;
  float power_in_out;
  float fuel_consumption;
  float distance;
  int gear;
  char engineOn;
  char rightBlinkr;
  char leftBlinkr;
  char straterState;
  char ignitionState;
  char headLamp;
  char lowLamp;
  char handBreak;
  char horn;
  char fog_light;
  char park_light;
  int wipers;
  int clutch;
  int accelerator;
  int brake;
  unsigned long indicators;
  unsigned long extra_control;
};


class udp_interface
// Zakladni trida pro udp komunikaci. Neni urcena pro vytvareni instanci teto tridy.
// Predstavuje vychozi rodicovskou tridu pro nasledujici tridy pouzivajici udp komunikaci
{
private:
	int nParams;
	SOCKET sock;
	HANDLE hreadthread;
	struct sockaddr_in remoteaddr;
	char *rcvbuf;
	int rcvbufsize;
	bool terminate;
	friend DWORD WINAPI udp_receive_thread( LPVOID owner );
	DWORD execute();
protected:
	virtual bool send( char *msg, int msglen );          // Odesle zpravu
  int flocal_port, fremote_port;

	// Nasledujici dve tridy budou pretypovany v potomcich teto tridy
	virtual void on_receive( char *msg, int msglen ) {}  // Vola se, pokud je prijata nejaka zprava
	virtual void on_socket_error( int errcode ) {}       // Vola se, pokud behem komunikace dojde k chybe

public:

	udp_interface( int local_port, int remote_port,const char *remote_addr, int bufsize, int params ); // Konstruktor
	~udp_interface();
	bool open();                                               // Otevre komunikaci
	bool close();                                              // Uzavre komunikaci
  void set(int local_port, int remote_port, const char *remote_addr, int bufsize);

};



class ConnectionInterface : public udp_interface
{
private:
	virtual void on_receive(char *msg, int msglen);
  dataDashBoard  data; // temporary container to recive the data from the simulator
  char * buffer;
  size_t data_size;
  int used_amount;
  double last_updata; // time of last update to prevent dummy data to be send.
public:
  void used_increase(int val);
  bool still_in_use();
  int getPort();
  ConnectionInterface(int portIn = 46000, int portOut = 45000,const char *remote_addr = "127.0.0.1");
  bool send_message_dash(int gear, double speed, double rmp, double temp, double soc, double  power, double fc, double fc_cumul, double distance, int engine_on, double time_in, int extra_control);
  double send_msg_plot(double * x_data, double * y_data, size_t x_size, size_t y_size, double time);

};


#endif

#ifndef HORIZONTDLL_H
#define HORIZONTDLL_H


#ifdef __cplusplus
extern "C" {
#endif


#if defined(_WIN32)
#  if defined (HorizontDLL_EXPORTS)
#    define HORIZONT_API __declspec(dllexport)
#  else
#    define HORIZONT_API __declspec(dllimport)
#  endif
#else
#  define HORIZONT_API
#endif  


  HORIZONT_API void * initialize_horizont(const char * filename, double Cd, double A, double q, double m, double g, double f, double Pmax, double Tmax);
  HORIZONT_API void   deinitialize_horizont(void* obj);
  HORIZONT_API double get_max_distance_horizont(void* obj);

  HORIZONT_API int    get_horizont_rows(void* obj);
  HORIZONT_API void   get_horizont_x_vec(void* obj, double * x_out, int length);
  HORIZONT_API void   get_horizont_y_vec(void* obj, double * y_out, int length);
  HORIZONT_API void   get_horizont_z_vec(void* obj, double * z_out, int length);
  HORIZONT_API void   get_horizont_dist_vec(void* obj, double * dist, int length);

  HORIZONT_API int    get_pos_at_distance(void* obj, double dist, double * x_out, double * y_out, double * z_out);
  HORIZONT_API int    get_pos_at_polyline_location(void* obj, double dist, double * pos, int size);

  HORIZONT_API double get_distance_at_position(void* obj ,double x, double y, double z, int search_all, double max_range);
  HORIZONT_API double get_max_speed_at_position(void* obj ,double x, double y, double z);
  HORIZONT_API double get_max_speed_at_distance(void* obj ,double distance);

  HORIZONT_API double get_recomended_speed_at_position(void* obj, double x, double y, double z);
  HORIZONT_API double get_recomended_speed_at_distance(void* obj, double distance);

  HORIZONT_API double get_coasting_speed_at_position(void* obj, double x, double y, double z);
  HORIZONT_API double get_coasting_speed_at_distance(void* obj, double distance);

  HORIZONT_API int get_all_at_position(void* obj, double x, double y, double z, int search_all, double max_range, double * pos_poly_out, double * max_speed_out, double * recomended_speed_out, double * coasting_speed_out);
  HORIZONT_API int get_all_at_distance(void* obj, double  pos_poly, double * max_speed_out, double * recomended_speed_out, double * coasting_speed_out);


  HORIZONT_API void * init_com(int port, const char *address, int bConnect, int sleep_ms);
  HORIZONT_API void send_msg(void* handl, int gear, double speed, double rmp, double temp, double soc, double  power, double fc, double fc_cumul, double distance, int engine_on, double time_in, int extra_control);
  HORIZONT_API double send_msg_plot(void* handl, double * x_data, double * y_data, size_t x_size, size_t y_size, double time);
  HORIZONT_API void close_com(void * handl);
#ifdef __cplusplus
}
#endif

#endif
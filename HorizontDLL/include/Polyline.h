#pragma once;
#include <vector>
#ifndef MIN
  #define MIN(X,Y)(((X)<(Y))?(X):(Y))
#endif // !MIN

#ifndef MAX
  #define MAX(X,Y)(((X)>(Y))?(X):(Y))
#endif // !MAX

namespace Horizont_DLL {
  class Point {
    // Getters and setters 
  public:
    Point() : x(0), y(0), z(0) {};
    Point(double x, double y, double z) : x(x), y(y), z(z){};
    ~Point(){};
    double get_x()                          { return x; };
    double get_y()                          { return y; };
    double get_z()                          { return z; };
    double set_x(double x)                  { this->x = x; };
    double set_y(double y)                  { this->y = y; };
    double set_z(double z)                  { this->z = z; };
    Point operator + (const Point& p)      { return Point(x + p.x, y + p.y, z + p.z); };
    Point operator - (const Point& p)      { return Point(x - p.x, y - p.y, z - p.z); };
    Point operator + (const double& p)     { return Point(x + p, y + p, z + p); };
    Point operator - (const double& p)     { return Point(x - p, y - p, z - p); };
    Point operator * (const double& p)     { return Point(x * p, y * p, z * p); };
    Point operator / (const double& p)     { return Point(x / p, y / p, z / p); };
    bool operator == (const Point& p)      { return x == p.x && y == p.y && z == p.z; };
  protected:
    double x;
    double y;
    double z;
  
  };


  class Vector : public Point {
    public:
      //Vector(const Point& p){ *this = p; }
      Vector(Point p) { x = p.get_x(); y = p.get_y(); z = p.get_z(); };
      Vector(double x, double y, double z) : Point(x,y,z){};
      ~Vector(){};
      // Methods
      double operator * (const Vector& p)  { return x*p.x + y*p.y + z*p.z; }  // DOT
      Vector cross(const Vector& p)         { return Vector(y * p.z - z * p.y, z * p.x - x * p.z, x * p.y - y * p.x); };   
      Vector normalize()                    { return (*this / MAX(0.0000001,this->length())); }
      double length()                       { return sqrt((x*x + y*y + z*z)); }

  };


  class Segment {
  public:
    Segment(const Point& start, const Point& end) : start(start), end(end) {};
    ~Segment() {};

    //Methods with implementations
    Vector to_vector()                     { return (end - start); };
    double length()                        { return to_vector().length(); }
    // Getters and setters
    Point get_start()                      { return start; }
    Point get_end()                        { return end; }
    void set_start(Point p)                { start = p; }
    void set_end(Point p)                  { end = p; }
    // Methods without implementation
    Point point_on_segment_closest_to( Point p);
    Point point_at_distance_along_vector(double d, bool forward = true);
    Point point_on_vector_closest_to(Point p);
  protected:
    Point start;
    Point end;
  
  };


  class Polyline {
  public:
    Polyline(double* x, double *y, double *z, int size);
    ~Polyline(){};
    Point closest_point_on_polyline(Point p, double * dist_from_start_out, double max_distance, bool search_all = true, bool interpolatae = true);
    Point point_at_index(int index);
    Point point_at_distance(double d);
    Segment segment_at_index(int index);
    int index_at_distance(double d);
    double distance_at_index(int index);
    double get_last_point();
    int get_last_index();
  protected:
    std::vector<Point> points;
    std::vector<double> distance;
    int size;
    double last_point; //last found distance based on polyline position 
    int last_index; //last closest index 
    Point last_lookup_point; // last lookup point
    Point last_lookup_result_point; // last lookup point

  };
}

cmake_minimum_required (VERSION 2.6)
set(project_name HORIZONT_TESTER)
project (${project_name})
remove_definitions(-DUNICODE -D_UNICODE)



set(SOURCES_CD
		src/horizont_dll_test.cpp
		src/stdafx.cpp
	)
		
		
set(HEADER_FILES_CD
		include/stdafx.h
		include/targetver.h
)



include_directories(include
					${CMAKE_SOURCE_DIR}/HorizontDLL/include
					)
					

add_executable (${project_name}  
				WIN32 
				${SOURCES_CD}
				${HEADER_FILES_CD}
				)

target_link_libraries(${project_name}
					  HorizontDLL)


set_target_properties(${project_name} PROPERTIES
                      RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/Distribution)	  
					   

					   

					   
set_target_properties(${project_name} PROPERTIES LINK_FLAGS_DEBUG "/SUBSYSTEM:CONSOLE")
set_target_properties(${project_name} PROPERTIES LINK_FLAGS_RELWITHDEBINFO "/SUBSYSTEM:CONSOLE")
set_target_properties(${project_name} PROPERTIES LINK_FLAGS_RELEASE "/SUBSYSTEM:CONSOLE")

					   
			
add_custom_command(TARGET ${project_name}
				   POST_BUILD
                   COMMAND ${CMAKE_COMMAND} -E copy_directory
                       ${CMAKE_SOURCE_DIR}/dependencies ${CMAKE_CURRENT_BINARY_DIR})		   
					    

					   
add_custom_command(TARGET ${project_name}
				   POST_BUILD
                   COMMAND ${CMAKE_COMMAND} -E copy_directory
                       ${CMAKE_SOURCE_DIR}/dependencies $<TARGET_FILE_DIR:${project_name}>)
					   
install (TARGETS ${project_name}
         RUNTIME DESTINATION ${CMAKE_BINARY_DIR}/_install)
		 
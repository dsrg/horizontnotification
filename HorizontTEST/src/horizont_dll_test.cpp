// FMISimulator.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "HorizontDLL.h"
#include <iostream>





int _tmain(int argc, _TCHAR* argv[])
{



  double mass = 1680;
  double gravity = 9.81;
  double rolling_resistance_coeff = 0.015;
  double aero_resistance_coeff = 0.32;
  double speed_cut_in = 0.01;
  double air_density = 1.293;
  double frontal_area = 2.4;
  double pMax = 2500;
  double tMax = 100;
  double wheel_radius = 0.32;


  void * dp = initialize_horizont("C:/Users/dr14/Documents/Repositories/simulator_all_in_one/dependencies/road_complete_speed_limits.txt", aero_resistance_coeff, frontal_area, air_density, mass, gravity, rolling_resistance_coeff, pMax, tMax);
  void * cn = init_com(47000, "127.0.0.1", 0, 0);
  void * cn1 = init_com(47000, "127.0.0.1", 0, 0);
  void * cn2 = init_com(47000, "127.0.0.1", 0, 0);

  close_com(cn1);
  close_com(cn2);


  double current_desired_velocity,x,y,z;
  double current_coasting_velocity;
  double max_distance = get_max_distance_horizont(dp);
  int stat;
  double max_speed_dist ,max_speed_pos;
  double rec_speed_dist, rec_speed_pos;
  double coasting_speed_dist, coasting_speed_pos;
  double p_out;
  


  //get_all_at_position(dp, 0, 0, 0, true, 100, &p_out,&max_speed_pos, &rec_speed_pos, &coasting_speed_pos );
  for (double d = 0; d < 0; d += 0.1) {
    send_msg(cn, 1, d, 800, 90, 1, 1, 0, 10, 999, 0, d, 0);
    get_pos_at_distance(dp, d, &x, &y, &z);
    get_all_at_position(dp, x, y, z, d == 0, 100, &p_out, &max_speed_pos, &rec_speed_pos, &coasting_speed_pos);

    double d_calc = get_distance_at_position(dp, x, y, z, true, 100);
    double error = d - d_calc;
    double error2 = d - p_out;

    max_speed_dist = get_max_speed_at_distance(dp, d);
    max_speed_pos = get_max_speed_at_position(dp, x, y, z);
    rec_speed_dist = get_recomended_speed_at_distance(dp, d);
    rec_speed_pos = get_recomended_speed_at_position(dp, x, y, z);
	  coasting_speed_dist = get_coasting_speed_at_distance(dp, d);
	  coasting_speed_pos = get_coasting_speed_at_position(dp, x, y, z);

    current_desired_velocity = get_recomended_speed_at_distance(dp,d);
	  current_coasting_velocity = get_coasting_speed_at_distance(dp, d);
    stat = get_pos_at_distance(dp, d, &x, &y, &z);
    std::cout << d << " " << current_desired_velocity << " " << x << " " << y <<" " << z <<  std::endl;
	  std::cout << d << " " << current_coasting_velocity << " " << x << " " << y << " " << z << std::endl;
  }
  delete dp;
}
 

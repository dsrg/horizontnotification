# --- This is a configuration file to build a library for IGNITE. ------------------------------------------------------
#
# It is not meant to be executed directly, instead execute with:
#
# ignite_user_lib_make <this_file.py>
# ----------------------------------------------------------------------------------------------------------------------


# The name of the library (must be a legal filename with no spaces)
lib_name = "HorizontNotification"

# List of Modelica packages/classes to load.  Must include every class that is used by any class in <classes>
# Any package ending in .* will recursively select all contained packages/classes.
packages =  [
              "Modelica.SIunits",
              "HorizontNotification.*",
              "Modelica.Constants.*",
			  "Modelica.Blocks.*",
			  "IStandard.Interpolation.*",
			  "IStandard.Functions.*",
			  "IStandard.Constants.*",
              "Modelica.Blocks.Interfaces.*",
              "Modelica.SIunits.Conversions.NonSIunits",
			  "IPowertrain.Controllers.Internal.*",
			  "IPowertrain.Interfaces.*",
			  "IStandard.Types.*",
			  "IStandard.Constants.*",
			  "Modelica.Blocks.Math.*",
			  "Modelica.Blocks.Routing.*"
            ]

# List of Modelica packages/classes to show in the IGNITE GUI.
# Any package ending in .* will recursively select all contained packages/classes.
classes =   [
              "HorizontNotification.Notification.PositionBased",
              "HorizontNotification.Notification.LocationBased",
			  "HorizontNotification.Controller.HybridVehicleController_Horizon",
			  "HorizontNotification.Controller.ElectricVehicleController"
            ]

# Dictionary mapping Modelica libraries to versions to use.
use_lib_versions = {"Modelica" : "3.2.1"}

output_message_level = "warning"  # default is "error" other options are "none", "critical", "info", "debug", "all"
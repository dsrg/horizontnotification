within HorizontNotification.Motorcycle.Suspension;
model ScisorSuspension
  Modelica.Mechanics.MultiBody.Parts.BodyCylinder bodyCylinder(r={0,0.5,0})
    annotation (Placement(transformation(extent={{-10,32},{10,52}})));
  Modelica.Mechanics.MultiBody.Parts.FixedTranslation fixedTranslation(r={0,0.5,
        0}) annotation (Placement(transformation(extent={{22,30},{42,50}})));
  Modelica.Mechanics.MultiBody.Joints.Revolute revolute(n={0,0,1}, phi(fixed=
          true, start=0.17453292519943)) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={40,8})));
  Modelica.Mechanics.MultiBody.Parts.BodyCylinder bodyCylinder1(r={0,0.5,0})
    annotation (Placement(transformation(extent={{-14,-40},{6,-20}})));
  Modelica.Mechanics.MultiBody.Joints.Assemblies.JointUPS jointUPS annotation (
      Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=90,
        origin={-18,2})));
  IMoved.Chassis.VehicleParts.SubComponents.SpringDampers.NonlinearSpringDamper
    nonlinearSpringDamper(
    b=1,
    s_unstretched=0.5,
    c=1000,
    d=2000) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-64,0})));
  Modelica.Mechanics.MultiBody.Parts.BodyCylinder bodyCylinder2(r={0,0.5,0})
    annotation (Placement(transformation(extent={{-62,-44},{-42,-24}})));
  IMoved.WheelsTires.MagicTire02.WheelPac02 wheelPac02_1(val_eps=0.3)
    annotation (Placement(transformation(extent={{-64,-70},{-44,-50}})));
  inner IMoved.Roads.FlatRoad road
    annotation (Placement(transformation(extent={{-94,72},{-74,92}})));
  Modelica.Mechanics.MultiBody.Parts.FixedTranslation fixedTranslation1(r={0,-0.5,
        0}) annotation (Placement(transformation(extent={{60,30},{80,50}})));
  IMoved.WheelsTires.MagicTire02.WheelPac02 wheelPac02_2(val_eps=0.3)
    annotation (Placement(transformation(extent={{92,-72},{112,-52}})));
  inner Modelica.Mechanics.MultiBody.World world(n={0,0,-1})
    annotation (Placement(transformation(extent={{146,58},{126,78}})));
equation
  connect(fixedTranslation.frame_a, bodyCylinder.frame_b) annotation (Line(
      points={{22,40},{16,40},{16,42},{10,42}},
      color={95,95,95},
      thickness=0.5));
  connect(revolute.frame_b, fixedTranslation.frame_a) annotation (Line(
      points={{40,18},{40,32},{22,32},{22,40}},
      color={95,95,95},
      thickness=0.5));
  connect(revolute.frame_a, bodyCylinder1.frame_b) annotation (Line(
      points={{40,-2},{14,-2},{14,-30},{6,-30}},
      color={95,95,95},
      thickness=0.5));
  connect(bodyCylinder1.frame_a, jointUPS.frame_a) annotation (Line(
      points={{-14,-30},{-16,-30},{-16,-18},{-18,-18}},
      color={95,95,95},
      thickness=0.5));
  connect(jointUPS.frame_b, bodyCylinder.frame_a) annotation (Line(
      points={{-18,22},{-14,22},{-14,42},{-10,42}},
      color={95,95,95},
      thickness=0.5));
  connect(nonlinearSpringDamper.frame_a, jointUPS.frame_ib) annotation (Line(
      points={{-64,10},{-52,10},{-52,18},{-38,18}},
      color={95,95,95},
      thickness=0.5));
  connect(nonlinearSpringDamper.frame_b, jointUPS.frame_ia) annotation (Line(
      points={{-64,-10},{-52,-10},{-52,-14},{-38,-14}},
      color={95,95,95},
      thickness=0.5));
  connect(bodyCylinder2.frame_b, jointUPS.frame_a) annotation (Line(
      points={{-42,-34},{-30,-34},{-30,-18},{-18,-18}},
      color={95,95,95},
      thickness=0.5));
  connect(wheelPac02_1.carrier_frame, bodyCylinder2.frame_a) annotation (Line(
      points={{-64,-60},{-64,-34},{-62,-34}},
      color={95,95,95},
      thickness=0.5));
  connect(fixedTranslation1.frame_a, fixedTranslation.frame_b) annotation (Line(
      points={{60,40},{42,40}},
      color={95,95,95},
      thickness=0.5));
  connect(wheelPac02_2.carrier_frame, fixedTranslation1.frame_b) annotation (
      Line(
      points={{92,-62},{86,-62},{86,40},{80,40}},
      color={95,95,95},
      thickness=0.5));
  connect(world.frame_b, fixedTranslation.frame_a) annotation (Line(
      points={{126,68},{74,68},{74,40},{22,40}},
      color={95,95,95},
      thickness=0.5));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end ScisorSuspension;

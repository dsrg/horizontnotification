within HorizontNotification.Notification;
model PositionBased
  extends HorizontNotification.Notification.BaseModel;

  parameter SI.Distance max_d = 200 "Maximal distance point lookup" annotation (Dialog(group="Base curve parameters"));
  parameter Boolean interpolate = true "Interpolate if not in range" annotation (Dialog(group="Base curve parameters"));
  Modelica.Blocks.Interfaces.RealInput position[3] "Actual position" annotation (Placement(
        transformation(extent={{-126,-20},{-86,20}}),
                                                    iconTransformation(extent={{106,-8},
            {88,10}})));
  Modelica.Blocks.Interfaces.RealOutput polyline_position
    "Diff between actual position and point on track" annotation (Placement(
        transformation(extent={{94,70},{114,90}}), iconTransformation(extent={{96,-90},
            {116,-70}})));


protected
  Boolean status;
  Real tmp;


  // Direct transformation matrix to move and rotate curve
  //final IMoved.Roads.Path.Internal.VisualizeTablePath path(final poly=poly,final width=width,final offset=offset);
algorithm
  (status, polyline_position,max_speed, recomended_speed, coasting_speed)  :=
    HorizontNotification.Internal.HorizontFunctions.get_all_at_position(h_obj,
     position[1],position[2],position[3],time < 0.03, max_d);
equation
    tmp = polyline_position;

    annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,34},{100,-28}},
          lineColor={28,108,200},
          fillColor={175,175,175},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-98,2},{-82,0}},
          lineColor={255,255,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-56,2},{-40,0}},
          lineColor={255,255,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{28,2},{44,0}},
          lineColor={255,255,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{78,2},{94,0}},
          lineColor={255,255,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-16,2},{0,0}},
          lineColor={255,255,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-78,18},{-70,10}},
          lineColor={0,0,0},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-12,20},{-4,12}},
          lineColor={0,0,0},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{56,-6},{64,-14}},
          lineColor={0,0,0},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{88,26},{96,18}},
          lineColor={0,0,0},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Line(points={{-72,14},{-8,16},{60,-10},{92,22}},    color={0,0,0})}),
                                                                 Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end PositionBased;

within HorizontNotification.Notification;
partial model BaseModel
  import Modelica.Constants.pi;
  parameter String filename="E:/modelica_input_file/inputdat.txt" annotation (Dialog(
      loadSelector(filter="Text files (*.txt);;MATLAB MAT-files (*.mat)",
          caption="Open file in which table is present")));
  parameter SI.Mass mass=1680.0 "Mass";
  parameter Real f=0.015 "Rolling resistance coefficient";
  parameter SI.Density q=1.293 "Air density";
  parameter SI.Area A=2.4 "Frontal area";
  parameter Real Cd=0.32 "Drag coefficient";
  parameter SI.Acceleration g=9.81 "Gravity";
  parameter Real Pmax=25000 "MAX Generator Power";
  parameter Real Tmax=100 "MAX Generator Torque";
    Modelica.Blocks.Interfaces.RealOutput recomended_speed "Recomended speed"
    annotation (Placement(transformation(extent={{98,12},{118,32}}),
        iconTransformation(extent={{96,-60},{116,-40}})));
  Modelica.Blocks.Interfaces.RealOutput coasting_speed "Coasting speed"
    annotation (Placement(transformation(extent={{98,-30},{118,-10}}),
        iconTransformation(extent={{96,70},{116,90}})));

  Modelica.Blocks.Interfaces.RealOutput max_speed
          "Maximal speed" annotation (
      Placement(transformation(extent={{96,-90},{116,-70}}), iconTransformation(
          extent={{96,40},{116,60}})));
  Modelica.Blocks.Interfaces.RealInput current_speed
    annotation (Placement(transformation(extent={{-130,-20},{-90,20}})));
  Modelica.Blocks.Interfaces.BooleanOutput message_active = msg_status
    annotation (Placement(transformation(extent={{-6,-110},{14,-90}}),
        iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={0,-100})));

protected
  Boolean msg_status(start = false);
  final parameter  HorizontNotification.Internal.HorizontFunctions.Horizont_2020 h_obj=
      HorizontNotification.Internal.HorizontFunctions.Horizont_2020(
      filename= filename,
      Cd = Cd,
      A = A,
      q= q,
      m = mass,
      g = g,
      f = f,
      Pmax = Pmax,
      Tmax = Tmax);

  final parameter Integer m = HorizontNotification.Internal.HorizontFunctions.get_num_rows(h_obj);

algorithm
  when ( current_speed > coasting_speed and not pre(msg_status)) then
    msg_status :=true;
       elsewhen
            (coasting_speed > (recomended_speed + 0.1 * recomended_speed)  and  pre(msg_status) and current_speed < coasting_speed) then
    msg_status :=false;
  end when;


  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end BaseModel;

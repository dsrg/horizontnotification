within ;
/*==============================================================================
* Description   : IGNITE Powertrain Library
* Filename      : IPowertrain/package.mo
* Creation Date : 02 Apr 2012
*===============: Copyright (c) 2012 Ricardo Software ==========================
*/
package HorizontNotification 
  import SI = Modelica.SIunits;
  import NSI = Modelica.SIunits.Conversions.NonSIunits;

  annotation (uses(Modelica(version="3.2.2"), IMoved(version="0.0.1"),
    IStandard(version="1.0.0"),
    IPowertrain(version="1.0.0")));
end HorizontNotification;

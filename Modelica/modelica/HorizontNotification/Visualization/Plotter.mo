within HorizontNotification.Visualization;
model Plotter
  extends HorizontNotification.Visualization.Internal.basic_connection;
  parameter String plot_titlename = "Title" "Plot title";
  parameter Integer n=1 "Amount of graphs";
  parameter Real[n] mult = {1} "Multiplier";
  parameter Real[n] add = {0} "Addition";
  parameter String[n] name = {"Plot_name"} "Graph name";

  parameter String launcherName="NoName" "Plotter application"
    annotation (Dialog(
      group="Table data definition",
      enable=tableOnFile,
      loadSelector(filter="Plotter (*.exe)",
      caption="Application to run a plotter")));
  Modelica.Blocks.Interfaces.RealInput u[n]
    annotation (Placement(transformation(extent={{-130,-20},{-90,20}})));
  Modelica.Blocks.Interfaces.RealInput x_axis
    annotation (Placement(transformation(extent={{-20,-20},{20,20}},
        rotation=90,
        origin={0,-124}), iconTransformation(
        extent={{-20,-20},{20,20}},
        rotation=90,
        origin={0,-124})));
  Real[n] data_x;
  Real[n] data_y;

algorithm
  when sample(startTime, samplePeriod) then
    out := HorizontNotification.Visualization.Internal.send_data_to_plot(
      hand=conn,
      x_data=data_x,
      y_data=data_y,
      _time_in=time);
  end when;
equation
   data_x = fill(x_axis,n);
    data_y = u.*mult+add;

  annotation (__RModel(
  element="  <panel>
    <tabs>
      <tab name='General'>
        <pane>
            <scalar ref='n'/>
            <collapsible frame='false' expanded='true' name='Input table' sizepolicy='minimumexpanding' fillSpaceAfterWidgets='true'>
            <line ref='plot_titlename'/>
            <table id='table_n' title='' sizeGrip='vertical' sizepolicy='minimumexpanding' size_ref='n' resizeable='true' style='1D'>
            <data ref='mult' resizeMode='resizeToContents' />
                <data ref='add' resizeMode='resizeToContents'/>
                <data ref='name' resizeMode='stretch'/>
              </table>
            </collapsible>
            <group name='Network settings'>
              <scalar ref='port'/>
              <line ref='adress'/>
              <scalar ref='bConnect'/>
              <scalar ref='sleepTime'/>
            </group>
            <group name='Component sampling'>
              <scalar ref='samplePeriod'/>
              <scalar ref='startTime'/>
              </group>
              <path ref='launcherName' editbutton='true' filter='Plotter (*.exe)' id='launcherName__desc'/>
              <button event-click='gauge-clicked' id='CalculateButton' text='Open Plot'/>
    
            <spacer expandVertically='true'/>
          </pane>
        </tab>
      </tabs>
  </panel>
  <events>
    <event name='gauge-clicked'>
      <calculate disable_caching='true' function='open_plot' input_units='' inputs='port,plot_titlename,name,launcherName' output_units='' outputs='' undoMode='snapshot'/>
    </event>
  </events>
  <python><![CDATA[
import sys
import os
from subprocess import Popen, PIPE
def open_plot(port,plot_titlename,names,path_to_exe):
    path_to_exe = path_to_exe[0]
    prev_dir = os.getcwd()
    gauge_dir = os.path.dirname(path_to_exe)
    os.chdir(gauge_dir)
    cmd = [os.path.basename(path_to_exe),plot_titlename, str(port)]
    cmd.extend(names)
    print ' '.join(cmd)
    p1 = Popen(cmd, stdout=PIPE, shell=True)
    os.chdir(prev_dir)
]]></python>
  
  "),Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-120},{
            100,100}}), graphics={
        Rectangle(
          extent={{-82,94},{-78,-110}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-92,-98},{90,-102}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-80,100},{-90,80},{-70,80},{-80,100}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{100,-100},{80,-90},{80,-110},{100,-100}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Line(
          points={{-70,-72},{-52,-14},{-14,-26},{16,4},{44,-4},{70,32}},
          color={0,0,255},
          smooth=Smooth.Bezier,
          thickness=1)}),                               Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-100,-120},{100,
            100}})));
end Plotter;

within HorizontNotification.Visualization;
model Dashboard
  extends HorizontNotification.Visualization.Internal.basic_connection;
  parameter Boolean use_speed = true "Use Speed" annotation(Dialog(tab="General", group="Avaliable inputs"));
  parameter Boolean use_rmp = true "Use RPM" annotation(Dialog(tab="General", group="Avaliable inputs"));
  parameter Boolean use_temp = true "Use Temperature" annotation(Dialog(tab="General", group="Avaliable inputs"));
  parameter Boolean use_soc= true "Use SoC" annotation(Dialog(tab="General", group="Avaliable inputs"));
  parameter Boolean use_fc= true "Use Fuel Cunsumption" annotation(Dialog(tab="General", group="Avaliable inputs"));

  parameter Boolean  use_gear = true "Use Gear" annotation(Dialog(tab="General", group="Avaliable inputs"));
  parameter Boolean use_power= true "Ose Power In/Out"
                                                      annotation(Dialog(tab="General", group="Avaliable inputs"));
  parameter Boolean use_engine_on= true "Use engin on/off"
                                                          annotation(Dialog(tab="General", group="Avaliable inputs"));
  parameter Boolean  use_indicators = true "Use Indicators" annotation(Dialog(tab="General", group="Avaliable inputs"));

  Modelica.Blocks.Interfaces.RealInput speed_kmh if
                                                   use_speed annotation (
      Placement(transformation(extent={{-134,52},{-94,92}}), iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-82,-60})));
  Modelica.Blocks.Interfaces.IntegerInput gear if use_gear
    annotation (Placement(transformation(extent={{-138,-124},{-98,-84}}),
        iconTransformation(
        extent={{-11,-11},{11,11}},
        rotation=90,
        origin={81,-59})));
  Modelica.Blocks.Interfaces.RealInput rpm_revpermin if
                                              use_rmp annotation (Placement(
        transformation(extent={{-134,22},{-94,62}}), iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-56,-60})));
  Modelica.Blocks.Interfaces.RealInput temp if use_temp
    annotation (Placement(transformation(extent={{-136,-94},{-96,-54}}),
        iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-30,-60})));
  Modelica.Blocks.Interfaces.RealInput SoC if use_soc
    annotation (Placement(transformation(extent={{-136,-64},{-96,-24}}),
        iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={22,-60})));
  Modelica.Blocks.Interfaces.RealInput fc if use_fc
    annotation (Placement(transformation(extent={{-134,-36},{-94,4}}),
        iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={50,-60})));
  Modelica.Blocks.Interfaces.RealInput powe_in_out if use_power
    annotation (Placement(
        transformation(extent={{-134,-6},{-94,34}}), iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-4,-60})));
  Modelica.Blocks.Interfaces.BooleanInput engine_on if use_engine_on
    annotation (Placement(transformation(extent={{-132,-20},{-92,20}}),
        iconTransformation(extent={{-116,-12},{-92,12}})));
  Modelica.Blocks.Interfaces.RealOutput distance
    annotation (Placement(transformation(extent={{94,10},{114,30}}),
        iconTransformation(extent={{94,10},{114,30}})));
  Modelica.Blocks.Interfaces.RealOutput fc_cumul
    annotation (Placement(
        transformation(extent={{94,-30},{114,-10}}), iconTransformation(extent={{94,-30},
            {114,-10}})));
  Modelica.Blocks.Interfaces.IntegerInput indicator if
                                                  use_indicators annotation (
      Placement(transformation(extent={{-138,-150},{-98,-110}}),
        iconTransformation(
        extent={{-11,-11},{11,11}},
        rotation=0,
        origin={-105,-29})));
protected
    Modelica.Blocks.Sources.Constant speed_zero(k=0) if not use_speed
    annotation (Placement(transformation(extent={{-80,64},{-74,70}})));
    Modelica.Blocks.Routing.RealPassThrough speed_int
    annotation (Placement(transformation(extent={{-56,68},{-48,76}})));
    Modelica.Blocks.Sources.IntegerConstant gear_zero(k=0) if not use_gear
    annotation (Placement(transformation(extent={{-84,-114},{-76,-106}})));
    Modelica.Blocks.Routing.IntegerPassThrough gear_int
    annotation (Placement(transformation(extent={{-54,-108},{-46,-100}})));
    Modelica.Blocks.Routing.RealPassThrough temp_int
    annotation (Placement(transformation(extent={{-54,-78},{-46,-70}})));
    Modelica.Blocks.Sources.Constant temp_zero(k=0) if not use_temp
    annotation (Placement(transformation(extent={{-88,-84},{-80,-76}})));
    Modelica.Blocks.Routing.RealPassThrough soc_int
    annotation (Placement(transformation(extent={{-54,-48},{-46,-40}})));
    Modelica.Blocks.Sources.Constant soc_zero(k=0) if not use_soc
    annotation (Placement(transformation(extent={{-84,-54},{-76,-46}})));
    Modelica.Blocks.Sources.Constant rmp_zero1(k=0) if   not use_rmp
    annotation (Placement(transformation(extent={{-84,32},{-76,40}})));
    Modelica.Blocks.Routing.RealPassThrough rmp_int
    annotation (Placement(transformation(extent={{-56,38},{-48,46}})));
    Modelica.Blocks.Routing.RealPassThrough fc_int
    annotation (Placement(transformation(extent={{-54,-20},{-46,-12}})));
    Modelica.Blocks.Sources.Constant fc_zero(k=0) if not use_fc
    annotation (Placement(transformation(extent={{-82,-28},{-74,-20}})));
    Modelica.Blocks.Sources.Constant power_in_out_zero(k=0) if not use_power
    annotation (Placement(transformation(extent={{-92,2},{-84,10}})));
    Modelica.Blocks.Routing.RealPassThrough power_in_out_int
    annotation (Placement(transformation(extent={{-56,10},{-48,18}})));
    Modelica.Blocks.Continuous.Integrator distance_integrator
    annotation (Placement(transformation(extent={{42,56},{62,76}})));
    Modelica.Blocks.Continuous.Integrator fc_integrator
    annotation (Placement(transformation(extent={{38,-26},{58,-6}})));
    Modelica.Blocks.Routing.BooleanPassThrough engine_on_int
    annotation (Placement(transformation(extent={{-54,102},{-46,110}})));
    Modelica.Blocks.Sources.BooleanConstant engine_on_zero(k=true) if  not use_engine_on
    annotation (Placement(transformation(extent={{-80,86},{-72,94}})));


protected
    Modelica.Blocks.Routing.IntegerPassThrough indicators_int
    annotation (Placement(transformation(extent={{-58,-132},{-50,-124}})));
    Modelica.Blocks.Sources.IntegerConstant indicators_zero(k=0) if
                                                              not use_indicators
    annotation (Placement(transformation(extent={{-90,-148},{-82,-140}})));

    Modelica.Blocks.Math.Gain gain(k=1/(3.6*1000))
    annotation (Placement(transformation(extent={{-8,56},{12,76}})));
algorithm
  when sample(startTime, samplePeriod) then
    out := HorizontNotification.Visualization.Internal.send_data(
      hand=conn,
      gear=gear_int.y,
      speed=speed_int.y,
      rmp=rmp_int.y,
      soc=soc_int.y,
      power=power_in_out_int.y,
      temp=temp_int.y,
      fc=fc_int.y,
      fc_cumul=fc_cumul,
      distance=distance,
      engine_on=engine_on_int.y,
      time_in=time,
      indicators=indicators_int.y);
  end when;

equation
  connect(distance_integrator.y, distance)
    annotation (Line(points={{63,66},{63,20},{104,20}}, color={0,0,127}));
  connect(fc_integrator.y, fc_cumul)
    annotation (Line(points={{59,-16},{104,-16},{104,-20}}, color={0,0,127}));
  connect(speed_zero.y, speed_int.u) annotation (Line(points={{-73.7,67},{-68,67},
          {-68,72},{-56.8,72}}, color={0,0,127}));
  connect(speed_kmh, speed_int.u)
    annotation (Line(points={{-114,72},{-56.8,72}}, color={0,0,127}));
  connect(rpm_revpermin, rmp_int.u)
    annotation (Line(points={{-114,42},{-56.8,42}}, color={0,0,127}));
  connect(rmp_zero1.y, rmp_int.u) annotation (Line(points={{-75.6,36},{-62,36},{
          -62,42},{-56.8,42}}, color={0,0,127}));
  connect(powe_in_out, power_in_out_int.u)
    annotation (Line(points={{-114,14},{-56.8,14}}, color={0,0,127}));
  connect(power_in_out_zero.y, power_in_out_int.u) annotation (Line(points={{-83.6,
          6},{-74,6},{-74,14},{-56.8,14}}, color={0,0,127}));
  connect(fc, fc_int.u)
    annotation (Line(points={{-114,-16},{-54.8,-16}}, color={0,0,127}));
  connect(fc_int.y, fc_integrator.u)
    annotation (Line(points={{-45.6,-16},{36,-16}}, color={0,0,127}));
  connect(fc_zero.y, fc_int.u) annotation (Line(points={{-73.6,-24},{-64,-24},{-64,
          -16},{-54.8,-16}}, color={0,0,127}));
  connect(SoC, soc_int.u)
    annotation (Line(points={{-116,-44},{-54.8,-44}}, color={0,0,127}));
  connect(soc_zero.y, soc_int.u) annotation (Line(points={{-75.6,-50},{-66,-50},
          {-66,-44},{-54.8,-44}}, color={0,0,127}));
  connect(temp, temp_int.u)
    annotation (Line(points={{-116,-74},{-54.8,-74}}, color={0,0,127}));
  connect(temp_zero.y, temp_int.u) annotation (Line(points={{-79.6,-80},{-70,-80},
          {-70,-74},{-54.8,-74}}, color={0,0,127}));
  connect(gear, gear_int.u)
    annotation (Line(points={{-118,-104},{-54.8,-104}}, color={255,127,0}));
  connect(gear_zero.y, gear_int.u) annotation (Line(points={{-75.6,-110},{-68,-110},
          {-68,-104},{-54.8,-104}}, color={255,127,0}));

  connect(engine_on_zero.y, engine_on_int.u) annotation (Line(points={{-71.6,90},
          {-62,90},{-62,106},{-54.8,106}}, color={255,0,255}));
  connect(engine_on, engine_on_int.u) annotation (Line(points={{-112,0},{-54.8,0},
          {-54.8,106}},      color={255,0,255}));
  connect(indicators_zero.y, indicators_int.u) annotation (Line(points={{-81.6,-144},
          {-70,-144},{-70,-128},{-58.8,-128}}, color={255,127,0}));
  connect(indicators_int.u, indicator) annotation (Line(points={{-58.8,-128},{-86,
          -128},{-86,-130},{-118,-130}}, color={255,127,0}));
  connect(distance_integrator.u, gain.y)
    annotation (Line(points={{40,66},{13,66}}, color={0,0,127}));
  connect(gain.u, speed_int.y) annotation (Line(points={{-10,66},{-30,66},{-30,72},
          {-47.6,72}}, color={0,0,127}));
annotation (__RModel(
  element=" <panel>
    <tabs>
      <tab name='General'>
        <pane>
          <group name='Avaliable inputs'>
            <checkbox ref='use_speed' labelSide='left'/>
            <checkbox ref='use_rmp' labelSide='left'/>
            <checkbox ref='use_temp' labelSide='left'/>
            <checkbox ref='use_soc' labelSide='left'/>
            <checkbox ref='use_fc' labelSide='left'/>
            <checkbox ref='use_gear' labelSide='left'/>
            <checkbox ref='use_power' labelSide='left'/>
            <checkbox ref='use_engine_on' labelSide='left'/>
            <checkbox ref='use_indicators' labelSide='left'/>


          </group>
          <group name='Network settings'>
            <scalar ref='port'/>
            <line ref='adress'/>
            <scalar ref='bConnect'/>
            <button event-click='gauge-clicked' id='CalculateButton' text='Open Gauge'/>

          </group>
          <group name='Component sampling'>
            <scalar ref='samplePeriod'/>
            <scalar ref='startTime'/>
            </group>
            
          <spacer expandVertically='true'/>
        </pane>
      </tab>
    </tabs>
  </panel>
  <events>
    <event name='gauge-clicked'>
      <calculate disable_caching='true' function='open_gauge' input_units='' inputs='port' output_units='' outputs='' undoMode='snapshot'/>
    </event>
  </events>
  <python><![CDATA[
from run_demonstrator import open_gauge
]]></python>
  
  "),
  Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-60},
            {100,80}}),                                         graphics={
        Polygon(
          points={{-100,-48},{-100,30},{-58,70},{60,70},{100,30},{100,-50},{-100,
              -50},{-100,-48}},
          lineColor={28,108,200},
          fillColor={175,175,175},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-92,38},{-12,-42}},
          lineColor={28,108,200},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{8,40},{88,-40}},
          lineColor={28,108,200},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{-56,-6},{-68,32},{-48,-2},{-52,-4},{-56,-6}},
          lineColor={255,255,0},
          fillColor={255,255,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{46,-2},{34,36},{54,2},{50,0},{46,-2}},
          lineColor={255,255,0},
          fillColor={255,255,0},
          fillPattern=FillPattern.Solid)}), Diagram(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-60},{100,80}})));
end Dashboard;

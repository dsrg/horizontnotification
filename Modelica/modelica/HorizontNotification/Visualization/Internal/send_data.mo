within HorizontNotification.Visualization.Internal;
function send_data
  input HorizontNotification.Visualization.Internal.Connector hand;
  input Integer gear;
  input Real speed;
  input Real rmp;
  input Real soc;
  input Real power;
  input Real temp;
  input Real fc;
  input Real fc_cumul;
  input Real distance;
  input Boolean engine_on;
  input Real time_in;
  input Integer indicators;

  output Real out_d = time_in;

  external "C" send_msg(hand,gear,speed,rmp,temp,soc, power,fc,fc_cumul,distance,engine_on,time_in,indicators)
   annotation(Library="HorizontDLL",
               Include="#include \"HorizontDLL.h\"",
               IncludeDirectory=
        "modelica://HorizontNotification/Resources/Include",
               LibraryDirectory=
        "modelica://HorizontNotification/Resources/Library");
end send_data;

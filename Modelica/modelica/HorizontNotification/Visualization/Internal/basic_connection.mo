within HorizontNotification.Visualization.Internal;
partial model basic_connection
  parameter Integer port = 5150 "Port" annotation(Dialog(tab="General", group="Network settings"));
  parameter String adress =  "127.0.0.1" "IP or host name" annotation(Dialog(tab="General", group="Network settings"));
  parameter Integer bConnect = 0 "Connect" annotation(Dialog(tab="General", group="Network settings"));
  parameter SI.Time sleepTime(min = 0) = 0 "Sleep_time" annotation(Dialog(tab="General", group="Network settings"));

  parameter Modelica.SIunits.Time samplePeriod(min=100*Modelica.Constants.eps, start=0.1) = 0.1 "Sample period of component" annotation(Dialog(tab="General",group = "Component sampling"));
  parameter Modelica.SIunits.Time startTime=0 "First sample time instant" annotation(Dialog(tab="General",group = "Component sampling"));
  Real out;

protected
  final parameter HorizontNotification.Visualization.Internal.Connector conn=HorizontNotification.Visualization.Internal.Connector(
      port=port,
      adress=adress,
      bConn=bConnect,
      sleep_ms=integer(floor(sleepTime*1000 + 0.5)));

  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end basic_connection;

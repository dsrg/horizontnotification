within HorizontNotification.Visualization.Internal;
class Connector
  extends ExternalObject;
  function constructor
    input Integer port = 5150;
    input String adress = "";
    input Integer bConn = 0;
    input Integer sleep_ms = 10;
    output Connector hand;

    external "C" hand = init_com(port,adress,bConn,sleep_ms)
    annotation(Library="HorizontDLL",
               Include="#include \"HorizontDLL.h\"",
               IncludeDirectory=
        "modelica://HorizontNotification/Resources/Include",
               LibraryDirectory=
        "modelica://HorizontNotification/Resources/Library");
  end constructor;

  function destructor
    input Connector hand;

    external "C" close_com(hand)
    annotation(Library="HorizontDLL",
               Include="#include \"HorizontDLL.h\"",
               IncludeDirectory=
        "modelica://HorizontNotification/Resources/Include",
               LibraryDirectory=
        "modelica://HorizontNotification/Resources/Library");
  end destructor;

end Connector;

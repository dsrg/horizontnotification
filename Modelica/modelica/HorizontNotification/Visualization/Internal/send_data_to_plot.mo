within HorizontNotification.Visualization.Internal;
function send_data_to_plot
   input HorizontNotification.Visualization.Internal.Connector hand;
   input Real x_data[:];
   input Real y_data[:];
   input Real _time_in;
   output Real time_out;

 external "C" time_out = send_msg_plot(hand, x_data, y_data, size(x_data,1),size(y_data,1),_time_in)
    annotation(Library="HorizontDLL",
               Include="#include \"HorizontDLL.h\"",
               IncludeDirectory=
        "modelica://HorizontNotification/Resources/Include",
               LibraryDirectory=
        "modelica://HorizontNotification/Resources/Library");
end send_data_to_plot;

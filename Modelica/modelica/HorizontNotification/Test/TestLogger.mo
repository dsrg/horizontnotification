within HorizontNotification.Test;
model TestLogger
  parameter String fname= "C:/Users/dr14/Desktop/TEMP_CAN_DELETE/first_test.txt";
  parameter String names[:] = {"test1", "test2"};

  Logger.Logger logger(n=3, names={"clock","sin","cos"},
    fname="C:/Records")
    annotation (Placement(transformation(extent={{-6,-10},{14,10}})));
  Modelica.Blocks.Sources.Clock clock
    annotation (Placement(transformation(extent={{-94,44},{-74,64}})));
  Modelica.Blocks.Sources.Sine sine
    annotation (Placement(transformation(extent={{-92,4},{-72,24}})));
  Modelica.Blocks.Sources.Cosine cosine
    annotation (Placement(transformation(extent={{-92,-38},{-72,-18}})));
  Visualization.Plotter plotter
    annotation (Placement(transformation(extent={{-20,30},{0,52}})));
equation
  connect(cosine.y, logger.u[3]) annotation (Line(points={{-71,-28},{-42,-28},{
          -42,1.33333},{-6.4,1.33333}},
                                     color={0,0,127}));
  connect(sine.y, logger.u[2]) annotation (Line(points={{-71,14},{-42,14},{-42,
          0},{-6.4,0}},
                      color={0,0,127}));
  connect(clock.y, logger.u[1]) annotation (Line(points={{-73,54},{-42,54},{-42,
          -1.33333},{-6.4,-1.33333}},  color={0,0,127}));
  connect(clock.y, plotter.x_axis) annotation (Line(points={{-73,54},{-42,54},{-42,
          29.6},{-10,29.6}}, color={0,0,127}));
  connect(sine.y, plotter.u[1]) annotation (Line(points={{-71,14},{-46,14},{-46,
          42},{-21,42}}, color={0,0,127}));
end TestLogger;

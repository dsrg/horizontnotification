within HorizontNotification.Test;
model TestBitSet
  Logger.bitSet bitSet(
    n=4,
    bits_address={1,3,5,8},
    bits_names={"1","2","3","4"})
    annotation (Placement(transformation(extent={{-12,-10},{8,10}})));
  Modelica.Blocks.Sources.BooleanConstant booleanConstant1
    annotation (Placement(transformation(extent={{-74,-16},{-54,4}})));
  Modelica.Blocks.Sources.BooleanConstant booleanConstant2(k=false)
    annotation (Placement(transformation(extent={{-76,-50},{-56,-30}})));
  Modelica.Blocks.Sources.BooleanPulse booleanPulse(period=1)
    annotation (Placement(transformation(extent={{-78,28},{-58,48}})));
  Modelica.Blocks.Sources.BooleanConstant booleanConstant3
    annotation (Placement(transformation(extent={{-82,-86},{-62,-66}})));
  Logger.bitRead bitRead(
    bits_address={1,3,5,8},
    n=4,
    bits_names={"1","2","3","4"})
    annotation (Placement(transformation(extent={{32,-10},{52,10}})));
equation
  connect(bitSet.u[1], booleanPulse.y) annotation (Line(points={{-13.2,-1.5},{
          -34,-1.5},{-34,38},{-57,38}}, color={255,0,255}));
  connect(bitSet.u[2], booleanConstant1.y) annotation (Line(points={{-13.2,-0.5},
          {-32,-0.5},{-32,-6},{-53,-6}}, color={255,0,255}));
  connect(booleanConstant2.y, bitSet.u[3]) annotation (Line(points={{-55,-40},{
          -33.5,-40},{-33.5,0.5},{-13.2,0.5}}, color={255,0,255}));
  connect(booleanConstant3.y, bitSet.u[4]) annotation (Line(points={{-61,-76},{
          -36,-76},{-36,1.5},{-13.2,1.5}}, color={255,0,255}));
  connect(bitRead.u, bitSet.y) annotation (Line(points={{30.8,0},{20,0},{20,0.2},
          {8.4,0.2}}, color={255,127,0}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end TestBitSet;

within HorizontNotification.Test;
model DashboardTest



public
  Visualization.Dashboard dashboard(
    use_speed=true,
    use_rmp=false,
    use_temp=false,
    use_soc=false,
    use_gear=false,
    use_power=false,
    use_engine_on=false,
    port=47000,
    use_indicators=true,
    use_fc=false,
    adress="127.0.0.1")
    annotation (Placement(transformation(extent={{-42,16},{-22,30}})));
  Modelica.Blocks.Sources.Clock clock(offset=10, startTime=0)
    annotation (Placement(transformation(extent={{-80,2},{-60,22}})));
  Logger.bitSet bitSet(
    bits_address={21},
    bits_names={"release"},
    n=1) annotation (Placement(transformation(extent={{-80,36},{-60,56}})));
  Modelica.Blocks.Sources.BooleanConstant booleanConstant1
    annotation (Placement(transformation(extent={{-96,72},{-76,92}})));
  Visualization.Plotter plotter
    annotation (Placement(transformation(extent={{-36,-28},{-16,-6}})));
equation
  connect(dashboard.speed_kmh, clock.y) annotation (Line(points={{-40.2,16},{-50,
          16},{-50,12},{-59,12}}, color={0,0,127}));
  connect(bitSet.u[1], booleanConstant1.y) annotation (Line(points={{-81.2,46},
          {-80,46},{-80,82},{-75,82}}, color={255,0,255}));
  connect(dashboard.indicator, bitSet.y) annotation (Line(points={{-42.5,19.1},
          {-42.5,32.55},{-59.6,32.55},{-59.6,46.2}}, color={255,127,0}));
  connect(clock.y, plotter.u[1]) annotation (Line(points={{-59,12},{-48,12},{
          -48,-16},{-37,-16}}, color={0,0,127}));
  connect(clock.y, plotter.x_axis) annotation (Line(points={{-59,12},{-52,12},{
          -52,-38},{-26,-38},{-26,-28.4}}, color={0,0,127}));
 annotation (
    Icon(coordinateSystem(preserveAspectRatio=false)),
    Diagram(coordinateSystem(preserveAspectRatio=false)));
end DashboardTest;

within HorizontNotification.Test;
model TestEVController
  Internal.RegenBrakingStrategy_Horizont regenBrakingStrategy_Horizont1(data_file=
       "C:/Users/dr14/Documents/Repositories/simulator_all_in_one/dependencies/drivecycle_electric.m")
                                                                        annotation (
    Placement(visible = true, transformation(origin = {-2, 0}, extent = {{-10, -16}, {10, 20}}, rotation = 0)));
  Modelica.Blocks.Sources.Constant SOC(k = 0.7)  annotation (
    Placement(visible = true, transformation(origin = {-82, -12}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.BooleanStep RB_Status(startTime = 50)  annotation (
    Placement(visible = true, transformation(origin = {-78, 38}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Constant const(k = 0.5)  annotation (
    Placement(visible = true, transformation(origin = {-33, 29}, extent = {{-5, -5}, {5, 5}}, rotation = 0)));
  Modelica.Blocks.Sources.Constant const1(k = 1) annotation (
    Placement(visible = true, transformation(origin = {-58, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Constant const2(k = 2500) annotation (
    Placement(visible = true, transformation(origin = {-34, -6}, extent = {{-4, -4}, {4, 4}}, rotation = 0)));
  Modelica.Blocks.Sources.Constant const3(k = 22) annotation (
    Placement(visible = true, transformation(origin={-160,2},   extent = {{-4, -4}, {4, 4}}, rotation = 0)));
  Modelica.Blocks.Sources.Constant const5(k = 0) annotation (
    Placement(visible = true, transformation(origin = {-33, -27}, extent = {{-5, -5}, {5, 5}}, rotation = 0)));
  Modelica.Blocks.Sources.Sine sine
    annotation (Placement(transformation(extent={{-136,-26},{-116,-6}})));
equation
  connect(const5.y, regenBrakingStrategy_Horizont1.brake_demand) annotation (
    Line(points={{-27.5,-27},{-14,-27},{-14,-23.4},{-14.2,-23.4}},  color = {0, 0, 127}));
  connect(const3.y, regenBrakingStrategy_Horizont1.vehicle_velocity) annotation (
    Line(points={{-155.6,2},{-14,2},{-14,-2},{-14.2,-2}},     color = {0, 0, 127}));
  connect(const2.y, regenBrakingStrategy_Horizont1.el_motor_speed) annotation (
    Line(points={{-29.6,-6},{-16,-6},{-16,-7.4},{-14.2,-7.4}},  color = {0, 0, 127}));
  connect(regenBrakingStrategy_Horizont1.el_motor_engaged, const1.y) annotation (
    Line(points={{-14.2,-18.4},{-48,-18.4},{-48,-30},{-47,-30}},    color = {0, 0, 127}));
  connect(regenBrakingStrategy_Horizont1.rb_level, const.y) annotation (
    Line(points={{-14.2,18.2},{-27.5,18.2},{-27.5,29}},
                                                     color = {0, 0, 127}));
  connect(RB_Status.y, regenBrakingStrategy_Horizont1.RB_Status) annotation (
    Line(points={{-67,38},{-16,38},{-16,23.8},{-14.4,23.8}},    color = {255, 0, 255}));
  connect(regenBrakingStrategy_Horizont1.battery_soc, SOC.y) annotation (
    Line(points={{-14.2,-12.6},{-42,-12.6},{-42,-12},{-71,-12}},
                                            color = {0, 0, 127}));
  connect(sine.y, regenBrakingStrategy_Horizont1.Acceleration_demand)
    annotation (Line(points={{-115,-16},{-66,-16},{-66,12.2},{-14.2,12.2}},
        color={0,0,127}));
end TestEVController;

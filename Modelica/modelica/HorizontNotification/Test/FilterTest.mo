within HorizontNotification.Test;
model FilterTest
  Modelica.Blocks.Sources.Sine sine(amplitude=100, freqHz=0.01)
    annotation (Placement(transformation(extent={{-92,-44},{-72,-24}})));
  Modelica.Blocks.Noise.UniformNoise uniformNoise(
    samplePeriod=1,
    y_max=10,
    y_min=20) annotation (Placement(transformation(extent={{-92,-2},{-72,18}})));
  Modelica.Blocks.Math.Add add
    annotation (Placement(transformation(extent={{-36,-20},{-16,0}})));
  Modelica.Blocks.Continuous.Filter filter(order=2, f_cut=0.1)
    annotation (Placement(transformation(extent={{30,-22},{50,-2}})));
equation
  connect(add.u1, uniformNoise.y) annotation (Line(points={{-38,-4},{-58,-4},{
          -58,8},{-71,8}}, color={0,0,127}));
  connect(sine.y, add.u2) annotation (Line(points={{-71,-34},{-60,-34},{-60,-32},
          {-52,-32},{-52,-16},{-38,-16}}, color={0,0,127}));
  connect(filter.u, add.y) annotation (Line(points={{28,-12},{6,-12},{6,-10},{
          -15,-10}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end FilterTest;

within HorizontNotification.Test;
model TestModel
  Modelica.Blocks.Sources.Clock clock
    annotation (Placement(transformation(extent={{-26,-42},{-6,-22}})));
  Notification.LocationBased locationBased(filename="C:/Users/dr14/Documents/Repositories/HorizontDLL/dependencies/road_complete_speed_limits.txt")
    annotation (Placement(transformation(extent={{-24,2},{-4,22}})));
  Modelica.Blocks.Sources.Constant const(k=100)
    annotation (Placement(transformation(extent={{-80,4},{-60,24}})));
  Notification.PositionBased positionBased(filename="C:/Users/dr14/Documents/Repositories/HorizontDLL/dependencies/road_complete_speed_limits.txt")
    annotation (Placement(transformation(extent={{-30,38},{-10,58}})));
  Logger.Logger logger(names={"d_p"," d_l"," ms_p"," ms_l"," rs_p"," rs_l"," cs_p"," cs_l",
        "x","y","z"}, n=11,
    fname="C:/Records")
    annotation (Placement(transformation(extent={{58,16},{78,36}})));
  Logger.bitSet bitSet(bits_address={21}, bits_names={"tmp"})
    annotation (Placement(transformation(extent={{-70,-44},{-50,-24}})));
equation
  connect(locationBased.polyline_location, clock.y) annotation (Line(points={{-4.3,
          12.1},{34,12.1},{34,-34},{-5,-34},{-5,-32}},           color={0,0,127}));
  connect(const.y, locationBased.current_speed) annotation (Line(points={{-59,14},
          {-42,14},{-42,12},{-25,12}}, color={0,0,127}));
  connect(const.y, positionBased.current_speed) annotation (Line(points={{-59,14},
          {-46,14},{-46,48},{-31,48}}, color={0,0,127}));
  connect(locationBased.polyline_position, positionBased.position) annotation (
      Line(points={{-3.4,4},{18,4},{18,48.1},{-10.3,48.1}}, color={0,0,127}));
  connect(clock.y, logger.u[1]) annotation (Line(points={{-5,-32},{24,-32},{24,
          24.1818},{57.6,24.1818}},
                           color={0,0,127}));
  connect(positionBased.polyline_position, logger.u[2]) annotation (Line(points={{-9.4,40},
          {22,40},{22,24.5455},{57.6,24.5455}},           color={0,0,127}));
  connect(locationBased.max_speed, logger.u[3]) annotation (Line(points={{-3.4,17},
          {25.3,17},{25.3,24.9091},{57.6,24.9091}}, color={0,0,127}));
  connect(positionBased.max_speed, logger.u[4]) annotation (Line(points={{-9.4,53},
          {22.3,53},{22.3,25.2727},{57.6,25.2727}}, color={0,0,127}));
  connect(locationBased.recomended_speed, logger.u[5]) annotation (Line(points={{-3.4,7},
          {25.3,7},{25.3,25.6364},{57.6,25.6364}},          color={0,0,127}));
  connect(positionBased.recomended_speed, logger.u[6]) annotation (Line(points={{-9.4,43},
          {22,43},{22,26},{57.6,26}},           color={0,0,127}));
  connect(locationBased.coasting_speed, logger.u[7]) annotation (Line(points={{-3.4,20},
          {26,20},{26,26.3636},{57.6,26.3636}},     color={0,0,127}));
  connect(positionBased.coasting_speed, logger.u[8]) annotation (Line(points={{-9.4,56},
          {22,56},{22,26.7273},{57.6,26.7273}},     color={0,0,127}));
  connect(locationBased.polyline_position[1], logger.u[9]) annotation (Line(
        points={{-3.4,3.33333},{26,3.33333},{26,27.0909},{57.6,27.0909}}, color=
         {0,0,127}));
  connect(locationBased.polyline_position[2], logger.u[10]) annotation (Line(
        points={{-3.4,4},{26,4},{26,27.4545},{57.6,27.4545}}, color={0,0,127}));
  connect(locationBased.polyline_position[3], logger.u[11]) annotation (Line(
        points={{-3.4,4.66667},{26,4.66667},{26,27.8182},{57.6,27.8182}}, color=
         {0,0,127}));
  connect(locationBased.message_active, bitSet.u[1]) annotation (Line(points={{
          -14,2},{-48,2},{-48,-10},{-86,-10},{-86,-34},{-71.2,-34}}, color={255,
          0,255}));
  annotation (
    Icon(coordinateSystem(preserveAspectRatio=false)),
    Diagram(coordinateSystem(preserveAspectRatio=false)));
end TestModel;

within HorizontNotification.Internal;
model HybridVehicleController_Wrapped_PID

  // --- parameters
  parameter IStandard.Types.Path data_file="" "Data file name";

  // ev clutch parameters
  parameter SI.Time ev_clutch_tau=0.5 "Time constant for EV clutch";
  parameter Boolean ev_enabled=true "EV enabled";

  // demand split
  parameter Boolean ds_boost_enabled=true "Electric motor boost enabled";
  parameter Real ds_boost_low_threshold=0.5 "Boost low demand threshold";
  parameter Real ds_boost_hi_threshold=0.55 "Boost high demand threshold";
  parameter Real ds_ev_demand_to_el_motor_demand=1.0 "EV mode demand factor";
  parameter Real ds_boost_disable_soc_threshold=0.0 "Disable boost SOC";

  // stop start
  parameter Boolean ss_enabled=true "Stop start enabled";
  parameter Real ss_eng_off_demand_threshold=0.1
    "EV engine off demand threshold";
  parameter Real ss_eng_on_demand_threshold=0.3 "EV engine on demand threshold";
  parameter SI.Time ss_t_end_stop=0.5 "Engine stop delay";
  parameter SI.Velocity ss_v_veh_static=1.0 "Vehicle static threshold";
  parameter NSI.AngularVelocity_rpm ss_omega_eng_running=800
    "Engine running threshold";
  parameter Real ss_inhibit_ev_soc=0.1 "EV mode not allowed below this SOC";
  parameter Real ss_reenable_ev_soc=0.15 "EV mode reenabled above this SOC";

  // regeneration braking
  parameter IPowertrain.Controllers.Internal.Types.HybridConfig hybrid_config=
      IPowertrain.Controllers.Internal.Types.HybridConfig.P2
    "Hybrid configuration::Position of el. machine and clutch";
  parameter Boolean rb_enabled=true "Regeneration braking enabled";
  parameter SI.Force rb_max_brake_force=2000 "Maximum friction brake force";
  parameter Real rb_soc_high=0.95 "Maximum SOC to allow regeneration";
  parameter SI.Velocity rb_speed_low=1 "Low speed regeneration limit";

  // -- generation
  parameter Boolean ge_enabled=true "Generation enabled";
  parameter Real ge_soc_low=0.6 "Full generation SOC";
  parameter Real ge_soc_high=0.95 "Zero generation SOC";
  parameter NSI.AngularVelocity_rpm ge_eng_speed_low=0
    "Zero generation engine speed";
  parameter NSI.AngularVelocity_rpm ge_eng_speed_high=0
    "Full generation engine speed";
  parameter Real gen_demand_disable=0.6
    "Driver demand where the generation will be disabled";
  parameter Modelica.Blocks.Types.SimpleController controllerType=.Modelica.Blocks.Types.SimpleController.PID
    "Type of controller";
  parameter Real k=1 "Gain of controller";
  parameter SI.Time Ti=0.5 "Time constant of Integrator block";
  parameter SI.Time Td=0.1 "Time constant of Derivative block";


  // --- Bus vars
  Modelica.Blocks.Interfaces.RealInput driver_brake_demand   annotation (Placement(transformation(extent={{-120,
            -114},{-100,-94}},                                                                                                  rotation=0),
        iconTransformation(extent={{-120,-114},{-100,-94}})));
  Modelica.Blocks.Interfaces.RealInput shift_accn_demand   annotation (Placement(transformation(extent={{-120,
            -10},{-100,10}},                                                                                              rotation=0),
        iconTransformation(extent={{-120,-10},{-100,10}})));
  Modelica.Blocks.Interfaces.RealInput battery_soc   annotation (Placement(transformation(extent={{-120,
            -42},{-100,-22}},                                                                                           rotation=0),
        iconTransformation(extent={{-120,-42},{-100,-22}})));
  Modelica.Blocks.Interfaces.RealInput engine_speed   annotation (Placement(transformation(extent={{-120,22},
            {-100,42}},                                                                                                  rotation=0),
        iconTransformation(extent={{-120,26},{-100,46}})));
  Modelica.Blocks.Interfaces.RealInput motor_gen_speed   annotation (Placement(transformation(extent={{-120,66},
            {-100,86}},                                                                                                    rotation=0),
        iconTransformation(extent={{-120,66},{-100,86}})));
  Modelica.Blocks.Interfaces.RealInput vehicle_velocity   annotation (Placement(transformation(extent={{-120,
            100},{-100,120}},                                                                                              rotation=0)));
  Modelica.Blocks.Interfaces.RealInput clutch_engagement   annotation (Placement(transformation(extent={{-120,
            -76},{-100,-56}},                                                                                               rotation=0),
        iconTransformation(extent={{-120,-76},{-100,-56}})));
  Modelica.Blocks.Interfaces.RealOutput engine_key_on   annotation (Placement(transformation(extent={{100,30},
            {120,50}},                                                                                                    rotation=0)));
  Modelica.Blocks.Interfaces.RealOutput engine_demand   annotation (Placement(transformation(extent={{100,-9},
            {120,11}},                                                                                                   rotation=0)));
  Modelica.Blocks.Interfaces.RealOutput motor_gen_demand   annotation (Placement(transformation(extent={{100,
            -156},{120,-136}},                                                                                             rotation=0)));
  Modelica.Blocks.Interfaces.RealOutput brake_demand   annotation (Placement(transformation(extent={{104,
            -266},{124,-246}},                                                                                            rotation=0)));
  Modelica.Blocks.Interfaces.RealOutput eclutch_engagement   annotation (Placement(transformation(extent={{100,71},
            {120,91}},                                                                                                   rotation=0)));

  IPowertrain.Controllers.Internal.DemandSplitStrategy demandSplitStrategy(
    boost_enabled=ds_boost_enabled,
    boost_low_threshold=ds_boost_low_threshold,
    boost_hi_threshold=ds_boost_hi_threshold,
    data_file=data_file,
    ev_demand_to_el_motor_demand=ds_ev_demand_to_el_motor_demand,
    boost_disable_soc_threshold=ds_boost_disable_soc_threshold,
    gen_demand_disable=gen_demand_disable)
    annotation (Placement(transformation(extent={{18,16},{38,36}})));
  IPowertrain.Controllers.Internal.GenerationStrategy generationStrategy(
    enabled=ge_enabled,
    soc_low=ge_soc_low,
    soc_high=ge_soc_high,
    eng_speed_low=eng_speed_low,
    eng_speed_high=eng_speed_high)
    annotation (Placement(transformation(extent={{-36,-10},{-16,10}})));
  IPowertrain.Controllers.Internal.StopStartStrategy stopStartStrategy(
    enabled=ss_enabled,
    eng_off_demand_threshold=ss_eng_off_demand_threshold,
    eng_on_demand_threshold=ss_eng_on_demand_threshold,
    t_end_stop=ss_t_end_stop,
    v_veh_static=ss_v_veh_static,
    omega_eng_running=ss_omega_eng_running,
    inhibit_ev_soc=ss_inhibit_ev_soc,
    reenable_ev_soc=ss_reenable_ev_soc,
    omega_eng_needs_cranking=ss_omega_eng_needs_cranking)
    annotation (Placement(transformation(extent={{-38,70},{-18,90}})));
  Modelica.Blocks.Sources.BooleanExpression booleanExpression(y=ev_enabled)
    annotation (Placement(transformation(extent={{-76,60},{-64,80}})));
  Modelica.Blocks.Logical.Switch switch1
    annotation (Placement(transformation(extent={{8,118},{28,98}})));
  Modelica.Blocks.Sources.Constant const(k=1.0)
    annotation (Placement(transformation(extent={{-28,118},{-8,138}})));

  Modelica.Blocks.Continuous.CriticalDamping criticalDamping(f=1/ev_clutch_tau,
      initType=Modelica.Blocks.Types.Init.SteadyState)
    annotation (Placement(transformation(extent={{34,98},{54,118}})));
  Modelica.Blocks.Interfaces.RealInput driver_accn_demand  annotation (Placement(transformation(extent={{-120,
            -144},{-100,-124}},                                                                                           rotation=0),
        iconTransformation(extent={{-120,-144},{-100,-124}})));
  parameter NSI.AngularVelocity_rpm ss_omega_eng_needs_cranking=500
    "Engine must be cranked speed threshold";
  Modelica.Blocks.Logical.And and1
    annotation (Placement(transformation(extent={{-52,100},{-36,116}})));
  Modelica.Blocks.Sources.BooleanExpression hybrid_architecture(y=hybrid_config
         <> IPowertrain.Controllers.Internal.Types.HybridConfig.P1)
    annotation (Placement(transformation(extent={{-86,118},{-66,138}})));
  RegenBrakingStrategy_Horizont regenBrakingStrategy_Horizont(data_file=
        data_file)
    annotation (Placement(transformation(extent={{4,-268},{24,-232}})));
  Modelica.Blocks.Interfaces.RealInput reg_level annotation (Placement(
        transformation(extent={{-122,-98},{-100,-76}}),
                                                      iconTransformation(extent={{-120,
            -236},{-100,-216}})));
  Modelica.Blocks.Interfaces.RealInput coasting_speed annotation (Placement(
        transformation(extent={{-120,-178},{-100,-158}}),
                                                   iconTransformation(extent={{-120,
            -202},{-100,-182}})));
  Modelica.Blocks.Continuous.LimPID PID(yMax=0, yMin=-1,
    controllerType=controllerType,
    k=k,
    Ti=Ti,
    Td=Td)
    annotation (Placement(transformation(extent={{-84,-154},{-58,-180}})));
  Modelica.Blocks.Interfaces.BooleanInput coasin_status annotation (Placement(
        transformation(extent={{-116,-240},{-94,-218}}),
                                                    iconTransformation(extent={{-120,
            -270},{-100,-250}})));
  Modelica.Blocks.Logical.Switch switch2
    annotation (Placement(transformation(extent={{76,-136},{96,-156}})));

  parameter NSI.AngularVelocity_rpm eng_speed_low=1200
    "Zero generation engine speed";
  parameter NSI.AngularVelocity_rpm eng_speed_high=2600
    "Full generation engine speed";
  Modelica.Blocks.Logical.Switch switch3
    annotation (Placement(transformation(extent={{78,8},{92,-6}})));
  Modelica.Blocks.Sources.Constant ZERO(k=0)
    annotation (Placement(transformation(extent={{64,-18},{72,-10}})));
  Modelica.Blocks.Sources.Constant ZERO1(k=1)
    annotation (Placement(transformation(extent={{62,68},{70,76}})));
  Modelica.Blocks.Logical.Switch switch4
    annotation (Placement(transformation(extent={{74,88},{88,74}})));
equation
  connect(generationStrategy.el_motor_gen_demand, demandSplitStrategy.el_motor_gen_demand)
    annotation (Line(
      points={{-15,0},{-6,0},{-6,18},{16,18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(stopStartStrategy.engine_key_on, demandSplitStrategy.engine_key_on)
    annotation (Line(
      points={{-17,85},{47.5,85},{47.5,42},{16,42}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(demandSplitStrategy.el_motor_crank_demand, stopStartStrategy.el_motor_crank_demand)
    annotation (Line(
      points={{16,36},{16,73},{-17,73}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(battery_soc, generationStrategy.battery_soc) annotation (Line(
      points={{-110,-32},{-40,-32},{-40,-8},{-38,-8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(battery_soc, demandSplitStrategy.battery_soc) annotation (Line(
      points={{-110,-32},{-40,-32},{-40,-20},{2,-20},{2,12},{16,12}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(generationStrategy.engine_speed, engine_speed) annotation (Line(
      points={{-38,0},{-58,0},{-58,32},{-110,32}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(engine_speed, stopStartStrategy.engine_speed) annotation (Line(
      points={{-110,32},{-58,32},{-58,82},{-40,82}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(vehicle_velocity, stopStartStrategy.vehicle_velocity) annotation (
      Line(
      points={{-110,110},{-80,110},{-80,88},{-40,88}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(shift_accn_demand, demandSplitStrategy.demand) annotation (Line(
      points={{-110,0},{-50,0},{-50,-26},{10,-26},{10,6},{16,6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(shift_accn_demand, generationStrategy.total_demand) annotation (Line(
      points={{-110,0},{-50,0},{-50,8},{-38,8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(engine_speed, demandSplitStrategy.engine_speed) annotation (Line(
      points={{-110,32},{-28,32},{-28,30},{16,30}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(demandSplitStrategy.el_motor_speed, motor_gen_speed) annotation (Line(
      points={{16,24},{-88,24},{-88,76},{-110,76}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(stopStartStrategy.engine_key_on, engine_key_on) annotation (Line(
      points={{-17,85},{58,85},{58,40},{110,40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(booleanExpression.y, stopStartStrategy.ev_enabled) annotation (Line(
      points={{-63.4,70},{-40,70}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(stopStartStrategy.engine_key_on, switch1.u1) annotation (Line(
      points={{-17,85},{-7.5,85},{-7.5,100},{6,100}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(const.y, switch1.u3) annotation (Line(
      points={{-7,128},{0,128},{0,116},{6,116}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(stopStartStrategy.soc, battery_soc) annotation (Line(
      points={{-31.4,65.4},{-31.4,60},{-70,60},{-70,-32},{-110,-32}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(criticalDamping.u, switch1.y) annotation (Line(
      points={{32,108},{29,108}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(driver_accn_demand, generationStrategy.driver_accn_demand)
    annotation (Line(
      points={{-110,-134},{-26,-134},{-26,-13.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(shift_accn_demand, stopStartStrategy.demand) annotation (Line(points={{-110,0},
          {-80,0},{-50,0},{-50,76},{-40,76}},                 color={0,0,127}));
  connect(demandSplitStrategy.engine_demand, stopStartStrategy.engine_demand)
    annotation (Line(points={{39,31},{52,31},{52,34},{52,56},{-22.6,56},{-22.6,
          65.4}},                                                         color=
         {0,0,127}));
  connect(booleanExpression.y, and1.u2) annotation (Line(points={{-63.4,70},{
          -60,70},{-60,101.6},{-53.6,101.6}}, color={255,0,255}));
  connect(and1.y, switch1.u2) annotation (Line(points={{-35.2,108},{-15.65,108},
          {6,108}}, color={255,0,255}));
  connect(hybrid_architecture.y, and1.u1) annotation (Line(points={{-65,128},{
          -62,128},{-62,108},{-53.6,108}}, color={255,0,255}));
  connect(driver_brake_demand, regenBrakingStrategy_Horizont.brake_demand)
    annotation (Line(points={{-110,-104},{-50,-104},{-50,-275.4},{1.8,-275.4}},
        color={0,0,127}));
  connect(clutch_engagement, regenBrakingStrategy_Horizont.el_motor_engaged)
    annotation (Line(points={{-110,-66},{-50,-66},{-50,-270.4},{1.8,-270.4}},
        color={0,0,127}));
  connect(battery_soc, regenBrakingStrategy_Horizont.battery_soc) annotation (
      Line(points={{-110,-32},{-50,-32},{-50,-264.6},{1.8,-264.6}},  color={0,0,
          127}));
  connect(motor_gen_speed, regenBrakingStrategy_Horizont.el_motor_speed)
    annotation (Line(points={{-110,76},{-50,76},{-50,-259.4},{1.8,-259.4}},
        color={0,0,127}));
  connect(vehicle_velocity, regenBrakingStrategy_Horizont.vehicle_velocity)
    annotation (Line(points={{-110,110},{-50,110},{-50,-254},{1.8,-254}},
        color={0,0,127}));
  connect(driver_accn_demand, regenBrakingStrategy_Horizont.Acceleration_demand)
    annotation (Line(points={{-110,-134},{-50,-134},{-50,-239.8},{1.8,-239.8}},
        color={0,0,127}));
  connect(reg_level, regenBrakingStrategy_Horizont.rb_level) annotation (Line(
        points={{-111,-87},{-80,-87},{-80,-88},{-50,-88},{-50,-233.8},{1.8,
          -233.8}},color={0,0,127}));
  connect(coasting_speed, PID.u_s) annotation (Line(points={{-110,-168},{-86.6,
          -168},{-86.6,-167}}, color={0,0,127}));
  connect(coasin_status, regenBrakingStrategy_Horizont.RB_Status) annotation (
      Line(points={{-105,-229},{1.6,-229},{1.6,-228.2}},
                   color={255,0,255}));
  connect(PID.y, switch2.u1) annotation (Line(points={{-56.7,-167},{-52,-167},{
          -52,-154},{74,-154}},                     color={0,0,127}));
  connect(vehicle_velocity, PID.u_m) annotation (Line(points={{-110,110},{-90,
          110},{-90,-151.4},{-71,-151.4}}, color={0,0,127}));
  connect(regenBrakingStrategy_Horizont.mech_brake_demand, brake_demand)
    annotation (Line(points={{24.8,-257},{66.4,-257},{66.4,-256},{114,-256}},
        color={0,0,127}));
  connect(regenBrakingStrategy_Horizont.el_motor_regen_demand,
    demandSplitStrategy.el_motor_regen_demand) annotation (Line(points={{24.8,
          -246.8},{24.8,-186},{42,-186},{42,-124},{16,-124},{16,0}}, color={0,0,
          127}));
  connect(switch2.y, motor_gen_demand)
    annotation (Line(points={{97,-146},{110,-146}}, color={0,0,127}));
  connect(demandSplitStrategy.el_motor_demand_out, switch2.u3) annotation (Line(
        points={{39,21},{39,-58.5},{74,-58.5},{74,-138}}, color={0,0,127}));
  connect(regenBrakingStrategy_Horizont.RB_Coasting, switch2.u2) annotation (
      Line(points={{26.3,-228.9},{56,-228.9},{56,-146},{74,-146}}, color={255,0,
          255}));
  connect(switch3.y, engine_demand)
    annotation (Line(points={{92.7,1},{98,1},{110,1}}, color={0,0,127}));
  connect(demandSplitStrategy.engine_demand, switch3.u3) annotation (Line(
        points={{39,31},{52.5,31},{52.5,6.6},{76.6,6.6}}, color={0,0,127}));
  connect(ZERO.y, switch3.u1) annotation (Line(points={{72.4,-14},{72.4,-4.6},{
          76.6,-4.6}}, color={0,0,127}));
  connect(regenBrakingStrategy_Horizont.RB_Coasting, switch3.u2) annotation (
      Line(points={{26.3,-228.9},{56,-228.9},{56,0},{76.6,0},{76.6,1}}, color={
          255,0,255}));
  connect(switch4.y, eclutch_engagement)
    annotation (Line(points={{88.7,81},{110,81}}, color={0,0,127}));
  connect(criticalDamping.y, switch4.u3) annotation (Line(points={{55,108},{66,
          108},{66,86.6},{72.6,86.6}}, color={0,0,127}));
  connect(ZERO1.y, switch4.u1) annotation (Line(points={{70.4,72},{72.6,72},{
          72.6,75.4}}, color={0,0,127}));
  connect(regenBrakingStrategy_Horizont.RB_Coasting, switch4.u2) annotation (
      Line(points={{26.3,-228.9},{60,-228.9},{60,81},{72.6,81}}, color={255,0,
          255}));
  annotation (
    __RModel(element="
    <panel>
      <tabs>
        <tab name='General'>
          <pane>
            <path ref='data_file' editbutton='true' />
            <checkbox ref='ev_enabled' />
            <scalar ref='ev_clutch_tau'  />
            <spacer expandVertically='true'/>
          </pane>
        </tab>
        <tab name='Demand split'>
          <pane>
            <scalar     ref='ds_ev_demand_to_el_motor_demand' />
            <checkbox ref='ds_boost_enabled'       />
            <scalar     ref='ds_boost_low_threshold' />
            <scalar     ref='ds_boost_hi_threshold'  />
            <scalar     ref='ds_boost_disable_soc_threshold' />
            <spacer expandVertically='true'/>
          </pane>
        </tab>
        <tab name='Generation'>
          <pane>
            <checkbox ref='ge_enabled'     />
            <scalar ref='ge_soc_low'         />
            <scalar ref='ge_soc_high'        />
            <scalar ref='ge_eng_speed_high'   />
            <scalar ref='ge_eng_speed_low'  />
            <spacer expandVertically='true'/>
          </pane>
        </tab>
        <tab name='Regen braking'>
          <pane>
            <checkbox ref='rb_enabled'      />
            <scalar ref='rb_max_brake_force'  />
            <scalar ref='rb_soc_high'         />
            <scalar ref='rb_speed_low'        />
            <spacer expandVertically='true'/>
          </pane>
        </tab>
        <tab name='Stop start'>
          <pane>
            <checkbox ref='ss_enabled'     />
            <scalar ref='ss_eng_off_demand_threshold' />
            <scalar ref='ss_eng_on_demand_threshold'   />
            <scalar ref='ss_t_end_stop'    />
            <scalar ref='ss_v_veh_static'  />
            <scalar ref='ss_omega_eng_running'  />
            <spacer expandVertically='true'/>
          </pane>
        </tab>
      </tabs>
    </panel>
    <logic default='active'>
      <selector ref='ev_enabled'>
        <option state='false'>
          <inactive ref='ev_clutch_tau'/>
          <inactive ref='ss_eng_off_demand_threshold' />
          <inactive ref='ss_eng_on_demand_threshold' />
          <inactive ref='ds_ev_demand_to_el_motor_demand' />
        </option> 
        <option state='true'> 
        </option> 
      </selector> 
      <selector ref='ds_boost_enabled' > 
        <option state='false'> 
          <inactive ref='ds_boost_low_threshold' />
          <inactive ref='ds_boost_hi_threshold' /> 
          <inactive ref='ds_boost_disable_soc_threshold' />
        </option> 
        <option state='true'> 
        </option>
      </selector> 
      <selector ref='ge_enabled'>
        <option state='false'> 
          <inactive ref='ge_soc_low' />
          <inactive ref='ge_soc_high' />
          <inactive ref='ge_eng_speed_low' />
          <inactive ref='ge_eng_speed_high' />
        </option>
        <option state='true'> 
        </option>
      </selector>
      <selector ref='rb_enabled'>
        <option state='false'> 
          <inactive ref='rb_max_brake_force' />
          <inactive ref='rb_soc_high' />
          <inactive ref='rb_speed_low' />
        </option>
        <option state='true'> 
        </option>
      </selector>
      <selector ref='ss_enabled'>
        <option state='false'> 
          <inactive ref='ss_eng_off_demand_threshold' />
          <inactive ref='ss_eng_on_demand_threshold' />
          <inactive ref='ss_t_end_stop' />
          <inactive ref='ss_v_veh_static' />
          <inactive ref='ss_omega_eng_running' />         
        </option>
        <option state='true'> 
        </option>
      </selector>          
    </logic>    
    "),
    Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,-320},{
            100,140}})),
    Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-320},{100,
            140}}),
        graphics={
        Rectangle(
          extent={{-70,-90},{70,90}},
          pattern=LinePattern.Solid,
          fillColor={175,175,175},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Rectangle(
          extent={{60,-90},{70,90}},
          pattern=LinePattern.Solid,
          fillColor={0,215,55},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,0}),
        Polygon(
          points={{-70,90},{-60,100},{60,100},{70,90},{-70,90}},
          pattern=LinePattern.Solid,
          fillPattern=FillPattern.Solid,
          fillColor={0,5,200},
          lineColor={0,0,0}),
        Polygon(
          points={{-70,-90},{-60,-100},{60,-100},{70,-90},{-70,-90}},
          pattern=LinePattern.Solid,
          fillPattern=FillPattern.Solid,
          fillColor={0,5,200},
          lineColor={0,0,0}),
        Line(
          points={{20,-90},{20,90}},
          pattern=LinePattern.Solid,
          color={0,0,0},
          smooth=Smooth.None,
          thickness=2),
        Rectangle(
          extent={{-70,50},{20,60}},
          pattern=LinePattern.Solid,
          fillColor={175,175,175},
          fillPattern=FillPattern.None,
          lineColor={0,0,0},
          lineThickness=1),
        Rectangle(
          extent={{-70,30},{20,40}},
          pattern=LinePattern.Solid,
          fillColor={175,175,175},
          fillPattern=FillPattern.None,
          lineColor={0,0,0},
          lineThickness=1),
        Rectangle(
          extent={{-70,10},{20,20}},
          pattern=LinePattern.Solid,
          fillColor={175,175,175},
          fillPattern=FillPattern.None,
          lineColor={0,0,0},
          lineThickness=1),
        Rectangle(
          extent={{-70,-10},{20,0}},
          pattern=LinePattern.Solid,
          fillColor={175,175,175},
          fillPattern=FillPattern.None,
          lineColor={0,0,0},
          lineThickness=1),
        Rectangle(
          extent={{-70,-30},{20,-20}},
          pattern=LinePattern.Solid,
          fillColor={175,175,175},
          fillPattern=FillPattern.None,
          lineColor={0,0,0},
          lineThickness=1),
        Rectangle(
          extent={{-70,-50},{20,-40}},
          pattern=LinePattern.Solid,
          fillColor={175,175,175},
          fillPattern=FillPattern.None,
          lineColor={0,0,0},
          lineThickness=1),
        Text(
          extent={{-90,132},{6,92}},
          lineColor={255,0,0},
          textString="vel",
          horizontalAlignment=TextAlignment.Left),
        Text(
          extent={{-90,98},{6,58}},
          lineColor={255,0,0},
          horizontalAlignment=TextAlignment.Left,
          textString="mgs"),
        Text(
          extent={{-90,60},{6,20}},
          lineColor={255,0,0},
          horizontalAlignment=TextAlignment.Left,
          textString="es"),
        Text(
          extent={{-90,24},{6,-16}},
          lineColor={255,0,0},
          horizontalAlignment=TextAlignment.Left,
          textString="sad"),
        Text(
          extent={{-90,-10},{6,-50}},
          lineColor={255,0,0},
          horizontalAlignment=TextAlignment.Left,
          textString="soc"),
        Text(
          extent={{-90,-46},{6,-86}},
          lineColor={255,0,0},
          horizontalAlignment=TextAlignment.Left,
          textString="cle"),
        Text(
          extent={{-90,-84},{0,-122}},
          lineColor={255,0,0},
          horizontalAlignment=TextAlignment.Left,
          textString="dbd"),
        Text(
          extent={{26,100},{122,60}},
          lineColor={255,0,0},
          horizontalAlignment=TextAlignment.Left,
          textString="ecle"),
        Text(
          extent={{24,60},{120,20}},
          lineColor={255,0,0},
          horizontalAlignment=TextAlignment.Left,
          textString="key"),
        Text(
          extent={{26,20},{122,-20}},
          lineColor={255,0,0},
          horizontalAlignment=TextAlignment.Left,
          textString="ed"),
        Text(
          extent={{26,-18},{122,-58}},
          lineColor={255,0,0},
          horizontalAlignment=TextAlignment.Left,
          textString="mgd"),
        Text(
          extent={{26,-58},{122,-98}},
          lineColor={255,0,0},
          horizontalAlignment=TextAlignment.Left,
          textString="bd")}));
end HybridVehicleController_Wrapped_PID;

within HorizontNotification.Internal;
model RB_Identifier

  Modelica.Blocks.Interfaces.RealInput velocity annotation (Placement(transformation(extent={{-140,34},{-100,74}})));
  Modelica.Blocks.Interfaces.RealInput acceleration_demand annotation (Placement(transformation(extent={{-140,-20},{-100,20}})));
  Modelica.Blocks.Interfaces.RealInput deceleration_demand annotation (Placement(transformation(extent={{-140,-80},{-100,-40}})));
  parameter Real V_RB_MIN=3 annotation (Dialog);
  Modelica.Blocks.Interfaces.BooleanOutput RB_on
    annotation (Placement(transformation(extent={{100,-20},{138,18}})));

equation
  if velocity>V_RB_MIN and acceleration_demand<0.001 and deceleration_demand<0.001 then
    RB_on=true;
    else
    RB_on=false;
  end if;

  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(coordinateSystem(preserveAspectRatio=false)));
end RB_Identifier;

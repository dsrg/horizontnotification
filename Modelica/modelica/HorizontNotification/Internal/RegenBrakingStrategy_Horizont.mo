within HorizontNotification.Internal;
model RegenBrakingStrategy_Horizont
  import IStandard.Interpolation.Interp1D;
  import IStandard.Interpolation.InterpLinear1D;
  import IStandard.Constants.RAD2RPM;

  parameter Boolean enabled = true "Regen enabled";
  parameter SI.Force max_brake_force=8000 "Maximum friction brake force";
  parameter Real soc_high=0.95 "Maximum SOC to allow regeneration";
  parameter SI.Velocity speed_low=1 "Low speed regeneration limit";
  parameter IStandard.Types.Path data_file = "E:/drivecycle_electric.m" "Data filename";
  parameter Types.HybridConfig hybrid_config= Types.HybridConfig.P2 "Hybrid configuration::Position of el. machine and clutch";
  //horizon vehicle
  Boolean RB_coasting_int;
  Boolean Acc_Low;

  Modelica.Blocks.Interfaces.RealInput brake_demand annotation (
    Placement(visible = true,transformation(extent = {{-140, -140}, {-100, -100}}, rotation = 0), iconTransformation(extent = {{-142, -254}, {-102, -214}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealInput battery_soc annotation (
    Placement(visible = true,transformation(extent = {{-140, -40}, {-100, 0}}, rotation = 0), iconTransformation(extent = {{-142, -146}, {-102, -106}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealInput vehicle_velocity annotation (
    Placement(visible = true, transformation(extent = {{-140, 84}, {-100, 124}}, rotation = 0), iconTransformation(extent = {{-142, -40}, {-102, 0}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealInput el_motor_speed annotation (
    Placement(visible = true,transformation(extent = {{-140, 24}, {-100, 64}}, rotation = 0), iconTransformation(extent = {{-142, -94}, {-102, -54}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealInput el_motor_engaged annotation (
    Placement(visible = true,transformation(extent = {{-140, -106}, {-100, -66}}, rotation = 0), iconTransformation(extent = {{-142, -204}, {-102, -164}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealOutput el_motor_regen_demand annotation (
    Placement(transformation(extent={{98,42},{118,62}}),       iconTransformation(extent={{98,42},
            {118,62}})));
  Modelica.Blocks.Interfaces.RealOutput mech_brake_demand annotation (
    Placement(transformation(extent={{98,-60},{118,-40}}),       iconTransformation(extent={{98,-60},
            {118,-40}})));
  Modelica.Blocks.Interfaces.RealInput Acceleration_demand annotation (
    Placement(visible = true, transformation(extent = {{-140, 54}, {-100, 94}}, rotation = 0), iconTransformation(extent={{-142,
            102},{-102,142}},                                                                                                                          rotation = 0)));
  Modelica.Blocks.Interfaces.RealInput rb_level annotation (
    Placement(visible = true, transformation(extent = {{-140, -8}, {-100, 32}}, rotation = 0), iconTransformation(extent={{-142,
            162},{-102,202}},                                                                                                                          rotation = 0)));
  Modelica.Blocks.Interfaces.BooleanInput RB_Status annotation (
    Placement(visible = true, transformation(origin = {-120, 134}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-124, 238}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealOutput Available_Power_RB annotation (
    Placement(visible = true, transformation(extent={{98,114},{118,134}},    rotation = 0), iconTransformation(extent={{102,130},
            {122,150}},                                                                                                                          rotation = 0)));
  Modelica.Blocks.Interfaces.BooleanOutput RB_Coasting
    annotation (Placement(transformation(extent={{102,210},{144,252}}),
        iconTransformation(extent={{102,210},{144,252}})));
protected
  Interp1D t_max = Interp1D(active = true, data_file = data_file, table_name = "el_motor_brake_torque_max");
  SI.Velocity abs_vehicle_vel "Absolute vehicle velocity";
  SI.Torque max_el_motor_trq;
  SI.AngularVelocity abs_el_motor_speed;
  Real max_el_demand;
  SI.Power p_shortfall "Brake power shortfall";


  SI.Power p_requested = brake_demand * max_brake_force * abs_vehicle_vel "Requested brake power";
  SI.Power p_elec_avail = max_el_motor_trq * abs_el_motor_speed "Amount of electrical braking available";
  SI.Power p_fric_avail = max_brake_force * abs_vehicle_vel "Amount of friction braking available";



algorithm
  when Acceleration_demand < 0.005 then
    Acc_Low:=true;
  end when;
  when Acceleration_demand > 0.01 then
    Acc_Low:=false;
  end when;




equation
  RB_coasting_int = Acc_Low and brake_demand < 0.01 and abs_vehicle_vel > speed_low;
  RB_Coasting =RB_coasting_int and RB_Status;
  Available_Power_RB=p_elec_avail;

  abs_vehicle_vel = abs(vehicle_velocity);
  abs_el_motor_speed = Modelica.SIunits.Conversions.from_rpm(abs(el_motor_speed));
  max_el_motor_trq = InterpLinear1D(t_max, abs(el_motor_speed));

  if hybrid_config == Types.HybridConfig.P1 then
    if el_motor_engaged > 0.99 then
      max_el_demand = IStandard.Functions.ramp(1.0, 0.0, soc_high - 0.05, soc_high, battery_soc) * IStandard.Functions.ramp(0.0, 1.0, speed_low, speed_low + 1.5, abs_vehicle_vel);
    else
      max_el_demand = 0;
    end if;
  else
    max_el_demand = IStandard.Functions.ramp(1.0, 0.0, soc_high - 0.05, soc_high, battery_soc) * IStandard.Functions.ramp(0.0, 1.0, speed_low, speed_low + 1.5, abs_vehicle_vel);
  end if;


  if (RB_coasting_int) then
    if (RB_Status) then
      el_motor_regen_demand = 0;
      mech_brake_demand = 0;
      p_shortfall = 0;
    else
      el_motor_regen_demand = -rb_level;
      mech_brake_demand = brake_demand;
      p_shortfall = 0;
    end if;
  else
    if noEvent(abs_vehicle_vel < speed_low or enabled == false or p_elec_avail < 1e-6) then
        el_motor_regen_demand = 0;
        mech_brake_demand = brake_demand;
        p_shortfall = 0;
    else
        el_motor_regen_demand = -min(max_el_demand, min(1.0, max(0, p_requested / p_elec_avail)));
        p_shortfall = p_requested + el_motor_regen_demand * p_elec_avail;
        mech_brake_demand = max(0, p_shortfall / p_fric_avail);
    end if;
  end if;

// Using tolerance of 0.01 to account for numerical errors in solution
  assert(max_el_motor_trq > (-0.01), "Electric motor braking torque should show absolute values of braking torque");
  assert(brake_demand > mech_brake_demand - 0.01, "Mechanical brake demand bigger than nominal mechanical brake demand");
  assert(brake_demand > (-0.01), "Internal variable brake_demand should not be negativ");
  assert(el_motor_regen_demand < 0.01, "Should not happen that el_demand > 0 for regen braking strategy");
  assert(mech_brake_demand > (-0.01), "Should not happen that mech_brake_demand < 0 for regen braking strategy");
  annotation (
    Icon(coordinateSystem(preserveAspectRatio = false, initialScale = 0.1,
        extent={{-100,-160},{100,200}}),                                    graphics={  Rectangle(fillColor = {215, 215, 215},
            fillPattern =                                                                                                                    FillPattern.Solid, extent = {{-102, 264}, {102, -264}}), Text(origin = {-6, -108}, lineColor = {128, 128, 128}, fillColor = {215, 215, 215},
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent = {{-76, -102}, {24, -142}}, textString = "bd",
            horizontalAlignment =                                                                                                                                                                                                        TextAlignment.Left), Text(origin = {0, -126}, lineColor = {128, 128, 128}, fillColor = {215, 215, 215},
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent = {{-78, 18}, {22, -22}}, textString = "soc",
            horizontalAlignment =                                                                                                                                                                                                        TextAlignment.Left), Text(origin = {-2, -140}, lineColor = {128, 128, 128}, fillColor = {215, 215, 215},
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent = {{-78, 138}, {22, 98}}, textString = "vel",
            horizontalAlignment =                                                                                                                                                                                                        TextAlignment.Left), Text(origin = {-2, -134}, lineColor = {128, 128, 128}, fillColor = {215, 215, 215},
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent = {{-78, 78}, {22, 38}}, textString = "ms",
            horizontalAlignment =                                                                                                                                                                                                        TextAlignment.Left), Text(origin = {-2, -124}, lineColor = {128, 128, 128}, fillColor = {215, 215, 215},
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent = {{-78, -42}, {22, -82}}, textString = "me",
            horizontalAlignment =                                                                                                                                                                                                        TextAlignment.Left), Text(lineColor = {128, 128, 128}, fillColor = {215, 215, 215},
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent = {{40, 72}, {140, 32}}, textString = "rd",
            horizontalAlignment =                                                                                                                                                                                                        TextAlignment.Left), Text(lineColor = {128, 128, 128}, fillColor = {215, 215, 215},
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent = {{40, -28}, {140, -68}}, textString = "bd",
            horizontalAlignment =                                                                                                                                                                                                        TextAlignment.Left), Text(origin = {2, 118}, fillColor = {215, 215, 215},
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent = {{-160, 208}, {158, 158}}, textString = "REGEN BRAKING"), Text(origin={
              -35.4,155.4},                                                                                                                                                                                                        lineColor = {128, 128, 128}, fillColor = {215, 215, 215},
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent={{
              -54.6,96.6},{15.4,68.6}},                                                                                                                                                                                                        textString = "RB_Status",
            horizontalAlignment =                                                                                                                                                                                                        TextAlignment.Left), Text(origin={
              -14,6},                                                                                                                                                                                                        lineColor = {128, 128, 128}, fillColor = {215, 215, 215},
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent = {{-78, 138}, {22, 98}}, textString = "Acc_Demand",
            horizontalAlignment =                                                                                                                                                                                                        TextAlignment.Left), Text(origin={
              -33.84,104.3},                                                                                                                                                                                                        lineColor = {128, 128, 128}, fillColor = {215, 215, 215},
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent={{
              -56.16,89.7},{15.84,63.7}},                                                                                                                                                                                                        textString = "RB_Level",
            horizontalAlignment =                                                                                                                                                                                                        TextAlignment.Left), Text(origin = {-14, -54}, lineColor = {128, 128, 128}, fillColor = {215, 215, 215},
            fillPattern =                                                                                                                                                                                                        FillPattern.Solid, extent = {{-78, 138}, {22, 98}}, textString = "Gear_Ratio",
            horizontalAlignment =                                                                                                                                                                                                        TextAlignment.Left)}),
      Diagram(coordinateSystem(extent={{-100,-160},{100,200}})));
end RegenBrakingStrategy_Horizont;

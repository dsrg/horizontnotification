within HorizontNotification.Internal.PolylineFunctions;
function distance_to_segment
  "Calculates a shortest distance to the polyline segment and returns the closest point within the segment"
  import v_length = Modelica.Math.Vectors.length;
  import Modelica.Math.Vectors.normalize;
  input  SI.Position s_p_s[3] "Start point of a segment";
  input  SI.Position s_p_e[3] "End point of a segment";
  input  SI.Position p[ 3] "Reference point";
  output SI.Distance dist "distance to the segment";
  output  SI.Position p_s[3] "Closest point on the segment";
protected
  SI.Position seg_vec[3] = s_p_e -s_p_s;
  SI.Position pos_vec[3] = p - s_p_s;
  SI.Length seg_l =  v_length(seg_vec);
  Real proj= max(0,min(seg_vec * pos_vec / max(seg_l*seg_l, 1.0e-6),1)); // We clamp t from [0,1] to handle points outside the segment.
algorithm
  if seg_l == 0 then
    dist :=v_length(s_p_e - p);
    p_s :=s_p_e;
  else
    p_s :=s_p_s + seg_vec*proj;
    dist :=v_length(p_s - p);
  end if;
  // loking for the projection

end distance_to_segment;

within HorizontNotification.Internal.PolylineFunctions;
record line_record
  // A * x + B * y = C
  parameter Modelica.SIunits.Position A;
  parameter Modelica.SIunits.Position B;
  parameter Real C;
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end line_record;

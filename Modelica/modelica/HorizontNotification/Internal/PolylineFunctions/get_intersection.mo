within HorizontNotification.Internal.PolylineFunctions;
function get_intersection
  "Finds the intersection point if exists. If it does it returns the coordinates."
  input HorizontNotification.Internal.PolylineFunctions.line_record L1 "First line section";
  input HorizontNotification.Internal.PolylineFunctions.line_record L2 "Second line section";
  output Boolean exist;
  output Modelica.SIunits.Position x;
  output Modelica.SIunits.Position y;
protected
  Real D, Dx, Dy;
algorithm
    D := L1.A * L2.B - L1.B * L2.A;
    if D <> 0 then
      Dx := L1.C * L2.B - L1.B * L2.C;
      Dy := L1.A * L2.C - L1.C * L2.A;
      exist := true;
      x := Dx / D;
      y := Dy / D;
    else
      exist := false;
      x := 0;
      y := 0;
    end if;
  annotation (Documentation(info="<html>
<p>So we have linear system:</p>
<p>A1&nbsp;* x + B1&nbsp;* y = C1</p>
<p>A2&nbsp;* x + B2&nbsp;* y = C2</p>
<p>let&apos;s do it with Cramer&apos;s rule, so solution can be found in determinants:</p>
<p>x = Dx/D</p>
<p>y = Dy/D</p>
<p>where&nbsp;D&nbsp;is main determinant of the system:</p>
<p>A1&nbsp;B1</p>
<p>A2&nbsp;B2</p>
<p>and&nbsp;Dx&nbsp;and&nbsp;Dy&nbsp;can be found from matricies:</p>
<p>C1&nbsp;B1</p>
<p>C2&nbsp;B2</p>
<p>and</p>
<p>A1&nbsp;C1</p>
<p>A2&nbsp;C2</p>
<p>for&nbsp;D:</p>
<p>L1.A L1.B</p>
<p>L2.A L2.B</p>
<p>for&nbsp;Dx:</p>
<p>L1.C L1.B</p>
<p>L2.C L2.B</p>
<p>for&nbsp;Dy:</p>
<p>L1.A L1.C</p>
<p>L2.A L2.C</p>
</html>"));
end get_intersection;

within HorizontNotification.Internal.PolylineFunctions;
function polyline_length_vector "Cumulative length of the polyline"
  import v_length = Modelica.Math.Vectors.length;
  input SI.Position x[:] "X coordinates of the polyline";
  input SI.Position y[size(x,1)] "Y coordinates of the polyline";
  input SI.Position z[size(x,1)] = zeros(size(x,1)) "Z coordinates of the polyline";
  output SI.Length d[size(x,1)] "Z coordinates of the polyline";

algorithm
  d[1] :=0;
  for i in 2:(size(x,1)) loop
    d[i] :=d[i - 1] + v_length({x[i - 1] - x[i],y[i - 1] - y[i],z[i - 1] - z[i]});
  end for;
end polyline_length_vector;

within HorizontNotification.Internal.PolylineFunctions;
record Polyline
  parameter Integer n=10 "Number of points";
  parameter SI.Position x[n] = zeros(n) "X coordinates";
  parameter SI.Position y[n] = zeros(n) "Y coordinates";
  parameter SI.Position z[n] = zeros(n) "Z coordinates";
  parameter SI.Length d_c[n]= zeros(n) "Cumulative length oh the polyline";

  encapsulated function heading_at_index
    "Returns a normal direction vector at i , i+1 if the last index"
    import Modelica;
    import HorizontNotification.Internal.PolylineFunctions.Polyline;
    import SI = Modelica.SIunits;
    input Polyline poly;
    input Integer  i;
    output SI.Position heading[3];
  algorithm
    if i >= poly.n-1 then
      heading :={poly.x[poly.n] - poly.x[poly.n - 1],
                 poly.y[poly.n] - poly.y[poly.n - 1],
                 poly.z[poly.n] - poly.z[poly.n - 1]};
    else
       heading :={poly.x[i+1] - poly.x[i],
                  poly.y[i+1] - poly.y[i],
                  poly.z[i+1] - poly.z[i]};
    end if;
  end heading_at_index;

encapsulated function heading_at_distance
    "Returns a normal direction vector at i , i+1 if the last index"
    import Modelica;
    import HorizontNotification.Internal.PolylineFunctions.Polyline;
    import SI = Modelica.SIunits;
    input  Polyline poly;
    input  Modelica.SIunits.Length d;
    output SI.Position heading[3];
  protected
    Integer ind = poly.index_at_distance(poly = poly, d = d);
algorithm
    if d >= poly.d_c[poly.n] then
      ind := poly.n-1;
    end if;
    heading :=poly.point_at_index(poly=poly, i=ind + 1) - poly.point_at_index(
      poly=poly, i=ind);
end heading_at_distance;

  encapsulated function point_at_index
    import Modelica;
    import HorizontNotification.Internal.PolylineFunctions.Polyline;
    input Polyline poly;
    input Integer  i(min= 1,  max = poly.n);
    output Modelica.SIunits.Position point[3];
  algorithm
    point :={poly.x[i],poly.y[i],poly.z[i]};
  end point_at_index;

  encapsulated function index_at_distance
    import Modelica;
    import HorizontNotification.Internal.PolylineFunctions.Polyline;
    input Polyline poly;
    input  Modelica.SIunits.Length d;
    output Integer  ind;
  algorithm
    if d <= 0 then
      ind :=1;
      return;
    elseif d >= poly.d_c[poly.n] then
      ind := poly.n;
      return;
    end if;

    for i in 2:poly.n loop
      ind :=i - 1;
      if poly.d_c[i] >= d then
        break;
      end if;
    end for;
  end index_at_distance;

end Polyline;

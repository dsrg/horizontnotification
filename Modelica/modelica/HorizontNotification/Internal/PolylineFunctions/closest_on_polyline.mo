within HorizontNotification.Internal.PolylineFunctions;
function closest_on_polyline "Looking for the closest point on the polyline
  in an interval (last_point - max_dist : last_point +max_dist
  returns the point and distance along the polyline to this point for
  further itteration"
    import v_length = Modelica.Math.Vectors.length;

    input HorizontNotification.Internal.PolylineFunctions.Polyline poly;
    input SI.Position p[3] "Reference point";
    input SI.Length last_point = 0;
    input SI.Length max_dist = 100 "Maximal distance flom last";
    input Boolean interpolate = true;
    input Boolean search_all = true;
    output SI.Position pos_o[3];
    output SI.Length poly_dist;
    // for the initial we will search the whole polylone
protected
    SI.Length max_dist_calc = if search_all then  poly.d_c[poly.n] else max_dist;
    Integer from = min(poly.n-1,HorizontNotification.Internal.PolylineFunctions.Polyline.index_at_distance(
                                                           poly= poly, d = last_point - max_dist_calc));
    Integer to =  max(2,HorizontNotification.Internal.PolylineFunctions.Polyline.index_at_distance(
                                                   poly= poly, d = last_point + max_dist_calc));
    Real min_dist = v_length(HorizontNotification.Internal.PolylineFunctions.Polyline.point_at_index(
                                                     poly= poly, i= from)- p);
    Real tmp_dist;
    Integer temp_i = from;
    SI.Position tmp_point[3];
algorithm

      for i in from+1:to loop
    (tmp_dist,tmp_point) := HorizontNotification.Internal.PolylineFunctions.distance_to_segment(
      HorizontNotification.Internal.PolylineFunctions.Polyline.point_at_index(poly=poly, i=i),
      HorizontNotification.Internal.PolylineFunctions.Polyline.point_at_index(poly=poly, i=i - 1),
      p);
        if tmp_dist <= min_dist then
          min_dist :=tmp_dist;
          temp_i :=i-1;
          pos_o :=tmp_point;
        end if;
      end for;
  poly_dist := poly.d_c[temp_i] + v_length(
    HorizontNotification.Internal.PolylineFunctions.Polyline.point_at_index(poly=poly, i=temp_i) -
    pos_o);

      if interpolate and  temp_i>= poly.n-1 then
          // polyline can be behind
    (tmp_dist,pos_o) := HorizontNotification.Internal.PolylineFunctions.distance_to_vector(
      v_p=HorizontNotification.Internal.PolylineFunctions.Polyline.point_at_index(poly=poly, i=poly.n),
      v_h=HorizontNotification.Internal.PolylineFunctions.Polyline.point_at_index(poly=poly, i=poly.n
         - 1) - HorizontNotification.Internal.PolylineFunctions.Polyline.point_at_index(poly=poly,
        i=(poly.n)),
      p=p);
          if tmp_dist <= min_dist then
            min_dist := tmp_dist;
      poly_dist := poly.d_c[poly.n] + v_length(
        HorizontNotification.Internal.PolylineFunctions.Polyline.point_at_index(poly=poly, i=poly.n)
         - pos_o);
          end if;
      elseif  interpolate and temp_i ==1 then
        // polyline can be ahead
    (tmp_dist,tmp_point) := HorizontNotification.Internal.PolylineFunctions.distance_to_vector(
      v_p=HorizontNotification.Internal.PolylineFunctions.Polyline.point_at_index(poly=poly, i=1),
      v_h=HorizontNotification.Internal.PolylineFunctions.Polyline.point_at_index(poly=poly, i=2) -
        HorizontNotification.Internal.PolylineFunctions.Polyline.point_at_index(poly=poly, i=1),
      p=p);
         if tmp_dist <= min_dist then
            min_dist := tmp_dist;
            pos_o := tmp_point;
            poly_dist := 0;
         end if;

      end if;

end closest_on_polyline;

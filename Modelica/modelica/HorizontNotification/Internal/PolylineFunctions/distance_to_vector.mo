within HorizontNotification.Internal.PolylineFunctions;
function distance_to_vector
  "Calculates a shortest distance to a line defined as a point and heading vector and returns the closest point liying on the vector"
  import v_length = Modelica.Math.Vectors.length;
  import Modelica.Math.Vectors.normalize;
  input  SI.Position v_p[3] "Vector point";
  input  SI.Position v_h[3] "Vector heading";
  input  SI.Position p[ 3] "Reference point";

  output SI.Distance dist "distance to the vector";
  output  SI.Position p_s[3] "Closest point on the vector";
protected
  SI.Position vh_n[3] = Modelica.Math.Vectors.normalize(v_h);

  SI.Position pos_vec[3] = p - v_p;
  Real proj= vh_n * pos_vec;
algorithm
    p_s :=v_p + vh_n*proj;
    dist :=v_length(p_s - p);
end distance_to_vector;

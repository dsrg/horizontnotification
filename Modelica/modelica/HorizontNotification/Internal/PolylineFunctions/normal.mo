within HorizontNotification.Internal.PolylineFunctions;
function normal "Function to find normalized normal to a vector"
  input Real vec[3];
  input Boolean clockwise = true "direction of normal clockwise (right)";
  output Real[3] norm;
protected
  Real R[3,3] = if not clockwise then
   [0,-1,0;
    1,0,0;
    0,0,1]
 else
   [0,1,0;
   -1,0,0;
    0,0,1];
algorithm
  norm := Modelica.Math.Vectors.normalize(R*vec);
end normal;

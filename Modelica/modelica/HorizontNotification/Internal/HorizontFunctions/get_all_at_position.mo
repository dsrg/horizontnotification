within HorizontNotification.Internal.HorizontFunctions;
function get_all_at_position
    input HorizontNotification.Internal.HorizontFunctions.Horizont_2020 h_obj;
    input Real  x;
    input Real  y;
    input Real  z;
    input Boolean search_all = false;
    input Real search_dist = 100;
    output Boolean status;
    output Real pos_on_polyline;
    output Real max_speed;
    output Real recomended_speed;
    output Real coasting_speed;
    external "C"
    status=get_all_at_position(h_obj,x,y,z,search_all,search_dist, pos_on_polyline,max_speed,recomended_speed,coasting_speed)
        annotation(Library="HorizontDLL",
               Include="#include \"HorizontDLL.h\"",
               IncludeDirectory=
        "modelica://HorizontNotification/Resources/Include",
               LibraryDirectory=
        "modelica://HorizontNotification/Resources/Library");

end get_all_at_position;

within HorizontNotification.Internal.HorizontFunctions;
function get_num_rows
   input HorizontNotification.Internal.HorizontFunctions.Horizont_2020 h_obj;
   output Integer rows;
    external "C"
     rows = get_horizont_rows(h_obj)
        annotation(Library="HorizontDLL",
               Include="#include \"HorizontDLL.h\"",
               IncludeDirectory=
        "modelica://HorizontNotification/Resources/Include",
               LibraryDirectory=
        "modelica://HorizontNotification/Resources/Library");
end get_num_rows;

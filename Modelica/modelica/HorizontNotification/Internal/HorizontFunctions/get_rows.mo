within HorizontNotification.Internal.HorizontFunctions;
function get_rows "evaluation of current vehicle speed"
  input HorizontNotification.Internal.HorizontFunctions.Horizont_2020 h_obj;
    output Real current_speed;
    external "C"
    current_speed=get_horizont_rows(h_obj)
        annotation(Library="HorizontDLL",
               Include="#include \"HorizontDLL.h\"",
               IncludeDirectory=
        "modelica://HorizontNotification/Resources/Include",
               LibraryDirectory=
        "modelica://HorizontNotification/Resources/Library");

end get_rows;

within HorizontNotification.Internal.HorizontFunctions;
function get_max_speed_at_position
    input HorizontNotification.Internal.HorizontFunctions.Horizont_2020 h_obj;
    input Real  x;
    input Real  y;
    input Real  z;
    output Real max_speed;
    external "C"
    max_speed=get_max_speed_at_position(h_obj, x,y,z)
        annotation(Library="HorizontDLL",
               Include="#include \"HorizontDLL.h\"",
               IncludeDirectory=
        "modelica://HorizontNotification/Resources/Include",
               LibraryDirectory=
        "modelica://HorizontNotification/Resources/Library");

end get_max_speed_at_position;

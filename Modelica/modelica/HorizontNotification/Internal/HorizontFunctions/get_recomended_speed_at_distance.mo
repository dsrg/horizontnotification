within HorizontNotification.Internal.HorizontFunctions;
function get_recomended_speed_at_distance
  "evaluation of current vehicle speed"
  input HorizontNotification.Internal.HorizontFunctions.Horizont_2020 h_obj;
    input Real  current_position;
    output Real current_speed;
    external "C"
    current_speed=get_recomended_speed_at_distance(h_obj, current_position)
        annotation(Library="HorizontDLL",
               Include="#include \"HorizontDLL.h\"",
               IncludeDirectory=
        "modelica://HorizontNotification/Resources/Include",
               LibraryDirectory=
        "modelica://HorizontNotification/Resources/Library");

end get_recomended_speed_at_distance;

within HorizontNotification.Internal.HorizontFunctions;
function get_position_at_polyline_loc
    input HorizontNotification.Internal.HorizontFunctions.Horizont_2020 h_obj;
    input Real pos_on_polyline;
    output Real position[3];
protected
    Boolean stat;
    external "C"
    stat=get_pos_at_polyline_location(h_obj, pos_on_polyline, position,  3)
        annotation(Library="HorizontDLL",
               Include="#include \"HorizontDLL.h\"",
               IncludeDirectory=
        "modelica://HorizontNotification/Resources/Include",
               LibraryDirectory=
        "modelica://HorizontNotification/Resources/Library");

end get_position_at_polyline_loc;

within HorizontNotification.Internal.HorizontFunctions;
function get_max_speed_at_distance
  "evaluation of current vehicle speed"
  input HorizontNotification.Internal.HorizontFunctions.Horizont_2020 h_obj;
    input Real  current_position;
    output Real max_speed;
    external "C"
    max_speed=get_max_speed_at_distance(h_obj, current_position)
        annotation(Library="HorizontDLL",
               Include="#include \"HorizontDLL.h\"",
               IncludeDirectory=
        "modelica://HorizontNotification/Resources/Include",
               LibraryDirectory=
        "modelica://HorizontNotification/Resources/Library");

end get_max_speed_at_distance;

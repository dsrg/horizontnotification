within HorizontNotification.Internal.HorizontFunctions;
function get_x
  input HorizontNotification.Internal.HorizontFunctions.Horizont_2020 h_obj;
    input Integer  size;
    output Real x[size];
    external "C"
     get_horizont_x_vec(h_obj, x, size)
        annotation(Library="HorizontDLL",
               Include="#include \"HorizontDLL.h\"",
               IncludeDirectory=
        "modelica://HorizontNotification/Resources/Include",
               LibraryDirectory=
        "modelica://HorizontNotification/Resources/Library");
end get_x;

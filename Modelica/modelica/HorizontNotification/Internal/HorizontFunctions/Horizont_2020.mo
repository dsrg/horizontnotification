within HorizontNotification.Internal.HorizontFunctions;
class Horizont_2020
  extends ExternalObject;

    function constructor

      input String filename;
      input Real Cd;
      input Real A;
      input Real q;
      input Real m;
      input Real g;
      input Real f;
      input Real Pmax;
      input Real Tmax;
      output Horizont_2020 h_obj;
      external "C" h_obj = initialize_horizont(filename, Cd, A, q, m, g, f, Pmax, Tmax)
        annotation(Library="HorizontDLL",
                   Include="#include \"HorizontDLL.h\"",
                   IncludeDirectory=
          "modelica://HorizontNotification/Resources/Include",
                   LibraryDirectory=
          "modelica://HorizontNotification/Resources/Library");

    end constructor;

    function destructor
        input Horizont_2020 h_obj;
    external "C" deinitialize_horizont(h_obj)
        annotation(Library="HorizontDLL",
                   Include="#include \"HorizontDLL.h\"",
                   IncludeDirectory=
          "modelica://HorizontNotification/Resources/Include",
                   LibraryDirectory=
          "modelica://HorizontNotification/Resources/Library");
    end destructor;

annotation (
Documentation(info="<html>
<p>An interface to an external object that is used by interpolation classes. 
The constructor reads a file containing a matrix [:,:] of numeric values into
an object in memory. The destructor deletes the object in memory.</p>
<p>If the table only contains one row of data (matrix[1,:]), the data will be
treated as a column in memory. It means in this case following two inputs are
the same:</p>
<p style='margin-left: 2em;'>x_arr1=[1,2,3,4,5];</p>
<p style='margin-left: 2em;'>x_arr1=[1;2;3;4;5];</p>
</html>"));

end Horizont_2020;

within HorizontNotification.Internal.HorizontFunctions;
function get_coasting_speed_at_position
    input HorizontNotification.Internal.HorizontFunctions.Horizont_2020 h_obj;
    input Real  x;
    input Real  y;
    input Real  z;
    output Real current_speed;
    external "C"
    current_speed=get_coasting_speed_at_position(h_obj, x,y,z)
        annotation(Library="HorizontDLL",
               Include="#include \"HorizontDLL.h\"",
               IncludeDirectory=
        "modelica://HorizontNotification/Resources/Include",
               LibraryDirectory=
        "modelica://HorizontNotification/Resources/Library");

end get_coasting_speed_at_position;

within HorizontNotification.Internal.HorizontFunctions;
function get_polyline_location_at_position
    input HorizontNotification.Internal.HorizontFunctions.Horizont_2020 h_obj;
    input Real  x;
    input Real  y;
    input Real  z;
    input Boolean search_all = false;
    input Real sarch_distance = 100;
    output Real pos_on_polyline;
    external "C"
    pos_on_polyline=get_distance_at_position(h_obj, x,y,z,search_all,sarch_distance)
        annotation(Library="HorizontDLL",
               Include="#include \"HorizontDLL.h\"",
               IncludeDirectory=
        "modelica://HorizontNotification/Resources/Include",
               LibraryDirectory=
        "modelica://HorizontNotification/Resources/Library");

end get_polyline_location_at_position;

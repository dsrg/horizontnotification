within HorizontNotification.Internal.HorizontFunctions;
function get_polyline_length
    input HorizontNotification.Internal.HorizontFunctions.Horizont_2020 h_obj;
    output Real length;
    external "C"
     length = get_horizont_dist_vec(h_obj)
        annotation(Library="HorizontDLL",
               Include="#include \"HorizontDLL.h\"",
               IncludeDirectory=
        "modelica://HorizontNotification/Resources/Include",
               LibraryDirectory=
        "modelica://HorizontNotification/Resources/Library");
end get_polyline_length;

within HorizontNotification.Internal.HorizontFunctions;
function get_distance_vector
    input HorizontNotification.Internal.HorizontFunctions.Horizont_2020 h_obj;
    input Integer  size;
    output Real dist[size];
    external "C"
     get_horizont_dist_vec(h_obj, dist, size)
        annotation(Library="HorizontDLL",
               Include="#include \"HorizontDLL.h\"",
               IncludeDirectory=
        "modelica://HorizontNotification/Resources/Include",
               LibraryDirectory=
        "modelica://HorizontNotification/Resources/Library");
end get_distance_vector;

within HorizontNotification.Internal.HorizontFunctions;
function get_z
  input HorizontNotification.Internal.HorizontFunctions.Horizont_2020 h_obj;
    input Integer  size;
    output Real z[size];
    external "C"
     get_horizont_z_vec(h_obj, z, size)
        annotation(Library="HorizontDLL",
               Include="#include \"HorizontDLL.h\"",
               IncludeDirectory=
        "modelica://HorizontNotification/Resources/Include",
               LibraryDirectory=
        "modelica://HorizontNotification/Resources/Library");
end get_z;

within HorizontNotification.Internal.HorizontFunctions;
function get_all_at_distance
    input HorizontNotification.Internal.HorizontFunctions.Horizont_2020 h_obj;
    input Real  polyline_location;
    output Boolean status;
    output Real max_speed;
    output Real recomended_speed;
    output Real coasting_speed;
    external "C"
    status=get_all_at_distance(h_obj, polyline_location,max_speed,recomended_speed,coasting_speed)
        annotation(Library="HorizontDLL",
               Include="#include \"HorizontDLL.h\"",
               IncludeDirectory=
        "modelica://HorizontNotification/Resources/Include",
               LibraryDirectory=
        "modelica://HorizontNotification/Resources/Library");



end get_all_at_distance;

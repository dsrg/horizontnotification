within HorizontNotification.Internal.HorizontFunctions;
function get_y
  input HorizontNotification.Internal.HorizontFunctions.Horizont_2020 h_obj;
    input Integer  size;
    output Real y[size];
    external "C"
     get_horizont_y_vec(h_obj, y, size)
        annotation(Library="HorizontDLL",
               Include="#include \"HorizontDLL.h\"",
               IncludeDirectory=
        "modelica://HorizontNotification/Resources/Include",
               LibraryDirectory=
        "modelica://HorizontNotification/Resources/Library");
end get_y;

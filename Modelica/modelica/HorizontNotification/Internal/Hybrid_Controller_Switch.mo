within HorizontNotification.Internal;
model Hybrid_Controller_Switch

  Modelica.Blocks.Interfaces.RealInput engine_demand_in annotation (Placement(
        transformation(extent={{-140,160},{-100,200}}), iconTransformation(
          extent={{-140,160},{-100,200}})));
  Modelica.Blocks.Interfaces.RealInput engine_demand_in_2020 annotation (
      Placement(transformation(extent={{-140,120},{-100,160}}),
        iconTransformation(extent={{-140,120},{-100,160}})));
  Modelica.Blocks.Interfaces.RealInput brake_demand_in annotation (Placement(
        transformation(extent={{-140,60},{-100,100}}), iconTransformation(
          extent={{-140,60},{-100,100}})));
  Modelica.Blocks.Interfaces.RealInput brake_demand_in_2020 annotation (
      Placement(transformation(extent={{-140,20},{-100,60}}),
        iconTransformation(extent={{-140,20},{-100,60}})));
  Modelica.Blocks.Interfaces.RealInput motor_gen_demand_in annotation (
      Placement(transformation(extent={{-140,-40},{-100,0}}),
        iconTransformation(extent={{-140,-40},{-100,0}})));
  Modelica.Blocks.Interfaces.RealInput motor_gen_demand_in_2020 annotation (
      Placement(transformation(extent={{-140,-80},{-100,-40}}),
        iconTransformation(extent={{-140,-80},{-100,-40}})));
  Modelica.Blocks.Interfaces.RealInput engine_key_on_in annotation (Placement(
        transformation(extent={{-140,-140},{-100,-100}}), iconTransformation(
          extent={{-140,-160},{-100,-120}})));
  Modelica.Blocks.Interfaces.RealInput engine_key_on_in_2020 annotation (
      Placement(transformation(extent={{-140,-180},{-100,-140}}),
        iconTransformation(extent={{-140,-220},{-100,-180}})));
  Modelica.Blocks.Interfaces.RealInput eclutch_engagement_in_2020 annotation (
      Placement(transformation(extent={{-140,-276},{-100,-236}}),
        iconTransformation(extent={{-140,-276},{-100,-236}})));
  Modelica.Blocks.Interfaces.RealInput eclutch_engagement_in annotation (
      Placement(transformation(extent={{-140,-240},{-100,-200}}),
        iconTransformation(extent={{-140,-330},{-100,-290}})));
  Modelica.Blocks.Interfaces.RealOutput eclutch_engagement annotation (
      Placement(transformation(extent={{60,-260},{100,-220}}),
        iconTransformation(extent={{60,128},{106,174}})));
  Modelica.Blocks.Interfaces.RealOutput engine_demand annotation (Placement(
        transformation(extent={{60,140},{102,182}}), iconTransformation(extent={
            {60,-116},{106,-70}})));
  Modelica.Blocks.Interfaces.RealOutput brake_demand annotation (Placement(
        transformation(extent={{60,40},{100,80}}), iconTransformation(extent={{60,
            -358},{106,-312}})));
  Modelica.Blocks.Interfaces.RealOutput motor_gen_demand annotation (Placement(
        transformation(extent={{60,-60},{98,-22}}), iconTransformation(extent={{
            60,-250},{106,-204}})));
  Modelica.Blocks.Interfaces.RealOutput engine_key_on annotation (Placement(
        transformation(extent={{60,-158},{98,-120}}), iconTransformation(extent=
           {{60,8},{106,54}})));

  Modelica.Blocks.Interfaces.BooleanInput message_in annotation (Placement(
        transformation(extent={{-42,-304},{-2,-264}}),
                                                    iconTransformation(extent={{-42,
            -304},{-2,-264}})));
  Modelica.Blocks.Interfaces.BooleanInput RB_on
    annotation (Placement(transformation(extent={{-44,180},{-4,220}})));
equation

  if message_in and RB_on then
  eclutch_engagement=eclutch_engagement_in_2020;
  engine_demand=engine_demand_in_2020;
  brake_demand=brake_demand_in_2020;
  motor_gen_demand = motor_gen_demand_in_2020;
  engine_key_on=engine_key_on_in_2020;
  else
  eclutch_engagement=eclutch_engagement_in;
  engine_demand=engine_demand_in;
  brake_demand=brake_demand_in;
  motor_gen_demand = motor_gen_demand_in;
  engine_key_on=engine_key_on_in;
  end if;

  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-280},
            {60,200}})), Diagram(coordinateSystem(preserveAspectRatio=false,
          extent={{-100,-280},{60,200}})));
end Hybrid_Controller_Switch;

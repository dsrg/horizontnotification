within HorizontNotification.Internal;
model state_changer_filter

  Modelica.Blocks.Interfaces.RealInput    vehicle_velocity annotation (Placement(transformation(extent={{-10,-10},
          {10,10}},
      rotation=0,
      origin={-106,0}), iconTransformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={0,-100})));
  Modelica.Blocks.Interfaces.RealInput accn0(start=0.0)
    "Accn - Raw (unclipped) total signal"
    annotation (Placement(transformation(extent={{-110,50},{-90,70}}),
        iconTransformation(extent={{-110,50},{-90,70}})));
  Modelica.Blocks.Interfaces.RealInput brake0(start=0.0)
    "Brake - Raw (unclipped) total signal"                                           annotation (Placement(transformation(extent={{-112,
            -70},{-92,-50}}), iconTransformation(extent={{-112,-70},{-92,-50}})));

  Modelica.Blocks.Interfaces.IntegerOutput   state(start=PARKED) annotation (Placement(transformation(extent={{100,-8},
            {120,12}}), iconTransformation(extent={{100,-8},{120,12}})));
  Modelica.Blocks.Interfaces.RealOutput driver_accn_demand(start=0.0) annotation (Placement(transformation(extent={{100,50},
            {120,70}}), iconTransformation(extent={{100,50},{120,70}})));
  Modelica.Blocks.Interfaces.RealOutput driver_brake_demand( start=0.0) annotation (Placement(transformation(extent={{100,-70},
            {120,-50}})));

  parameter SI.Time accn_t = 0.05 "Accel filter time constant";
  parameter SI.Time brake_t = 0.05 "Brake filter time constant";
  parameter Real driver_brake_parked = 1.0;
  parameter Real  speed_cut_in_kmph = 0.01;      // distinguish between driving and parked states
  parameter Real accn_cut_in=0.05;  // distinguish between driving and parked states
  parameter Real brake_cut_in = 0.05;
  Boolean over_speed = abs(vehicle_velocity) < speed_cut_in_kmph;
  Boolean braking = brake0 > brake_cut_in;
  Boolean accing = accn0 > accn_cut_in;

protected
  constant Integer  PARKED =  101;
  constant Integer  DRIVING = 102;
  constant Integer  BRAKING = 103;
initial algorithm
  if accing then
    state:=DRIVING;
  else
    state :=PARKED;
  end if;

equation
  // --- state machine

algorithm
  when pre(state) == PARKED and accing and not braking then
    state := DRIVING;
       elsewhen
            pre(state) == DRIVING and braking and not accing then
    state := BRAKING;
       elsewhen
            pre(state) == BRAKING and over_speed and not accing then
    state :=PARKED;
       elsewhen
             pre(state) == BRAKING and accing and not braking then
    state := DRIVING;
  end when;

equation

  if state == PARKED then
    der(driver_accn_demand) = (0.0 - driver_accn_demand) / accn_t;
    der(driver_brake_demand) =  (driver_brake_parked - driver_brake_demand) / brake_t;
  elseif state == BRAKING then
    der(driver_accn_demand) = (0.0 - driver_accn_demand) / accn_t;
    der(driver_brake_demand) = (max(0.0, min(1.0, brake0)) - driver_brake_demand) / brake_t;
  else
    // --- DRIVING state
    der(driver_accn_demand) = (max(0.0, min(1.0, accn0)) - driver_accn_demand) / accn_t;
    der(driver_brake_demand) = (0.0 - driver_brake_demand) / brake_t;
  end if;

  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,100}})),
    Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,
            100}}), graphics={Text(
          extent={{-100,100},{100,-100}},
          lineColor={217,67,180},
          textString="S")}));
end state_changer_filter;

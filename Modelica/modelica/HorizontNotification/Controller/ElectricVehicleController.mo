within HorizontNotification.Controller;
model ElectricVehicleController
  /*==============================================================================
  * Description   : IGNITE Powertrain Library
  * Filename      : IPowertrain/Controllers/ElectricVehicleController.mo
  * Creation Date : 2 Jan 2013
  *===============: Copyright (c) 2013 Ricardo Software ==========================
  */

    parameter IStandard.Types.Path  data_file="" "Data file name" annotation (Dialog(  tab = "Regenerative braking"));

    // regeneration braking
    parameter Boolean rb_enabled=true "Regeneration braking enabled"  annotation (Dialog(  tab = "Regenerative braking"));
    parameter Real    rb_max_brake_force(unit="N")
    "Maximum friction brake force" annotation (Dialog(  tab = "Regenerative braking"));
    parameter Real    rb_speed_low(unit="m/s") "Low speed regeneration limit" annotation (Dialog(  tab = "Regenerative braking"));

    // gear shift
  parameter Real    gear_shift(unit="s")=0.5 "Gear shift period" annotation (Dialog(  tab = "Gear shift"));

  parameter Boolean gbx_discrete=false "Gearbox has discrete ratios" annotation (Dialog(  tab = "Gear shift"));

  parameter Real soc_high=1.0 "Maximum SOC to allow regeneration"
    annotation (Dialog(tab="Regenerative braking"));

  // PID
  parameter Modelica.Blocks.Types.SimpleController controllerType=.Modelica.Blocks.Types.SimpleController.PID
    "Type of controller" annotation (Dialog(tab="PID"));
  parameter Real k=1 "Gain of controller"
    annotation (Dialog(tab="PID"));
  parameter SI.Time Ti=0.5 "Time constant of Integrator block"
    annotation (Dialog(tab="PID"));
  parameter SI.Time Td=0.1 "Time constant of Derivative block"
    annotation (Dialog(tab="PID"));


  Internal.RegenBrakingStrategy_Horizont regenBrakingStrategy(
    enabled=rb_enabled,
    max_brake_force=rb_max_brake_force,
    speed_low=rb_speed_low,
    data_file=data_file,
    soc_high=soc_high)
    annotation (Placement(transformation(extent={{-42,8},{-14,44}})));
  IPowertrain.Interfaces.SystemBus   bus
    annotation (Placement(transformation(extent={{-10,-100},{10,-80}})));
  Modelica.Blocks.Math.Add add
    annotation (Placement(transformation(extent={{60,-22},{80,-2}})));
  Modelica.Blocks.Sources.Constant const(k=1)
    annotation (Placement(transformation(extent={{-56,3},{-50,9}})));
  Modelica.Blocks.Continuous.FirstOrder gear_shift_control(T=gear_shift/2,
      initType=Modelica.Blocks.Types.Init.SteadyState)
    "fixed time constant filter applied to target gear signal"
    annotation (Placement(transformation(extent={{-12,-72},{8,-52}})));
  Modelica.Blocks.Math.IntegerToReal integerToReal
    annotation (Placement(transformation(extent={{-40,-72},{-20,-52}})));
  Modelica.Blocks.Routing.RealPassThrough gbx_not_discrete if not gbx_discrete
    annotation (Placement(transformation(extent={{20,-72},{40,-52}})));
  Modelica.Blocks.Math.RealToInteger gbx_is_discrete if gbx_discrete
    annotation (Placement(transformation(extent={{20,-44},{40,-24}})));
  Modelica.Blocks.Interfaces.RealInput reg_level annotation (Placement(
        transformation(extent={{-134,-64},{-94,-24}}),iconTransformation(extent={{-134,
            -64},{-94,-24}})));
  Modelica.Blocks.Interfaces.BooleanInput coasin_status annotation (Placement(
        transformation(extent={{124,68},{84,108}}), iconTransformation(extent={
            {110,-20},{70,20}})));

  Modelica.Blocks.Nonlinear.Limiter limiter(uMax=1, uMin=0) annotation (
      Placement(transformation(
        extent={{5,-5},{-5,5}},
        rotation=180,
        origin={-67,43})));
  Modelica.Blocks.Interfaces.RealInput coasting_speed annotation (Placement(
        transformation(extent={{124,40},{84,80}}), iconTransformation(extent={{-134,
            -10},{-94,30}})));
  Modelica.Blocks.Logical.Switch switch1
    annotation (Placement(transformation(extent={{8,28},{28,48}})));
  Modelica.Blocks.Continuous.LimPID PID(yMax=0, yMin=-1,
    controllerType=controllerType,
    k=k,
    Ti=Ti,
    Td=Td)
    annotation (Placement(transformation(extent={{66,46},{40,72}})));


equation
  connect(regenBrakingStrategy.mech_brake_demand, bus.brake_demand)
    annotation (Line(
      points={{-12.88,19},{90,19},{90,-90},{0,-90}},
      color={0,0,127},
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(bus.motor_gen_demand, add.y)         annotation (Line(
      points={{0,-90},{86,-90},{86,-12},{81,-12}},
      color={0,0,127},
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(add.u1, bus.driver_accn_demand)         annotation (Line(
      points={{58,-6},{-54,-6},{-54,-90},{0,-90}},
      color={0,0,127},
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(regenBrakingStrategy.vehicle_velocity, bus.vehicle_velocity)
    annotation (Line(
      points={{-45.08,22},{-70,22},{-70,-90},{0,-90}},
      color={0,0,127},
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(regenBrakingStrategy.el_motor_speed, bus.motor_gen_speed)
    annotation (Line(
      points={{-45.08,16.6},{-66,16.6},{-66,-90},{0,-90}},
      color={0,0,127},
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(regenBrakingStrategy.battery_soc, bus.battery_soc)
    annotation (Line(
      points={{-45.08,11.4},{-62,11.4},{-62,-90},{0,-90}},
      color={0,0,127},
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(regenBrakingStrategy.brake_demand, bus.driver_brake_demand)
    annotation (Line(
      points={{-45.08,0.6},{-58,0.6},{-58,-90},{0,-90}},
      color={0,0,127},
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(integerToReal.y, gear_shift_control.u) annotation (Line(
      points={{-19,-62},{-18,-62},{-18,-64},{-16,-64},{-16,-62},{-14,-62}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(integerToReal.u, bus.shift_target_gear) annotation (Line(
      points={{-42,-62},{-46,-62},{-46,-90},{0,-90}},
      color={255,127,0},
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(gear_shift_control.y, gbx_not_discrete.u) annotation (Line(
      points={{9,-62},{12,-62},{12,-64},{14,-64},{14,-62},{18,-62}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gbx_not_discrete.y, bus.gearbox_gear) annotation (Line(
      points={{41,-62},{48,-62},{48,-90},{0,-90}},
      color={0,0,127},
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(gear_shift_control.y, gbx_is_discrete.u) annotation (Line(
      points={{9,-62},{12,-62},{12,-34},{18,-34}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gbx_is_discrete.y, bus.gearbox_gear) annotation (Line(
      points={{41,-34},{68,-34},{68,-90},{0,-90}},
      color={255,127,0},
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(regenBrakingStrategy.Acceleration_demand, bus.driver_accn_demand)
    annotation (Line(points={{-45.08,36.2},{-76,36.2},{-76,-90},{0,-90}},
        color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(regenBrakingStrategy.RB_Status, coasin_status) annotation (Line(
        points={{-45.36,47.8},{-46,47.8},{-46,48},{-46,50},{-46,82},{-46,88},{
          104,88}},                                            color={255,0,255}));
  connect(reg_level, limiter.u)
    annotation (Line(points={{-114,-44},{-82,-44},{-82,44},{-74,44},{-74,43},{
          -73,43}},                                        color={0,0,127}));
  connect(const.y, regenBrakingStrategy.el_motor_engaged) annotation (Line(
        points={{-49.7,6},{-45.08,6},{-45.08,5.6}}, color={0,0,127}));
  connect(regenBrakingStrategy.rb_level, limiter.y) annotation (Line(points={{
          -45.08,42.2},{-53.54,42.2},{-53.54,43},{-61.5,43}}, color={0,0,127}));
  connect(coasting_speed, PID.u_s) annotation (Line(points={{104,60},{90,60},{
          90,59},{68.6,59}}, color={0,0,127}));
  connect(regenBrakingStrategy.el_motor_regen_demand, switch1.u3) annotation (
      Line(points={{-12.88,29.2},{4.84,29.2},{4.84,30},{6,30}}, color={0,0,127}));
  connect(switch1.y, add.u2) annotation (Line(points={{29,38},{50,38},{50,-18},
          {58,-18}}, color={0,0,127}));
  connect(PID.y, switch1.u1)
    annotation (Line(points={{38.7,59},{6,59},{6,46}}, color={0,0,127}));
  connect(PID.u_m, bus.vehicle_velocity) annotation (Line(points={{53,43.4},{96,
          43.4},{96,-90},{0,-90}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(regenBrakingStrategy.RB_Coasting, switch1.u2) annotation (Line(points=
         {{-10.78,47.1},{-2.95,47.1},{-2.95,38},{6,38}}, color={255,0,255}));
annotation (
Diagram(
    coordinateSystem(preserveAspectRatio=false,extent={{-100,-100},{100,100}})),
Icon(
    coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},{100,100}}),
    graphics={
    Rectangle(
        extent={{-70,-90},{70,90}},
        pattern=LinePattern.Solid,
        fillColor={175,175,175},
        fillPattern=FillPattern.Solid,
        lineColor={0,0,0}),
    Rectangle(
        extent={{60,-90},{70,90}},
        pattern=LinePattern.Solid,
        fillColor={0,215,55},
        fillPattern=FillPattern.Solid,
        lineColor={0,0,0}),
    Polygon(
        points={{-70,90},{-60,100},{60,100},{70,90},{-70,90}},
        pattern=LinePattern.Solid,
        fillPattern=FillPattern.Solid,
        fillColor={0,5,200},
        lineColor={0,0,0}),
    Polygon(
        points={{-70,-90},{-60,-100},{60,-100},{70,-90},{-70,-90}},
        pattern=LinePattern.Solid,
        fillPattern=FillPattern.Solid,
        fillColor={0,5,200},
        lineColor={0,0,0}),
    Line(
        points={{20,-90},{20,90}},
        pattern=LinePattern.Solid,
        color={0,0,0},
        smooth=Smooth.None,
        thickness = 2),
    Rectangle(
        extent={{-70,50},{20,60}},
        pattern=LinePattern.Solid,
        fillColor={175,175,175},
        fillPattern=FillPattern.None,
        lineColor={0,0,0},
        lineThickness = 1),
    Rectangle(
        extent={{-70,30},{20,40}},
        pattern=LinePattern.Solid,
        fillColor={175,175,175},
        fillPattern=FillPattern.None,
        lineColor={0,0,0},
        lineThickness = 1),
    Rectangle(
        extent={{-70,10},{20,20}},
        pattern=LinePattern.Solid,
        fillColor={175,175,175},
        fillPattern=FillPattern.None,
        lineColor={0,0,0},
        lineThickness = 1),
    Rectangle(
        extent={{-70,-10},{20,0}},
        pattern=LinePattern.Solid,
        fillColor={175,175,175},
        fillPattern=FillPattern.None,
        lineColor={0,0,0},
        lineThickness = 1),
    Rectangle(
        extent={{-70,-30},{20,-20}},
        pattern=LinePattern.Solid,
        fillColor={175,175,175},
        fillPattern=FillPattern.None,
        lineColor={0,0,0},
        lineThickness = 1),
    Rectangle(
        extent={{-70,-50},{20,-40}},
        pattern=LinePattern.Solid,
        fillColor={175,175,175},
        fillPattern=FillPattern.None,
        lineColor={0,0,0},
        lineThickness = 1)}));
end ElectricVehicleController;

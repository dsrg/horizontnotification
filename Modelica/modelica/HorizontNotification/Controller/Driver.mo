within HorizontNotification.Controller;
model Driver

  parameter SI.Time accn_t = 0.05  "Accel filter time constant" annotation (Dialog(  tab = "Settings"));
  parameter SI.Time brake_t = 0.05 "Brake filter time constant" annotation (Dialog(  tab = "Settings"));
  parameter Real  speed_cut_in_kmph = 0.01 "Park State speed" annotation (Dialog(  tab = "Settings"));
  parameter Real accn_cut_in=0.05 "Acc cut in" annotation (Dialog(  tab = "Settings"));
  parameter Real brake_cut_in = 0.05 "Brakecut in" annotation (Dialog(  tab = "Settings"));

  Modelica.Blocks.Interfaces.RealInput input_acceleration annotation (Placement(transformation(
          extent={{-126,48},{-86,88}}), iconTransformation(extent={{-126,48},{-86,
            88}})));
  Modelica.Blocks.Interfaces.RealInput input_brake annotation (Placement(transformation(
          extent={{-124,-80},{-84,-40}}),
                                        iconTransformation(extent={{-124,-80},{-84,
            -40}})));
  IPowertrain.Interfaces.SystemBus bus
    annotation (Placement(transformation(extent={{-10,-108},{10,-88}})));
  HorizontNotification.Internal.state_changer_filter state_changer_filter(
    accn_t=accn_t,
    brake_t=brake_t,
    speed_cut_in_kmph=speed_cut_in_kmph,
    accn_cut_in=accn_cut_in,
    brake_cut_in)
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Modelica.Blocks.Nonlinear.Limiter limiter(uMax=1, uMin=0) annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={74,-42})));
  Modelica.Blocks.Nonlinear.Limiter limiter1(uMax=1, uMin=0) annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={28,-40})));

equation
  connect(bus.vehicle_velocity, state_changer_filter.vehicle_velocity)
    annotation (Line(
      points={{0,-98},{0,-98},{0,-10}},
      color={181,181,0},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(state_changer_filter.accn0, input_acceleration)
    annotation (Line(points={{-10,6},{-54,6},{-54,68},{-106,68}},   color={0,0,127}));
  connect(state_changer_filter.brake0, input_brake)
    annotation (Line(points={{-10.2,-6},{-10.2,-6},{-54,-6},{-54,-60},{-104,-60}},
                                                                                 color={0,0,127}));
  connect(state_changer_filter.state, bus.cycle_driver_state) annotation (Line(
        points={{11,0.2},{44,0.2},{44,-98},{0,-98}}, color={255,127,0}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(limiter.y, bus.driver_accn_demand) annotation (Line(points={{74,-53},
          {76,-53},{76,-98},{0,-98}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(limiter.u, state_changer_filter.driver_accn_demand) annotation (Line(
        points={{74,-30},{74,-30},{74,4},{11,4},{11,6}}, color={0,0,127}));
  connect(limiter1.y, bus.driver_brake_demand) annotation (Line(points={{28,-51},
          {28,-51},{28,-94},{28,-98},{0,-98}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(limiter1.u, state_changer_filter.driver_brake_demand)
    annotation (Line(points={{28,-28},{28,-6},{11,-6}}, color={0,0,127}));
  annotation ( __RModel(attributes="
  <scalar type='string' name='IPRIVATE__VD_type' label='IPRIVATE__VD_type'>driver</scalar>
  <scalar type='string' name='IPRIVATE__template_param_add' label='IPRIVATE__template_param_add' __comment='Template to add to the main model parameters session' __bfname='String'>
    Modelica.Blocks.Interfaces.RealInput  input_acceleration; 
    Modelica.Blocks.Interfaces.RealInput  input_brake; 
  </scalar>
  <scalar type='string' name='IPRIVATE__template_con_connect' label='IPRIVATE__template_con_connect' __comment='Template to connect aditional connectors' __bfname='String'>
    connect({{element.name}}._input_acceleration, input_acceleration);
  
    connect({{element.name}}._input_brake, input_brake);
  
  </scalar>  
  "),
  Diagram(
    coordinateSystem(preserveAspectRatio=false,extent={{-100,-100},{100,100}})),
Icon(
    coordinateSystem(preserveAspectRatio=false,extent={{-100,-100},{100,100}}),
    graphics={
  Polygon(  points={{-75,-20},{-90,40},{30,40},{15,-20},{-75,-20}},
            pattern=LinePattern.Solid,
            fillPattern=FillPattern.Solid,
            fillColor={175,175,175},
            lineColor={0,0,0}),
  Polygon(  points={{-35,40},{-25,80},{-15,80},{-25,40},{-35,40}},
            pattern=LinePattern.Solid,
            fillPattern=FillPattern.None,
            fillColor={175,175,175},
            lineColor={0,0,0}),
  Polygon(  points={{25,-85},{50,40},{90,40},{90,-85},{25,-85}},
            pattern=LinePattern.Solid,
            fillPattern=FillPattern.Solid,
            fillColor={175,175,175},
            lineColor={0,0,0}),
  Polygon(  points={{65,40},{75,80},{85,80},{75,40},{65,40}},
            pattern=LinePattern.Solid,
            fillPattern=FillPattern.None,
            fillColor={175,175,175},
            lineColor={0,0,0}),
  Line(     points={{-30,80},{-10,80}},
            pattern=LinePattern.Solid,
            color={0,0,0},
   smooth=Smooth.None,
   thickness = 1),
  Line(     points={{70,80},{90,80}},
            pattern=LinePattern.Solid,
            color={0,0,0},
   smooth=Smooth.None,
   thickness = 1),
  Ellipse(  extent={{-75,15},{-65,25}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1),
  Ellipse(  extent={{-55,15},{-45,25}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1),
  Ellipse(  extent={{-35,15},{-25,25}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1),
  Ellipse(  extent={{-15,15},{-5,25}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1),
  Ellipse(  extent={{5,15},{15,25}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1),
  Ellipse(  extent={{-65,-5},{-55,5}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1),
  Ellipse(  extent={{-45,-5},{-35,5}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1),
  Ellipse(  extent={{-25,-5},{-15,5}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1),
  Ellipse(  extent={{-5,-5},{5,5}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1),
  Ellipse(  extent={{37,-73},{43,-67}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1),
  Ellipse(  extent={{47,-58},{53,-52}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1),
  Ellipse(  extent={{47,-28},{53,-22}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1),
  Ellipse(  extent={{57,-73},{63,-67}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1),
  Ellipse(  extent={{57,-43},{63,-37}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1),
  Ellipse(  extent={{57,-13},{63,-7}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1),
  Ellipse(  extent={{57,17},{63,23}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1),
  Ellipse(  extent={{67,-58},{73,-52}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1),
  Ellipse(  extent={{67,-28},{73,-22}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1),
  Ellipse(  extent={{67,2},{73,8}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1),
  Ellipse(  extent={{77,-73},{83,-67}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1),
  Ellipse(  extent={{77,-43},{83,-37}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1),
  Ellipse(  extent={{77,-13},{83,-7}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1),
  Ellipse(  extent={{77,17},{83,23}},
            pattern=LinePattern.Solid,
            fillColor={0,5,200},
            fillPattern=FillPattern.Solid,
            lineColor={0,0,0},
   lineThickness = 1)}));
end Driver;

within HorizontNotification.Logger;
model Logger
  parameter Integer n= 1
                        "Number of columns"
    annotation(Dialog(tab="General",group = "General"));
  parameter String names[n]= {"var1"} "Header names"
    annotation(Dialog(tab="General",group = "General"));
  parameter String fname= "C:/Users/dr14/Desktop/TEMP_CAN_DELETE/first_test.txt"
    annotation(Dialog(tab="General",group = "General"));

  parameter Modelica.SIunits.Time samplePeriod(min=100*Modelica.Constants.eps, start=0.1) = 0.1 "Sample period of component"
      annotation(Dialog(tab="General",group = "Component sampling"));
  parameter Modelica.SIunits.Time startTime=0 "First sample time instant"
     annotation(Dialog(tab="General",group = "Component sampling"));

  Modelica.Blocks.Interfaces.RealInput u[n] annotation (Placement(transformation(
          extent={{-124,-20},{-84,20}}), iconTransformation(extent={{-124,-20},{
            -84,20}})));

protected
  parameter HorizontNotification.Logger.Internal.DataLogger logger=
      HorizontNotification.Logger.Internal.DataLogger(fname,names);

algorithm
  when sample(startTime, samplePeriod) then
      HorizontNotification.Logger.Internal.write_data(logger, u);
  end when;

  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(extent={{-100,100},{100,-100}}, lineColor={28,108,200}),
        Line(points={{-60,100},{-60,-100}}, color={28,108,200}),
        Line(points={{-20,100},{-20,-100}}, color={28,108,200}),
        Line(points={{20,100},{20,-100}}, color={28,108,200}),
        Line(points={{60,100},{60,-100}}, color={28,108,200}),
        Line(points={{-100,80},{100,80}}, color={28,108,200}),
        Line(points={{-100,60},{100,60}}, color={28,108,200}),
        Line(points={{-100,40},{100,40}}, color={28,108,200}),
        Line(points={{-100,20},{100,20}}, color={28,108,200}),
        Line(points={{-100,0},{100,0}}, color={28,108,200}),
        Line(points={{-100,-20},{100,-20}}, color={28,108,200}),
        Line(points={{-100,-40},{100,-40}}, color={28,108,200}),
        Line(points={{-100,-60},{100,-60}}, color={28,108,200}),
        Line(points={{-100,-80},{100,-80}}, color={28,108,200}),
        Text(
          extent={{-94,98},{-68,80}},
          lineColor={28,108,200},
          textString="var1"),
        Text(
          extent={{-54,98},{-28,80}},
          lineColor={28,108,200},
          textString="var2"),
        Text(
          extent={{-12,98},{14,80}},
          lineColor={28,108,200},
          textString="var3"),
        Text(
          extent={{28,98},{54,80}},
          lineColor={28,108,200},
          textString="var4"),
        Text(
          extent={{66,98},{92,80}},
          lineColor={28,108,200},
          textString="var5")}), Diagram(coordinateSystem(preserveAspectRatio=false)));
end Logger;

within HorizontNotification.Logger;
model bitRead
   parameter Integer n= 1
                        "Number of bits";
  parameter Integer bits_address[n] "Bits address";
  parameter String bits_names[n] "Names";
  Modelica.Blocks.Interfaces.IntegerInput u
    annotation (Placement(transformation(extent={{-132,-20},{-92,20}})));
  Modelica.Blocks.Interfaces.BooleanOutput y[n] annotation (Placement(
        transformation(extent={{94,-8},{114,12}}), iconTransformation(extent={{94,
            -8},{114,12}})));

algorithm
  y :=HorizontNotification.Logger.Internal.bit_read(
    n,
    u,
    bits_address);
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={Text(
          extent={{-56,54},{64,-50}},
          lineColor={28,108,200},
          textString="01001"), Rectangle(extent={{-90,52},{94,-46}}, lineColor={
              28,108,200})}),                                    Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end bitRead;

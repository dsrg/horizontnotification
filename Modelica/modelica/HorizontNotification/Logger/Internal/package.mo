within HorizontNotification.Logger;
package Internal

  class DataLogger
    extends ExternalObject;

      function constructor

        input String filename;
        input String[:] names;
        output DataLogger h_obj;
        external "C" h_obj = initialize_data_logger(filename, names, size(names,1))
          annotation(Include="#include \"client.c\"");

      end constructor;

      function destructor
          input DataLogger h_obj;
      external "C" deinitialize_data_logger(h_obj)
        annotation(Include="#include \"client.c\"");
      end destructor;

  annotation (
  Documentation(info="<html>
<p>An interface to an external object that is used by interpolation classes. 
The constructor reads a file containing a matrix [:,:] of numeric values into
an object in memory. The destructor deletes the object in memory.</p>
<p>If the table only contains one row of data (matrix[1,:]), the data will be
treated as a column in memory. It means in this case following two inputs are
the same:</p>
<p style='margin-left: 2em;'>x_arr1=[1,2,3,4,5];</p>
<p style='margin-left: 2em;'>x_arr1=[1;2;3;4;5];</p>
</html>"));

  end DataLogger;
end Internal;

within HorizontNotification.Logger.Internal;
function write_data

  input DataLogger h_obj;
  input Real[:] data;
  external "C" log_data(h_obj, data, size(data,1))
    annotation(Include="#include \"client.c\"");

end write_data;

within HorizontNotification.Logger.Internal;
function bit_read
  input Integer size;
  input Integer inp;
  input Integer[size] bits_address;
  output Boolean[size] bits_status;
  external "C" bit_read(inp, bits_address, bits_status,size)
    annotation(Include="#include \"client.c\"");

end bit_read;

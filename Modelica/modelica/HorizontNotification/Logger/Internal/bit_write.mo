within HorizontNotification.Logger.Internal;
function bit_write
  input Integer size;
  input Integer[:] bits_address;
  input Boolean[:] bits_status;
  output Integer res;
  external "C" res = bit_set(bits_address, bits_status,size)
    annotation(Include="#include \"client.c\"");

end bit_write;

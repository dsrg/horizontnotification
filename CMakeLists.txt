cmake_minimum_required (VERSION 2.6)
project (HORIZONT_DLL_ALL_IN_ONE)

#find_package(GLUT)
#find_package(OpenGL)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/Distribution/libs)
set(output_dir ${CMAKE_BINARY_DIR}/Distribution)

add_subdirectory(HorizontDLL)
add_subdirectory(Modelica)
add_subdirectory(HorizontTEST)





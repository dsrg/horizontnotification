#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  srand(QDateTime::currentDateTime().toTime_t());
  ui->setupUi(this);
  
  ui->customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes |
                                   QCP::iSelectPlottables);

  ui->custom_plot2->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes |
                                   QCP::iSelectPlottables);
  ui->customPlot->xAxis->setRange(-8, 8);
  ui->customPlot->yAxis->setRange(-5, 5);
  ui->customPlot->axisRect()->setupFullAxesBox();
  
  ui->customPlot->plotLayout()->insertRow(0);
  QCPTextElement *title = new QCPTextElement(ui->customPlot, "Interaction Example", QFont("sans", 17, QFont::Bold));
  ui->customPlot->plotLayout()->addElement(0, 0, title);
  
  ui->customPlot->xAxis->setLabel("x Axis");
  ui->customPlot->yAxis->setLabel("y Axis");
  ui->customPlot->legend->setVisible(true);
  QFont legendFont = font();
  legendFont.setPointSize(10);
  ui->customPlot->legend->setFont(legendFont);
  ui->customPlot->legend->setSelectedFont(legendFont);
  ui->customPlot->legend->setSelectableParts(QCPLegend::spItems); // legend box shall not be selectable, only legend items
  
  ui->customPlot->rescaleAxes();
  
  connect(ui->customPlot, SIGNAL(selectionChangedByUser()), this, SLOT(selectionChanged()));
  connect(ui->customPlot, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(mousePress(QMouseEvent *)));

  //connect(ui->customPlot, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(mouseWheel()));


  connect(ui->customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->customPlot->xAxis2, SLOT(setRange(QCPRange)));
  connect(ui->customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), ui->customPlot->yAxis2, SLOT(setRange(QCPRange)));
  

  connect(title, SIGNAL(doubleClicked(QMouseEvent*)), this, SLOT(titleDoubleClick(QMouseEvent*)));
  
 // connect(ui->customPlot, SIGNAL(plottableClick(QCPAbstractPlottable*,int,QMouseEvent*)), this, SLOT(graphClicked(QCPAbstractPlottable*,int)));
  

  // setup policy and connect slot for context menu popup:
  ui->customPlot->setContextMenuPolicy(Qt::CustomContextMenu);
  connect(ui->customPlot, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(contextMenuRequest(QPoint)));

  double mass = 1680;
  double gravity = 9.81;
  double rolling_resistance_coeff = 0.015;
  double aero_resistance_coeff = 0.32;
  double air_density = 1.293;
  double frontal_area = 2.4;
  double pMax = 2500;
  double tMax = 100;
  QString filename = "C:/Users/dr14/Documents/Repositories/HorizontDLL/dependencies/road_complete_speed_limits_curvature_filter.txt";
  dp = new DataParser(filename.toStdString().c_str(), aero_resistance_coeff, frontal_area, air_density, mass, gravity, rolling_resistance_coeff, pMax, tMax);
  if (dp->get_rows()>0) {
      int n = dp->get_rows();
      QVector<double> x(n), y(n);
      double * x_p =(double *) malloc(sizeof(double) * n);
      double * y_p =(double *) malloc(sizeof(double) * n);
      double * z_p =(double *) malloc(sizeof(double) * n);

      dp->get_x_vec(x_p,n);
      dp->get_y_vec(y_p,n);
      dp->get_z_vec(z_p,n);
      for (int i=0; i<n; i++)
      {
        x[i] = x_p[i];
        y[i] = y_p[i];
      }
      ui->customPlot->addGraph();
      ui->customPlot->graph()->setName(QString("Coords"));
      ui->customPlot->graph()->setData(x, y);
      ui->customPlot->graph()->setLineStyle(QCPGraph::LineStyle::lsNone);
      ui->customPlot->graph()->setScatterStyle(QCPScatterStyle::ScatterShape::ssCross);

      QPen graphPen;
      graphPen.setColor(QColor(0,0,0));
      ui->customPlot->graph()->setPen(graphPen);
      ui->customPlot->replot();
      ui->customPlot->rescaleAxes();



      pl_res = ui->customPlot->addGraph();
      pl_res->setName(QString("Reult"));
      pl_res->setLineStyle(QCPGraph::LineStyle::lsLine);
      pl_res->setScatterStyle(QCPScatterStyle::ScatterShape::ssDot);

      pl_res_dis = ui->customPlot->addGraph();
      pl_res_dis->setName(QString("Reult"));
      pl_res_dis->setLineStyle(QCPGraph::LineStyle::lsNone);
      pl_res_dis->setScatterStyle(QCPScatterStyle::ScatterShape::ssDiamond);

      // recomended speed
      double * rec_speed_vec =(double *) malloc(sizeof(double) * n);
      double * dist_vec =(double *) malloc(sizeof(double) * n);
      double * max_speed_vec =(double *) malloc(sizeof(double) * n);
      double * coasting_vec =(double *) malloc(sizeof(double) * n);


      dp->get_rec_speed_vec(rec_speed_vec,n);
      dp->get_dist_vec(dist_vec,n);
      dp->get_max_speed_vec(max_speed_vec,n);
      dp->get_coasting_speed_vec(coasting_vec,n);
      QVector<double> dist(n), rec_speed(n),max_speed(n), coasting(n);

      for (int i=0; i<n; i++)
      {
        dist[i] = dist_vec[i];
        rec_speed[i] = rec_speed_vec[i];
        max_speed[i] = max_speed_vec[i];
        coasting[i]  = coasting_vec[i];
      }

      ui->custom_plot2->addGraph();
      ui->custom_plot2->graph()->setName(QString("Recomended"));
      ui->custom_plot2->graph()->setData(dist, rec_speed);
      ui->custom_plot2->graph()->setLineStyle(QCPGraph::LineStyle::lsLine);
      ui->custom_plot2->graph()->setScatterStyle(QCPScatterStyle::ScatterShape::ssNone);


      ui->custom_plot2->addGraph();
      ui->custom_plot2->graph()->setName(QString("Max"));
      ui->custom_plot2->graph()->setData(dist, max_speed);
      ui->custom_plot2->graph()->setLineStyle(QCPGraph::LineStyle::lsLine);
      ui->custom_plot2->graph()->setScatterStyle(QCPScatterStyle::ScatterShape::ssNone);
      QPen graphPen2;
      graphPen2.setColor(QColor(255,0,0));
      ui->custom_plot2->graph()->setPen(graphPen2);


      ui->custom_plot2->addGraph();
      ui->custom_plot2->graph()->setName(QString("Coasting"));
      ui->custom_plot2->graph()->setData(dist, coasting);
      ui->custom_plot2->graph()->setLineStyle(QCPGraph::LineStyle::lsLine);
      ui->custom_plot2->graph()->setScatterStyle(QCPScatterStyle::ScatterShape::ssCircle);
      QPen graphPen3;
      graphPen3.setColor(QColor(0,255,0));
      ui->custom_plot2->graph()->setPen(graphPen3);


      ui->custom_plot2->replot();
      ui->custom_plot2->rescaleAxes();
      //preserveRatio(ui->custom_plot2);
      preserveRatio(ui->customPlot);

  }
}

void MainWindow::preserveRatio(QCustomPlot * pl) {
    auto center =pl->axisRect()->center();
    auto rangeX = pl->axisRect()->rangeZoomAxis(Qt::Horizontal)->range();
    auto rangeY = pl->axisRect()->rangeZoomAxis(Qt::Vertical)->range();
    double rx = (rangeX.upper - rangeX.lower)*1.0 /2;
    double ry = (rangeY.upper - rangeY.lower)*1.0 /2;
    if (rx>ry){
        pl->yAxis->setRangeLower(ry - rx);
        pl->yAxis->setRangeUpper(ry + rx);

    } else {
        pl->xAxis->setRangeLower(rx - ry);
        pl->xAxis->setRangeUpper(rx + ry);
    }


}


MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::titleDoubleClick(QMouseEvent* event)
{
  Q_UNUSED(event)
  if (QCPTextElement *title = qobject_cast<QCPTextElement*>(sender()))
  {
    // Set the plot title by double clicking on it
    bool ok;
    QString newTitle = QInputDialog::getText(this, "QCustomPlot example", "New plot title:", QLineEdit::Normal, title->text(), &ok);
    if (ok)
    {
      title->setText(newTitle);
      ui->customPlot->replot();
    }
  }
}

void MainWindow::axisLabelDoubleClick(QCPAxis *axis, QCPAxis::SelectablePart part)
{
  // Set an axis label by double clicking on it
  if (part == QCPAxis::spAxisLabel) // only react when the actual axis label is clicked, not tick label or axis backbone
  {
    bool ok;
    QString newLabel = QInputDialog::getText(this, "QCustomPlot example", "New axis label:", QLineEdit::Normal, axis->label(), &ok);
    if (ok)
    {
      axis->setLabel(newLabel);
      ui->customPlot->replot();
    }
  }
}

void MainWindow::legendDoubleClick(QCPLegend *legend, QCPAbstractLegendItem *item)
{
  // Rename a graph by double clicking on its legend item
  Q_UNUSED(legend)
  if (item) // only react if item was clicked (user could have clicked on border padding of legend where there is no item, then item is 0)
  {
    QCPPlottableLegendItem *plItem = qobject_cast<QCPPlottableLegendItem*>(item);
    bool ok;
    QString newName = QInputDialog::getText(this, "QCustomPlot example", "New graph name:", QLineEdit::Normal, plItem->plottable()->name(), &ok);
    if (ok)
    {
      plItem->plottable()->setName(newName);
      ui->customPlot->replot();
    }
  }
}

void MainWindow::selectionChanged()
{
  /*
   normally, axis base line, axis tick labels and axis labels are selectable separately, but we want
   the user only to be able to select the axis as a whole, so we tie the selected states of the tick labels
   and the axis base line together. However, the axis label shall be selectable individually.
   
   The selection state of the left and right axes shall be synchronized as well as the state of the
   bottom and top axes.
   
   Further, we want to synchronize the selection of the graphs with the selection state of the respective
   legend item belonging to that graph. So the user can select a graph by either clicking on the graph itself
   or on its legend item.
  */
  
  // make top and bottom axes be selected synchronously, and handle axis and tick labels as one selectable object:
  if (ui->customPlot->xAxis->selectedParts().testFlag(QCPAxis::spAxis) || ui->customPlot->xAxis->selectedParts().testFlag(QCPAxis::spTickLabels) ||
      ui->customPlot->xAxis2->selectedParts().testFlag(QCPAxis::spAxis) || ui->customPlot->xAxis2->selectedParts().testFlag(QCPAxis::spTickLabels))
  {
    ui->customPlot->xAxis2->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
    ui->customPlot->xAxis->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
  }
  // make left and right axes be selected synchronously, and handle axis and tick labels as one selectable object:
  if (ui->customPlot->yAxis->selectedParts().testFlag(QCPAxis::spAxis) || ui->customPlot->yAxis->selectedParts().testFlag(QCPAxis::spTickLabels) ||
      ui->customPlot->yAxis2->selectedParts().testFlag(QCPAxis::spAxis) || ui->customPlot->yAxis2->selectedParts().testFlag(QCPAxis::spTickLabels))
  {
    ui->customPlot->yAxis2->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
    ui->customPlot->yAxis->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
  }
  
  // synchronize selection of graphs with selection of corresponding legend items:
  for (int i=0; i<ui->customPlot->graphCount(); ++i)
  {
    QCPGraph *graph = ui->customPlot->graph(i);
    QCPPlottableLegendItem *item = ui->customPlot->legend->itemWithPlottable(graph);
    if (item->selected() || graph->selected())
    {
      item->setSelected(true);
      graph->setSelection(QCPDataSelection(graph->data()->dataRange()));
    }
  }
}





void MainWindow::mousePress(QMouseEvent *event)
{
  // if an axis is selected, only allow the direction of that axis to be dragged
  // if no axis is selected, both directions may be dragged
 if (ui->customPlot->xAxis->selectedParts().testFlag(QCPAxis::spAxis))
    ui->customPlot->axisRect()->setRangeDrag(ui->customPlot->xAxis->orientation());
  else if (ui->customPlot->yAxis->selectedParts().testFlag(QCPAxis::spAxis))
    ui->customPlot->axisRect()->setRangeDrag(ui->customPlot->yAxis->orientation());
  else
    ui->customPlot->axisRect()->setRangeDrag(Qt::Horizontal|Qt::Vertical);\
 auto tl = ui->customPlot->axisRect()->topLeft();
 auto br = ui->customPlot->axisRect()->bottomRight();
 auto rangeX = ui->customPlot->axisRect()->rangeZoomAxis(Qt::Horizontal)->range();
 auto rangeY = ui->customPlot->axisRect()->rangeZoomAxis(Qt::Vertical)->range();
 double x =(event->pos().x() - tl.x())*1.0 / (br.x() - tl.x()) * (rangeX.upper - rangeX.lower)+rangeX.lower;
 double y =(event->pos().y() - br.y())*1.0 / (tl.y() - br.y()) * (rangeY.upper - rangeY.lower)+rangeY.lower;
 Horizont_DLL::Point p(x,y,0);
 double d_from_start;
 Horizont_DLL::Point cp = dp->get_polyline()->closest_point_on_polyline(p,&d_from_start,100, true,true);
 pl_res->setData(QVector<double>(),QVector<double>());
 QString message = QString("Clicked on axis (%1 %2) (%3 %4) - %5")
         .arg(x).arg(y)
         .arg(cp.get_x()).arg(cp.get_y()).arg(d_from_start);
 pl_res->addData(x,y);
 pl_res->addData(cp.get_x(),cp.get_y());
 double x_o,y_o,z_o;
 dp->get_pos_at_distance(d_from_start, &x_o,&y_o,&z_o);
 pl_res_dis->setData(QVector<double>(),QVector<double>());
 pl_res_dis->addData(x_o,y_o);
 ui->statusBar->showMessage(message, 2500);

 // recomended speed_plot


}



void MainWindow::addRandomGraph()
{
  int n = 50; // number of points in graph
  double xScale = (rand()/(double)RAND_MAX + 0.5)*2;
  double yScale = (rand()/(double)RAND_MAX + 0.5)*2;
  double xOffset = (rand()/(double)RAND_MAX - 0.5)*4;
  double yOffset = (rand()/(double)RAND_MAX - 0.5)*10;
  double r1 = (rand()/(double)RAND_MAX - 0.5)*2;
  double r2 = (rand()/(double)RAND_MAX - 0.5)*2;
  double r3 = (rand()/(double)RAND_MAX - 0.5)*2;
  double r4 = (rand()/(double)RAND_MAX - 0.5)*2;
  QVector<double> x(n), y(n);
  for (int i=0; i<n; i++)
  {
    x[i] = (i/(double)n-0.5)*10.0*xScale + xOffset;
    y[i] = (qSin(x[i]*r1*5)*qSin(qCos(x[i]*r2)*r4*3)+r3*qCos(qSin(x[i])*r4*2))*yScale + yOffset;
  }
  
  ui->customPlot->addGraph();
  ui->customPlot->graph()->setName(QString("New graph %1").arg(ui->customPlot->graphCount()-1));
  ui->customPlot->graph()->setData(x, y);
  ui->customPlot->graph()->setLineStyle((QCPGraph::LineStyle)(rand()%5+1));
  if (rand()%100 > 50)
    ui->customPlot->graph()->setScatterStyle(QCPScatterStyle((QCPScatterStyle::ScatterShape)(rand()%14+1)));
  QPen graphPen;
  graphPen.setColor(QColor(rand()%245+10, rand()%245+10, rand()%245+10));
  graphPen.setWidthF(rand()/(double)RAND_MAX*2+1);
  ui->customPlot->graph()->setPen(graphPen);
  ui->customPlot->replot();
}

void MainWindow::removeSelectedGraph()
{
  if (ui->customPlot->selectedGraphs().size() > 0)
  {
    ui->customPlot->removeGraph(ui->customPlot->selectedGraphs().first());
    ui->customPlot->replot();
  }
}

void MainWindow::removeAllGraphs()
{
  ui->customPlot->clearGraphs();
  ui->customPlot->replot();
}

void MainWindow::contextMenuRequest(QPoint pos)
{
  QMenu *menu = new QMenu(this);
  menu->setAttribute(Qt::WA_DeleteOnClose);
  
  if (ui->customPlot->legend->selectTest(pos, false) >= 0) // context menu on legend requested
  {
    menu->addAction("Move to top left", this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignLeft));
    menu->addAction("Move to top center", this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignHCenter));
    menu->addAction("Move to top right", this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignRight));
    menu->addAction("Move to bottom right", this, SLOT(moveLegend()))->setData((int)(Qt::AlignBottom|Qt::AlignRight));
    menu->addAction("Move to bottom left", this, SLOT(moveLegend()))->setData((int)(Qt::AlignBottom|Qt::AlignLeft));
  } else  // general context menu on graphs requested
  {
    menu->addAction("Add random graph", this, SLOT(addRandomGraph()));
    if (ui->customPlot->selectedGraphs().size() > 0)
      menu->addAction("Remove selected graph", this, SLOT(removeSelectedGraph()));
    if (ui->customPlot->graphCount() > 0)
      menu->addAction("Remove all graphs", this, SLOT(removeAllGraphs()));
  }
  
  menu->popup(ui->customPlot->mapToGlobal(pos));
}

void MainWindow::moveLegend()
{
  if (QAction* contextAction = qobject_cast<QAction*>(sender())) // make sure this slot is really called by a context menu action, so it carries the data we need
  {
    bool ok;
    int dataInt = contextAction->data().toInt(&ok);
    if (ok)
    {
      ui->customPlot->axisRect()->insetLayout()->setInsetAlignment(0, (Qt::Alignment)dataInt);
      ui->customPlot->replot();
    }
  }
}

void MainWindow::graphClicked(QCPAbstractPlottable *plottable, int dataIndex)
{
  // since we know we only have QCPGraphs in the plot, we can immediately access interface1D()
  // usually it's better to first check whether interface1D() returns non-zero, and only then use it.
  double dataValue = plottable->interface1D()->dataMainValue(dataIndex);
  QString message = QString("Clicked on graph '%1' at data point #%2 with value %3.").arg(plottable->name()).arg(dataIndex).arg(dataValue);
  ui->statusBar->showMessage(message, 2500);
}





void MainWindow::on_evaluata_clicked()
{
    double x = ui->x->value();
    double y = ui->y->value();
    double z  = ui->z->value();


    Horizont_DLL::Point p(x,y,x);
    double d_from_start;
    Horizont_DLL::Point cp = dp->get_polyline()->closest_point_on_polyline(p,&d_from_start,100, true,true);

    pl_res->setData(QVector<double>(),QVector<double>());
    QString message = QString("Clicked on axis (%1 %2) (%3 %4) - %5")
            .arg(x).arg(y)
            .arg(cp.get_x()).arg(cp.get_y()).arg(d_from_start);
    pl_res->addData(x,y);
    pl_res->addData(cp.get_x(),cp.get_y());
    double x_o,y_o,z_o;
    dp->get_pos_at_distance(d_from_start, &x_o,&y_o,&z_o);
    pl_res_dis->setData(QVector<double>(),QVector<double>());
    pl_res_dis->addData(x_o,y_o);
    ui->statusBar->showMessage(message, 2500);


}

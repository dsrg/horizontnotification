#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QInputDialog>
#include "../qcustomplot/qcustomplot.h"
#include "Polyline.h"
#include "DataParser.h"

using namespace Horizont_DLL;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT
  
public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();
  
private slots:
  void titleDoubleClick(QMouseEvent *event);
  void axisLabelDoubleClick(QCPAxis* axis, QCPAxis::SelectablePart part);
  void legendDoubleClick(QCPLegend* legend, QCPAbstractLegendItem* item);
  void selectionChanged();
  void mousePress(QMouseEvent *event);
  void addRandomGraph();
  void removeSelectedGraph();
  void removeAllGraphs();
  void contextMenuRequest(QPoint pos);
  void moveLegend();
  void graphClicked(QCPAbstractPlottable *plottable, int dataIndex);
  void on_evaluata_clicked();

private:
  Ui::MainWindow *ui;
  DataParser * dp;
  QCPGraph * pl_res;
  QCPGraph * pl_res_dis;

  void preserveRatio(QCustomPlot *pl);
};

#endif // MAINWINDOW_H

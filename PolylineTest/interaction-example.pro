#-------------------------------------------------
#
# Project created by QtCreator 2012-03-04T23:24:55
#
#-------------------------------------------------

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = interaction-example
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
         ../qcustomplot/qcustomplot.cpp \
    ../HorizontDLL/src/Polyline.cpp \
    ..//HorizontDLL/src/DataParser.cpp

HEADERS  += mainwindow.h \
         ../qcustomplot/qcustomplot.h \
    ../HorizontDLL/include/Polyline.h \
    ../HorizontDLL/include/DataParser.h

INCLUDEPATH += $$PWD/../HorizontDLL/include

FORMS    += mainwindow.ui

